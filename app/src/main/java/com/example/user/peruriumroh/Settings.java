package com.example.user.peruriumroh;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

public class Settings {

    SharedPreferences mSettings;
    SharedPreferences.Editor editor;

    public Settings(Activity act){
        mSettings = act.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        editor = mSettings.edit();
    }

    public void SettingAddData(String token, String userId, String name, String email, String telp){
        editor.putString("token",token);
        editor.putString("userid",userId);
        editor.putString("name",name);
        editor.putString("email",email);
        editor.putString("telp",telp);
    }

    public List<String> SettingGetData(){
        List<String> dataSetting = new ArrayList<String>();
        dataSetting.add(mSettings.getString("token",null));
        dataSetting.add(mSettings.getString("userid",null));
        dataSetting.add(mSettings.getString("name",null));
        dataSetting.add(mSettings.getString("email",null));
        dataSetting.add(mSettings.getString("telp",null));
        return dataSetting;
    }
}

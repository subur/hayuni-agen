package com.example.user.peruriumroh.model;

public class ResoponseTopUp {
    private Data data;

    private String errorcode;

    private String errormsg;

    public Data getData() {
        return data;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public class Data {
        private String timeStamp;

        private String timeout_duration;

        private String resultCode;

        private String resultDesc;

        private String payment_code;

        public String getTimeStamp() {
            return timeStamp;
        }

        public String getTimeout_duration() {
            return timeout_duration;
        }

        public String getResultCode() {
            return resultCode;
        }

        public String getResultDesc() {
            return resultDesc;
        }

        public String getPayment_code() {
            return payment_code;
        }
    }
}

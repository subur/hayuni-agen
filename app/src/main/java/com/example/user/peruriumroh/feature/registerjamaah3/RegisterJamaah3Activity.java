package com.example.user.peruriumroh.feature.registerjamaah3;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.registerjamaah1.RegisterJamaah1Activity;
import com.example.user.peruriumroh.feature.registerjamaah2.RegisterJamaah2Activity;
import com.example.user.peruriumroh.feature.registerjamaah4.RegisterJamaah4Activity;
import com.example.user.peruriumroh.feature.registerjamaah5.RegisterJamaah5Activity;
import com.example.user.peruriumroh.model.RegisterResponse;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterJamaah3Activity extends AppCompatActivity {

    EditText reg3AlamatTempatTinggal, reg3AlamatKabupatenKota, reg3AlamatProvinsi, reg3AlamatEmail, reg3NomorTelRumah,
            reg3NomorHpWa, reg3NamaKontakDarurat, reg3NomorKontakDarurat;

    Spinner reg3SpinnerHubunganKontak;

    RelativeLayout rlDokumenTambahan;
    Preferences preferences;

    public static Activity fa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_jamaah3);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        fa = this;
        preferences = new Preferences(RegisterJamaah3Activity.this);

        reg3AlamatTempatTinggal = (EditText) findViewById(R.id.reg3_alamat_tempat_tinggal);
        reg3AlamatKabupatenKota = (EditText) findViewById(R.id.reg3_alamat_kabupaten_kota);
        reg3AlamatProvinsi = (EditText) findViewById(R.id.reg3_alamat_provinsi);
        reg3AlamatEmail = (EditText)findViewById(R.id.reg3_alamat_email);
        reg3NomorTelRumah = (EditText)findViewById(R.id.reg3_nomor_tel_rumah);
        reg3NomorHpWa = (EditText)findViewById(R.id.reg3_nomor_hp_wa);
        reg3NamaKontakDarurat = (EditText)findViewById(R.id.reg3_nama_kontak_darurat);
        reg3NomorKontakDarurat = (EditText)findViewById(R.id.reg3_nomor_kontak_darurat);
        reg3SpinnerHubunganKontak = (Spinner)findViewById(R.id.reg3_spinner_hubungan_kontak);

        rlDokumenTambahan = (RelativeLayout)findViewById(R.id.rl_dokumen_tambahan);
        rlDokumenTambahan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(reg3AlamatTempatTinggal.getText().length() == 0){
                    reg3AlamatTempatTinggal.setError("Alamat tempat tinggal tidak boleh kosong");
                }else if(reg3AlamatKabupatenKota.getText().length() == 0){
                    reg3AlamatKabupatenKota.setError("Alamat Kabupaten tidak boleh kosong");
                }else if(reg3AlamatProvinsi.getText().length() == 0){
                    reg3AlamatProvinsi.setError("Alamat provinsi tidak boleh kosong");
                }else if(reg3NomorHpWa.getText().length() == 0){
                    reg3NomorHpWa.setError("Nomor HP / WA tidak boleh kosong");
                }else if(reg3NamaKontakDarurat.getText().length() == 0){
                    reg3NamaKontakDarurat.setError("Nama Kontak Darurat tidak boleh kosong");
                }else if(reg3NomorKontakDarurat.getText().length() == 0){
                    reg3NomorKontakDarurat.setError("Nomor Kontak Darurat tidak boleh kosong");
                }else {
                    preferences.setReg3AlamatTempatTinggal(reg3AlamatTempatTinggal.getText().toString());
                    preferences.setReg3AlamatKabupatenKota(reg3AlamatKabupatenKota.getText().toString());
                    preferences.setReg3AlamatProvinsi(reg3AlamatProvinsi.getText().toString());
                    preferences.setReg3AlamatEmail(reg3AlamatEmail.getText().toString());
                    preferences.setReg3NomorTelRumah(reg3NomorTelRumah.getText().toString());
                    preferences.setReg3NomorHpWa(reg3NomorHpWa.getText().toString());
                    preferences.setReg3NamaKontakDarurat(reg3NamaKontakDarurat.getText().toString());
                    preferences.setReg3NomorKontakDarurat(reg3NomorKontakDarurat.getText().toString());
                    preferences.setReg3HubunganKontak(reg3SpinnerHubunganKontak.getSelectedItem().toString());
                    registerUser();
                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Spinner spinner = (Spinner) findViewById(R.id.reg3_spinner_hubungan_kontak);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.hubungan_arrays, R.layout.spinner_form);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void registerUser() {
//        ApiClient apiClient = new ApiClient();
//        ApiClientInterface apiClientInterface = apiClient.clientInterface();
//        Call<RegisterResponse> call = apiClientInterface.registerJamaah(
//                preferences.getUserid(),
//                preferences.getToken(),
//                "mobile",
//                preferences.getAgent_id(),
//                preferences.getReg1NamaLengkap(),
//                preferences.getReg1NomorKtp(),
//                preferences.getReg1TempatLahir(),
//                preferences.getReg1TanggalLahir(),
//                preferences.getReg1GolonganDarah(),
//                preferences.getReg1StatusKawin(),
//                preferences.getReg1PendidikanTerakhir(),
//                preferences.getReg1Pekerjaan(),
//                preferences.getReg1PenyakitKhusus(),
//                preferences.getReg1Koordinator(),
//                preferences.getReg1Koordinator(),
//                preferences.getPaketId(),
//                preferences.getReg3AlamatTempatTinggal(),
//                preferences.getReg3AlamatKabupatenKota(),
//                preferences.getReg3AlamatProvinsi(),
//                preferences.getReg3AlamatEmail(),
//                preferences.getReg3NomorTelRumah(),
//                preferences.getReg3NomorHpWa(),
//                preferences.getReg3NamaKontakDarurat(),
//                preferences.getReg3NomorKontakDarurat(),
//                preferences.getReg3HubunganKontak(),
//                preferences.getReg2NomorPaspor(),
//                preferences.getReg2MasaBerlakuPaspor(),
//                preferences.getReg2TanggalDikeluarkanPaspor(),
//                preferences.getReg2UmrohKeberapa(),
//                preferences.getReg2KeberangkatanTerakhir(),
//                preferences.getReg1PeruriEmail(),
//                preferences.getReg1PeruriName()
//        );
//
//        call.enqueue(new Callback<ResponseBody>() {
//
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if(response.code() == 200){
//                    RegisterJamaah1Activity.fa.finish();
//                    RegisterJamaah2Activity.fa.finish();
//                    finish();
//                    preferences.setReg1NamaLengkap(null);
//                    preferences.setReg1NomorKtp(null);
//                    preferences.setReg1TempatLahir(null);
//                    preferences.setReg1TanggalLahir(null);
//                    preferences.setReg1GolonganDarah(null);
//                    preferences.setReg1StatusKawin(null);
//                    preferences.setReg1PendidikanTerakhir(null);
//                    preferences.setReg1Pekerjaan(null);
//                    preferences.setReg1PenyakitKhusus(null);
//                    preferences.setReg1PeruriName(null);
//                    preferences.setReg1PeruriEmail(null);
//                    preferences.setReg1Koordinator(null);
//                    preferences.setReg2KeberangkatanTerakhir(null);
//                    preferences.setReg2UmrohKeberapa(null);
//                    preferences.setReg2TanggalDikeluarkanPaspor(null);
//                    preferences.setReg2TempatDikeluarkanPaspor(null);
//                    preferences.setReg2MasaBerlakuPaspor(null);
//                    preferences.setReg2NomorPaspor(null);
//                    preferences.setReg3AlamatTempatTinggal(null);
//                    preferences.setReg3AlamatKabupatenKota(null);
//                    preferences.setReg3AlamatProvinsi(null);
//                    preferences.setReg3AlamatEmail(null);
//                    preferences.setReg3NomorTelRumah(null);
//                    preferences.setReg3NomorHpWa(null);
//                    preferences.setReg3NamaKontakDarurat(null);
//                    preferences.setReg3NomorKontakDarurat(null);
//                    preferences.setReg3HubunganKontak(null);
//                    Intent intent = new Intent(RegisterJamaah3Activity.this,RegisterJamaah5Activity.class);
//                    startActivity(intent);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//            }
//        });
    }
}

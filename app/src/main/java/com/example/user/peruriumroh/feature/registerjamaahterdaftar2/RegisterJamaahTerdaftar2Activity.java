package com.example.user.peruriumroh.feature.registerjamaahterdaftar2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.user.peruriumroh.R;

public class RegisterJamaahTerdaftar2Activity extends AppCompatActivity {

    TextView tvNama,tvKtp, tvNoHp, tvAlamat;

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_jamaah_terdaftar2);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        //VIEW VARIABLE
        tvNama = (TextView)findViewById(R.id.tv_register_nama);
        tvKtp = (TextView)findViewById(R.id.tv_register_ktp);
        tvNoHp = (TextView)findViewById(R.id.tv_register_nohp);
        tvAlamat = (TextView)findViewById(R.id.tv_register_alamat);

        Intent intent = getIntent();

        tvNama.setText(intent.getStringExtra("nama"));
        tvKtp.setText(intent.getStringExtra("ktp"));
        tvNoHp.setText(intent.getStringExtra("nohp"));
        tvAlamat.setText(intent.getStringExtra("alamat"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_close_white,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result","finish");
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
}

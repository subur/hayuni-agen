package com.example.user.peruriumroh.model;

public class SignInPPay {
    private Data data;

    private String errorcode;

    private String errormsg;

    public Data getData() {
        return data;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public class Data {
        private String timeStamp;

        private String resultCode;

        private String sessionId;

        private String login;

        private String resultDesc;

        private User user;

        public String getTimeStamp() {
            return timeStamp;
        }

        public String getResultCode() {
            return resultCode;
        }

        public String getSessionId() {
            return sessionId;
        }

        public String getLogin() {
            return login;
        }

        public String getResultDesc() {
            return resultDesc;
        }

        public User getUser() {
            return user;
        }
    }

    public class User {
        private String lastLogin;

        private String idTmoney;

        private String changeDevice;

        private String balance;

        private String verified;

        private String tokenExpiry;

        private String idFusion;

        private String phoneVerify;

        private String custName;

        private String custPhone;

        private String token;

        public String getLastLogin() {
            return lastLogin;
        }

        public String getIdTmoney() {
            return idTmoney;
        }

        public String getChangeDevice() {
            return changeDevice;
        }

        public String getBalance() {
            return balance;
        }

        public String getVerified() {
            return verified;
        }

        public String getTokenExpiry() {
            return tokenExpiry;
        }

        public String getIdFusion() {
            return idFusion;
        }

        public String getPhoneVerify() {
            return phoneVerify;
        }

        public String getCustName() {
            return custName;
        }

        public String getCustPhone() {
            return custPhone;
        }

        public String getToken() {
            return token;
        }
    }
}

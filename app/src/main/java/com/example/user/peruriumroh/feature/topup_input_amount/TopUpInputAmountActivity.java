package com.example.user.peruriumroh.feature.topup_input_amount;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.agen_profile_password.AgenProfilePasswordActivity;
import com.example.user.peruriumroh.feature.topup_kode_topup.TopUpKodeTopUpActivity;
import com.example.user.peruriumroh.feature.transfer_scan.TransferScanActivity;
import com.example.user.peruriumroh.model.ResoponseTopUp;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopUpInputAmountActivity extends AppCompatActivity {

    Button btTopUp;
    Preferences preferences;
    EditText etNominal;

    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();

    ProgressDialog progressBar;

    private String current = "";

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_up_input_amount);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Top Up Saldo");

        //PROGRESS BAR
        progressBar = new ProgressDialog(TopUpInputAmountActivity.this);
        progressBar.setCancelable(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        preferences = new Preferences(TopUpInputAmountActivity.this);

        btTopUp = (Button)findViewById(R.id.bt_top_up);

        etNominal = (EditText)findViewById(R.id.et_nominal);
        etNominal.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(current)){
                    String formatted = "0";
                    etNominal.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    if (cleanString.equals("")){
                        formatted = CurrencyFormating.setToCurrency("0");
                        btTopUp.setEnabled(false);
                    }else {
                        formatted = CurrencyFormating.setToCurrency(cleanString);
                        if(cleanString.length() >= 5){
                            btTopUp.setEnabled(true);
                        }else {
                            btTopUp.setEnabled(false);
                        }
                    }

                    current = formatted;
                    etNominal.setText(formatted);
                    etNominal.setSelection(formatted.length());

                    etNominal.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressBar.setMessage("Membuat Kode Top Up");
                progressBar.show();

                Call<ResoponseTopUp> call = apiClientInterface.jamaahPostTopUp(
                        preferences.getUserid(),
                        preferences.getToken(),
                        "mobile",
                        preferences.getIdTmoney(),
                        preferences.getIdFusion(),
                        preferences.getTokenppay(),
                        etNominal.getText().toString().replaceAll("[Rp,.]", "")
                );

                call.enqueue(new Callback<ResoponseTopUp>() {
                    @Override
                    public void onResponse(Call<ResoponseTopUp> call, Response<ResoponseTopUp> response) {
                        if(response.code() == 200){
                            if(response.body().getData().getResultCode().equals("0")){
                                progressBar.dismiss();
                                Intent intent = new Intent(TopUpInputAmountActivity.this,TopUpKodeTopUpActivity.class);
                                startActivity(intent);
                            }else {
                                progressBar.dismiss();
                                Toast.makeText(getApplicationContext(),response.body().getData().getResultDesc(),Toast.LENGTH_LONG).show();
                            }
                        }else if(response.code() == 401){
                            progressBar.dismiss();
                            Logout.logout(TopUpInputAmountActivity.this);
                        }
                        else {
                            progressBar.dismiss();
                            Toast.makeText(getApplicationContext(),response.body().getData().getResultDesc(),Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResoponseTopUp> call, Throwable t) {
                        AlertConnectionDialog.showConnectionAlert(TopUpInputAmountActivity.this);
                        progressBar.dismiss();
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

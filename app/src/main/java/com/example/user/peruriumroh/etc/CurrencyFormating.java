package com.example.user.peruriumroh.etc;

import java.text.NumberFormat;
import java.util.Locale;

public class CurrencyFormating {

    public static String setToCurrency(String Rupiah){
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(Double.valueOf(Rupiah));
    }

}

package com.example.user.peruriumroh.feature.MainDrawer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.BottomSheetConnectionDialog;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.fragment_daftarjamaah.DaftarJamaahFragment;
import com.example.user.peruriumroh.feature.fragment_paketumroh.PaketUmrohFragment;
import com.example.user.peruriumroh.feature.fragment_pembayaran.PembayaranFragment;
import com.example.user.peruriumroh.feature.home.HomeActivity;
import com.example.user.peruriumroh.feature.landing.WelcomeActivity;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import org.json.JSONObject;

import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BottomSheetConnectionDialog.BottomSheetListener {

    int page;
    LinearLayout llLogout;
    Preferences preferences;
    TextView tvUsernameHeader;
    CircleImageView civProfileHeader;

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Get Preferences
        preferences = new Preferences(DrawerActivity.this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        tvUsernameHeader = (TextView) headerView.findViewById(R.id.tv_username_header);
        tvUsernameHeader.setText(preferences.getName());

        civProfileHeader = (CircleImageView)headerView.findViewById(R.id.civ_profile_header);
        //tvUsernameHeader.setText("Asdsadsadsadsadas");
        Glide.with(DrawerActivity.this).load("http://13.228.32.47/umroh/api/" + preferences.getImages()).apply(RequestOptions.bitmapTransform(new RoundedCorners(10))).into(civProfileHeader);

        llLogout = (LinearLayout)findViewById(R.id.nav_logout);
        llLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Logout.logoutNoAlert(DrawerActivity.this);
            }
        });


        if(savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if(extras != null){
                page = extras.getInt("INT_PAGE");
            }
        }else {
            page = (int) savedInstanceState.getSerializable("INT_PAGE");
        }
        switch (page){
            case 1:
                navigationView.getMenu().getItem(0).setChecked(true);
                PaketUmrohFragment fragmentPaket = new PaketUmrohFragment();
                changeFragment(fragmentPaket);
                break;

            case 2:
                navigationView.getMenu().getItem(1).setChecked(true);
                PembayaranFragment fragmentPembayaran = new PembayaranFragment();
                changeFragment(fragmentPembayaran);
                break;

            case 3:
                navigationView.getMenu().getItem(2).setChecked(true);
                DaftarJamaahFragment fragmentJamaah = new DaftarJamaahFragment();
                changeFragment(fragmentJamaah);
                break;
        }
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e){}
    }
    public static boolean deleteDir(File dir){
        if (dir != null && dir.isDirectory()){
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_paket_umroh)  {
            PaketUmrohFragment fragment = new PaketUmrohFragment();
            changeFragment(fragment);
        } else if (id == R.id.nav_pembayaran) {
            PembayaranFragment fragment = new PembayaranFragment();
            changeFragment(fragment);
        } else if (id == R.id.nav_daftar_jamaah) {
            DaftarJamaahFragment fragment = new DaftarJamaahFragment();
            changeFragment(fragment);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void changeFragment(Fragment vFragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main,vFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onButtonClick(String text) {
        finish();
    }
}

package com.example.user.peruriumroh.feature.login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.user.peruriumroh.R;
import com.goodiebag.pinview.Pinview;

public class WhiteLabelActivity extends AppCompatActivity {

    Pinview pv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_white_label);

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        pv = (Pinview)findViewById(R.id.pinview);
    }
}

package com.example.user.peruriumroh.feature.transfer_manual_amount;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.AlertDialog;
import com.example.user.peruriumroh.etc.AlertDialogFinish;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.transfer_manual.TransferManualActivity;
import com.example.user.peruriumroh.feature.transfer_pin.TransferPINActivity;
import com.example.user.peruriumroh.feature.transfer_scan.TransferScanActivity;
import com.example.user.peruriumroh.model.ResponseInquiry;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransferManualAmountActivity extends AppCompatActivity {

    String role, id, nama, email;

    TextView tvDetailNama, tvEmail;

    EditText etNominal;

    ProgressDialog progressBar;

    Button btSubmit;

    private String current = "";

    private String cleanString;

    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();

    Preferences preferences;

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_manual_amount);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Transfer Peruri Tunai");

        preferences = new Preferences(TransferManualAmountActivity.this);

        tvDetailNama    = (TextView)findViewById(R.id.tv_detail_nama);
        tvEmail = (TextView)findViewById(R.id.tv_detail_email);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                role = null;
                id = null;
                nama = null;
                email = null;
            } else {
                role = extras.getString("role");
                id = extras.getString("id");
                nama = extras.getString("nama");
                email = extras.getString("email");
            }
        } else {
            role    = (String) savedInstanceState.getSerializable("role");
            id      = (String) savedInstanceState.getSerializable("id");
            nama    = (String) savedInstanceState.getSerializable("nama");
            email   = (String) savedInstanceState.getSerializable("email");
        }

        btSubmit = (Button)findViewById(R.id.bt_submit);

        tvDetailNama.setText(nama);
        tvEmail.setText(email);

        //PROGRESS BAR
        progressBar = new ProgressDialog(TransferManualAmountActivity.this);
        progressBar.setCancelable(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        etNominal = (EditText)findViewById(R.id.et_nominal);
        etNominal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(current)){
                    String formatted = "0";
                    etNominal.removeTextChangedListener(this);

                    cleanString = s.toString().replaceAll("[Rp,.]", "");
                    if (cleanString.equals("") || Integer.valueOf(cleanString) == 0){
                        formatted = CurrencyFormating.setToCurrency("0");
                        btSubmit.setEnabled(false);
                    }else if(Integer.valueOf(cleanString) > 0 && cleanString.length() <= 8){
                        Log.d("TAG", "onTextChanged: "+ cleanString.length() + Integer.MAX_VALUE);
                        formatted = CurrencyFormating.setToCurrency(cleanString);
                        btSubmit.setEnabled(true);
                    }else if(cleanString.length() > 8) {
                        String subs = cleanString.substring(0,8);
                        formatted = CurrencyFormating.setToCurrency(subs);
                        btSubmit.setEnabled(true);
                    }

                    current = formatted;
                    etNominal.setText(formatted);
                    etNominal.setSelection(formatted.length());

                    etNominal.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setMessage("Memuat Transaksi");
                progressBar.show();
                Call<ResponseInquiry> transInquiry = apiClientInterface.transactionPostInquiry(
                        preferences.getUserid(),
                        preferences.getToken(),
                        cleanString,
                        preferences.getAgent_id(),
                        "Agent",
                        preferences.getIdTmoney(),
                        preferences.getIdFusion(),
                        preferences.getTokenppay(),
                        id,
                        role
                );
                transInquiry.enqueue(new Callback<ResponseInquiry>() {
                    @Override
                    public void onResponse(Call<ResponseInquiry> call, Response<ResponseInquiry> response) {
                        if(response.code() == 200){
                            if(response.body().getErrorcode().equals("00000") && response.body().getErrormsg().equals("OK")){
                                progressBar.dismiss();
                                String destId           = id;
                                String destRole         = role;
                                String paymentAmount    = response.body().getData().getAmount();
                                String transactionId    = response.body().getData().getTransactionID();
                                String refNo            = response.body().getData().getRefNo();

                                //Open Intent PIN
                                Intent intent = new Intent(TransferManualAmountActivity.this,TransferPINActivity.class);
                                intent.putExtra("destinationId",destId);
                                intent.putExtra("destinationRole",destRole);
                                intent.putExtra("paymentAmount",paymentAmount);
                                intent.putExtra("transactionId",transactionId);
                                intent.putExtra("refNo",refNo);
                                startActivity(intent);
                            }else if(response.body().getErrorcode().equals("00165")){
                                if(response.body().getData().getResultCode().equals("GL-004")){
                                    progressBar.dismiss();
                                    AlertDialogFinish.showAlertFinish(TransferManualAmountActivity.this,"Umrah Safe", "Saldo Peruri Pay anda tidak mencukupi untuk transaksi ini.");
                                }else if(response.body().getData().getResultCode().equals("TA-005")){
                                    progressBar.dismiss();
                                    AlertDialogFinish.showAlertFinish(TransferManualAmountActivity.this,"Peruri Pay","Akun Peruri Pay tujuan tidak boleh sama dengan akun pengirim");
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseInquiry> call, Throwable t) {
                        progressBar.dismiss();
                        if(t instanceof IOException){
                            AlertConnectionDialog.showConnectionAlert(TransferManualAmountActivity.this);
                        }else {
                            AlertDialog.showAlert(TransferManualAmountActivity.this,"Peruri Umroh", "Terjadi kesalahan sistem");
                        }
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                TransferScanActivity.ac.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result","finish");
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
}

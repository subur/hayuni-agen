package com.example.user.peruriumroh.feature.registerjamaah5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.user.peruriumroh.R;

public class RegisterJamaah5Activity extends AppCompatActivity {
    RelativeLayout rlOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_jamaah5);
        rlOK = (RelativeLayout)findViewById(R.id.rl_ok);
        rlOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}

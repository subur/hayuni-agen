package com.example.user.peruriumroh.etc;

public class CheckNull {
    public static String checkNullString(String data){
        String returnData = "";
        if(data == null){
            returnData = "";
        }else {
            returnData = data;
        }
        return returnData;
    }

    public static String checkNullInt(String data){
        String returnData = "";
        if(data == null){
            returnData = "0";
        }else {
            returnData = data;
        }
        return returnData;
    }
}

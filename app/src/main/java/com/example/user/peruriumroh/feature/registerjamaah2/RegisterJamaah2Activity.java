package com.example.user.peruriumroh.feature.registerjamaah2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.registerjamaah1.RegisterJamaah1Activity;
import com.example.user.peruriumroh.feature.registerjamaah3.RegisterJamaah3Activity;
import com.example.user.peruriumroh.preference.Preferences;

public class RegisterJamaah2Activity extends AppCompatActivity {
    EditText reg2NomorPaspor, reg2MasaBerlakuPaspor, reg2TempatDikeluarkanPaspor,
        reg2TanggalDikeluarkanPaspor, reg2UmrohKeberapa, reg2KeberangkatanTerakhir;

    RelativeLayout rlDataKontak;
    Preferences preferences;
    public static Activity fa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_jamaah2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        fa = this;
        preferences = new Preferences(RegisterJamaah2Activity.this);

        reg2NomorPaspor = (EditText) findViewById(R.id.reg2_nomor_paspor);
        reg2MasaBerlakuPaspor = (EditText)findViewById(R.id.reg2_masa_berlaku_paspor);
        reg2TempatDikeluarkanPaspor = (EditText)findViewById(R.id.reg2_tempat_dikeluarkan_paspor);
        reg2TanggalDikeluarkanPaspor = (EditText)findViewById(R.id.reg2_tanggal_dikeluarkan_paspor);
        reg2UmrohKeberapa = (EditText)findViewById(R.id.reg2_umroh_keberapa);
        reg2KeberangkatanTerakhir = (EditText)findViewById(R.id.reg2_keberangkatan_terakhir);

        rlDataKontak = (RelativeLayout)findViewById(R.id.rl_data_kontak);
        rlDataKontak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(reg2NomorPaspor.getText().length() == 0){
                    reg2NomorPaspor.setError("Nomor paspor tidak boleh kosong");
                }else if(reg2MasaBerlakuPaspor.getText().length() == 0){
                    reg2MasaBerlakuPaspor.setError("Masa berlaku tidak boleh kosong");
                }else if(reg2TempatDikeluarkanPaspor.getText().length() == 0){
                    reg2TempatDikeluarkanPaspor.setError("Tempat dikeluarkan paspor tidak boleh kosong");
                }else if(reg2TanggalDikeluarkanPaspor.getText().length() == 0){
                    reg2TanggalDikeluarkanPaspor.setError("Tanggal dikeluarkan paspor tidak boleh kosong");
                }else {
                    preferences.setReg2NomorPaspor(reg2NomorPaspor.getText().toString());
                    preferences.setReg2MasaBerlakuPaspor(reg2MasaBerlakuPaspor.getText().toString());
                    preferences.setReg2TempatDikeluarkanPaspor(reg2TempatDikeluarkanPaspor.getText().toString());
                    preferences.setReg2TanggalDikeluarkanPaspor(reg2TanggalDikeluarkanPaspor.getText().toString());
                    preferences.setReg2UmrohKeberapa(reg2UmrohKeberapa.getText().toString());
                    preferences.setReg2KeberangkatanTerakhir(reg2KeberangkatanTerakhir.getText().toString());
                    Intent intent = new Intent(RegisterJamaah2Activity.this, RegisterJamaah3Activity.class);
                    startActivity(intent);
                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}

package com.example.user.peruriumroh.model;

import java.util.List;

public class ResponseBank {
    private List<Data> data;

    private String errorcode;

    private String errormsg;

    public List<Data> getData() {
        return data;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }


    public class Data {
        private String bank_telp;

        private String logo_path;

        private String bank_id;

        private String bank_name;

        private String bank_address;

        private String logo_filename;

        private String bank_email;

        public String getBank_telp() {
            return bank_telp;
        }

        public String getLogo_path() {
            return logo_path;
        }

        public String getBank_id() {
            return bank_id;
        }

        public String getBank_name() {
            return bank_name;
        }

        public String getBank_address() {
            return bank_address;
        }

        public String getLogo_filename() {
            return logo_filename;
        }

        public String getBank_email() {
            return bank_email;
        }
    }
}

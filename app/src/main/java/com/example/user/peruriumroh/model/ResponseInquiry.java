package com.example.user.peruriumroh.model;

public class ResponseInquiry {
    private Data data;

    private String errorcode;

    private String errormsg;

    public Data getData() {
        return data;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public class Data {
        private String timeStamp;

        private String refNo;

        private String feeAmount;

        private String totalAmount;

        private String destName;

        private String amount;

        private String destCode;

        private String resultCode;

        private String resultDesc;

        private String transactionID;

        public String getTimeStamp() {
            return timeStamp;
        }

        public String getRefNo() {
            return refNo;
        }

        public String getFeeAmount() {
            return feeAmount;
        }

        public String getTotalAmount() {
            return totalAmount;
        }

        public String getDestName() {
            return destName;
        }

        public String getAmount() {
            return amount;
        }

        public String getDestCode() {
            return destCode;
        }

        public String getResultCode() {
            return resultCode;
        }

        public String getResultDesc() {
            return resultDesc;
        }

        public String getTransactionID() {
            return transactionID;
        }
    }
}

package com.example.user.peruriumroh.etc;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatingDate {
    public static String getDate(String tanggal){
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dtIn = null;
        try {
            dtIn = inFormat.parse(tanggal);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy",LocaleID.getLocaleId());
        return dateFormat.format(dtIn);
    }

    public static String getDateCompleteMonth(String tanggal){
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dtIn = null;
        try {
            dtIn = inFormat.parse(tanggal);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy",LocaleID.getLocaleId());
        return dateFormat.format(dtIn);
    }
}

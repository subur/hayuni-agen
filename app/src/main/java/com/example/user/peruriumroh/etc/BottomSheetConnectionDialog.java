package com.example.user.peruriumroh.etc;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.user.peruriumroh.R;

public class BottomSheetConnectionDialog extends BottomSheetDialogFragment {

    private BottomSheetListener bottomSheetListener;
    Activity act;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_connection, container, false);

        Button buttonOk = (Button) view.findViewById(R.id.bt_connection_ok);
        buttonOk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                bottomSheetListener.onButtonClick("OK");
                dismiss();
            }
        });

        return view;
    }

    public interface BottomSheetListener{
        void onButtonClick(String text);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            bottomSheetListener = (BottomSheetListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implements BottomSheetListener");
        }

        if (context instanceof Activity){
            act = (Activity) context;
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        handleExit();
    }

    private void handleExit() {
        //Toast.makeText(getContext(), "TODO - SAVE data or similar" + act.getLocalClassName(), Toast.LENGTH_SHORT).show();
        act.finish();
    }
}

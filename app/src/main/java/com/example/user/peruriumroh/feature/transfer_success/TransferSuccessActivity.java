package com.example.user.peruriumroh.feature.transfer_success;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.CurrencyFormating;

public class TransferSuccessActivity extends AppCompatActivity {

    public static Activity ac;

    String amount;

    TextView tvAmountVal;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_success);

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        ac = this;

        if (savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                amount = null;
            } else {
                amount = extras.getString("amount");
            }
        } else {
            amount = (String) savedInstanceState.getSerializable("amount");
        }

        //VARIABLE VIEW
        tvAmountVal = (TextView)findViewById(R.id.tv_amount_val);
        tvAmountVal.setText(CurrencyFormating.setToCurrency(amount));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close_white,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Integer ItemMenu = item.getItemId();
        switch (ItemMenu){
            case R.id.action_close :

                finish();

                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

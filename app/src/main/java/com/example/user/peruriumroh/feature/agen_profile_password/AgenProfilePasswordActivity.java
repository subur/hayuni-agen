package com.example.user.peruriumroh.feature.agen_profile_password;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.ArrayMap;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class AgenProfilePasswordActivity extends AppCompatActivity {

    EditText etOldPassword, etNewPassword;
    Button btSave;
    Preferences preferences;

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agen_profile_password);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ganti Password");

        etOldPassword = (EditText)findViewById(R.id.et_old_password);
        etNewPassword = (EditText)findViewById(R.id.et_new_password);

        preferences = new Preferences(AgenProfilePasswordActivity.this);

        btSave = (Button)findViewById(R.id.bt_save);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etOldPassword.getText() == null){
                    etOldPassword.setError("Form tidak boleh kosong");
                }else if(etNewPassword.getText() == null){
                    etNewPassword.setError("Form tidak boleh kosong");
                }else if(etOldPassword != null && etNewPassword != null){
                    ApiClient apiClient = new ApiClient();
                    ApiClientInterface apiClientInterface = apiClient.clientInterface();

                    Map<String,Object> map = new ArrayMap<>();
                    map.put("oldpassword",etOldPassword.getText().toString());
                    map.put("newpassword",etNewPassword.getText().toString());

                    RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(map)).toString());
                    Call<ResponseBody> call = apiClientInterface.agenEdit(preferences.getUserid(),preferences.getToken(),"mobile",body);
                    call.enqueue(new retrofit2.Callback<ResponseBody>(){
                        @Override
                        public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                            try {
                                String strResponse = response.body().string();
                            } catch (IOException e){
                                e.printStackTrace();
                            }
                            if(response.code() == 200){
                                Toast.makeText(getApplicationContext(),"Sukses mengubah password",Toast.LENGTH_SHORT).show();
                            }else if(response.code() == 401){
                                Logout.logout(AgenProfilePasswordActivity.this);
                            }
                        }

                        @Override
                        public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
                            AlertConnectionDialog.showConnectionAlert(AgenProfilePasswordActivity.this);
                        }
                    });
                }
            }
        });
    }
}

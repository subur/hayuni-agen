package com.example.user.peruriumroh.network;

import com.example.user.peruriumroh.model.AgentData;
import com.example.user.peruriumroh.model.EachAgenLogin;
import com.example.user.peruriumroh.model.KoordinatorId;
import com.example.user.peruriumroh.model.ListJamaah;
import com.example.user.peruriumroh.model.PeruriPayRegister;
import com.example.user.peruriumroh.model.Produk;
import com.example.user.peruriumroh.model.RegisterResponse;
import com.example.user.peruriumroh.model.ResoponseTopUp;
import com.example.user.peruriumroh.model.ResponseBank;
import com.example.user.peruriumroh.model.ResponseChangePIN;
import com.example.user.peruriumroh.model.ResponseInquiry;
import com.example.user.peruriumroh.model.ResponseMutasi;
import com.example.user.peruriumroh.model.ResponsePayment;
import com.example.user.peruriumroh.model.ResponseTransfer;
import com.example.user.peruriumroh.model.SignInPPay;
import com.example.user.peruriumroh.model.userData;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiClientInterface {

    @FormUrlEncoded
    @POST("userlogin")
    Call<userData> agenLogin(
            @Field("username") String username,
            @Field("password") String password,
            @Field("role") String role,
            @Field("platform") String platform
    );

    @POST("userlogout")
    Call<ResponseBody> agenLogout(@Body RequestBody body);

    @PUT("users/user_id/{userId}/access_token/{accessToken}/location/platform/{dashboard}")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<ResponseBody> agenEdit(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Path("dashboard") String dashboard,
            @Body RequestBody body);

    @PUT("password/user_id/{userId}/access_token/{accessToken}/location/platform/{dashboard}")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<ResponseBody> agenEditPassword(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Path("dashboard") String dashboard,
            @Body RequestBody body);

    @GET("agent")
    Call<EachAgenLogin> getAgenByIdAgent(
            @Query("user_id") String userId,
            @Query("access_token") String accessToken,
            @Query("platform") String platform,
            @Query("agent_id") String agentId
    );

    @GET("agent")
    Call<EachAgenLogin> getAgenByEmail(
            @Query("user_id") String userId,
            @Query("access_token") String accessToken,
            @Query("platform") String platform,
            @Query("agent_email") String email
    );

    @GET("jamaah")
    Call<ListJamaah> jamaahGetJamaahByEmail(
            @Query("user_id") String userId,
            @Query("access_token") String accessToken,
            @Query("platform") String platform,
            @Query("email") String email
    );

    @GET("product")
    Call<Produk> agenGetProduk(
            @Query("user_id") String userId,
            @Query("access_token") String accessToken,
            @Query("location") String location,
            @Query("platform") String platform,
            @Query("group") String group,
            @Query("n_item") String nItem,
            @Query("page") String page,
            @Query("travel_id") String travelId
    );

    @GET("jamaah")
    Call<ListJamaah> agenGetJamaah(
            @Query("user_id") String userId,
            @Query("access_token") String accessToken,
            @Query("location") String location,
            @Query("platform") String platform,
            @Query("group") String group,
            @Query("n_item") String nItem,
            @Query("page") String page,
            @Query("agent_id") String agent_id
    );

    @GET("jamaah")
    Call<KoordinatorId> agenGetKoordinator(
            @Query("user_id") String userId,
            @Query("access_token") String accessToken,
            @Query("location") String location,
            @Query("platform") String platform,
            @Query("group") String group,
            @Query("n_item") String nItem,
            @Query("page") String page,
            @Query("email") String email
    );

    @FormUrlEncoded
    @POST("jamaah/user_id/{userId}/access_token/{accessToken}/location/platform/{platform}")
    Call<RegisterResponse> registerJamaah(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Path("platform") String platform,
            @Field("agent_id") String agent_id,
            @Field("name") String name,
            @Field("ayah") String ayah,
            @Field("ktp") String ktp,
            @Field("status") String status,
            @Field("package_id") String package_id,
            @Field("address") String address,
            @Field("email") String email,
            @Field("handphone") String handphone
    );

    @FormUrlEncoded
    @POST("jamaah/user_id/{userId}/access_token/{accessToken}/location/platform/{platform}")
    Call<RegisterResponse> registerJamaahKoordinator(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Path("platform") String platform,
            @Field("agent_id") String agent_id,
            @Field("name") String name,
            @Field("ayah") String ayah,
            @Field("ktp") String ktp,
            @Field("status") String status,
            @Field("package_id") String package_id,
            @Field("address") String address,
            @Field("email") String email,
            @Field("handphone") String handphone,
            @Field("koordinator") String koordinator
    );

//    @POST("jamaah/user_id/{userId}/access_token/{accessToken}/location/platform/{platform}")
//    Call<RegisterResponse> registerBasicData(
//            @Path("userId") String userId,
//            @Path("accessToken") String accessToken,
//            @Path("platform") String platform,
//            @Body RequestBody body
//    );

    @Multipart
    @POST("usersimage/user_id/{userId}/access_token/{accessToken}/location/platform/{platform}")
    Call<ResponseBody> agenEditPP(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Path("platform") String platform,
            @Part() MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("peruri_signup/user_id/{userId}/access_token/{accessToken}/location/platform/{platform}")
    Call<PeruriPayRegister> peruriSignUp(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Path("platform") String platform,
            @Field("accType") String accType,
            @Field("userName") String userName,
            @Field("password") String password,
            @Field("fullName") String fullName,
            @Field("phoneNo") String phoneNo
    );

    @FormUrlEncoded
    @POST("updatejamaah/user_id/{userId}/access_token/{accessToken}/location/platform/{platform}")
    Call<ResponseBody> jamaahUpdatePeruriPay(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Path("platform") String dashboard,
            @Field("jamaah_id") String jamaahId,
            @Field("peruri_pay_username") String peruriPayUsername,
            @Field("peruri_pay_fullname") String peruriPayFullName,
            @Field("peruri_pay_password") String peruriPayPassword,
            @Field("peruri_pay_phoneno") String peruriPayPhoneNo,
            @Field("status_registrasi") String statusRegistrasi,
            @Field("status_integrasi") String statusIntegrasi
    );

    @FormUrlEncoded
    @POST("updatejamaah/user_id/{userId}/access_token/{accessToken}/location/platform/{platform}")
    Call<ResponseBody> jamaahUpdatePeruriPayGagal(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Path("platform") String dashboard,
            @Field("jamaah_id") String jamaahId,
            @Field("status_registrasi") String statusRegistrasi,
            @Field("status_integrasi") String statusIntegrasi
    );

    @GET("agent")
    Call<AgentData> agenGetData(
            @Query("user_id") String userId,
            @Query("access_token") String accessToken,
            @Query("location") String location,
            @Query("platform") String platform,
            @Query("group") String group,
            @Query("agent_id") String agentId,
            @Query("n_item") String nItem,
            @Query("page") String page
    );

    @FormUrlEncoded
    @POST("peruri_signin/user_id/{userId}/access_token/{accessToken}/location/platform/{platform}")
    Call<SignInPPay> agenSignInPPay(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Path("platform") String dashboard,
            @Field("userName") String username
    );

    @FormUrlEncoded
    @POST("transactionInquiry/user_id/{userId}/access_token/{accessToken}/location/platform/{platform}")
    Call<ResponseInquiry> agenPostInquiry(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Path("platform") String dashboard,
            @Field("umroh_id") String umrohId,
            @Field("payment_amount") String paymentAmount,
            @Field("jamaah_id") String jamaahId,
            @Field("role") String role,
            @Field("idTmoney") String idTmoney,
            @Field("idFusion") String idFusion,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("transactionPayment/user_id/{userId}/access_token/{accessToken}/location/platform/{platform}")
    Call<ResponsePayment> agenPostPayment(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Path("platform") String dashboard,
            @Field("umroh_id") String umrohId,
            @Field("payment_amount") String paymentAmount,
            @Field("jamaah_id") String jamaahId,
            @Field("role") String role,
            @Field("idTmoney") String idTmoney,
            @Field("idFusion") String idFusion,
            @Field("token") String token,
            @Field("pin") String pin,
            @Field("transactionID") String transactionID,
            @Field("refNo") String refNo,
            @Field("modelPayment") String modelPayment
    );

    @GET("mutasi")
    Call<ResponseMutasi> agenGetMutasi(
            @Query("user_id") String userId,
            @Query("access_token") String accessToken,
            @Query("location") String location,
            @Query("platform") String platform,
            @Query("n_item") String nItem,
            @Query("page") String page,
            @Query("umroh_id") String umrohId
    );

    @GET("jamaah")
    Call<ListJamaah> agenGetJamaahByIdJamaah(
            @Query("user_id") String userId,
            @Query("access_token") String accessToken,
            @Query("location") String location,
            @Query("platform") String platform,
            @Query("group") String group,
            @Query("n_item") String nItem,
            @Query("page") String page,
            @Query("jamaah_id") String jamaahId
    );

    @FormUrlEncoded
    @POST("peruri_finpay/user_id/{userId}/access_token/{accessToken}/location/platform/{platform}")
    Call<ResoponseTopUp> jamaahPostTopUp(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Path("platform") String dashboard,
            @Field("idTmoney") String idTmoney,
            @Field("idFusion") String idFusion,
            @Field("token") String token,
            @Field("amount") String amount
    );

    @FormUrlEncoded
    @POST("transferInquiry/user_id/{userId}/access_token/{accessToken}/location/platform/mobile")
    Call<ResponseInquiry> transactionPostInquiry(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Field("payment_amount") String paymentAmount,
            @Field("jamaah_id") String jamaahId,
            @Field("role") String role,
            @Field("idTmoney") String idTmoney,
            @Field("idFusion") String idFusion,
            @Field("token") String token,
            @Field("dest_id") String destId,
            @Field("dest_role") String destRole
    );

    @FormUrlEncoded
    @POST("transactionTransfer/user_id/{userId}/access_token/{accessToken}/location/platform/mobile")
    Call<ResponseTransfer> transactionPostTransfer(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Field("dest_id") String destId,
            @Field("dest_role") String destRole,
            @Field("payment_amount") String paymentAmount,
            @Field("jamaah_id") String jamaahId,
            @Field("role") String role,
            @Field("idTmoney") String idTmoney,
            @Field("idFusion") String idFusion,
            @Field("token") String token,
            @Field("pin") String pin,
            @Field("transactionID") String transactionID,
            @Field("refNo") String refNo
    );

    @FormUrlEncoded
    @POST("peruri_changepin/user_id/{userId}/access_token/{accessToken}/location/platform/mobile")
    Call<ResponseChangePIN> changePIN(
            @Path("userId") String userId,
            @Path("accessToken") String accessToken,
            @Field("idTmoney") String idTmoney,
            @Field("idFusion") String idFusion,
            @Field("token") String token,
            @Field("old_pin") String oldPIN,
            @Field("new_pin") String newPIN
    );


    @GET("bank")
    Call<ResponseBank> getBank(
            @Query("bank_id") String bankId,
            @Query("user_id") String userId,
            @Query("access_token") String accessToken,
            @Query("platform") String platform
    );
}


package com.example.user.peruriumroh.etc;

import android.app.Activity;
import android.content.DialogInterface;

public class AlertDialog {
    public static void showAlert(Activity activity, String title, String message){
        android.app.AlertDialog.Builder builder2 = new android.app.AlertDialog.Builder(activity);
        builder2.setTitle(title);
        builder2.setMessage(message);
        builder2.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder2.show();
    }
}

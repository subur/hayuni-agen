package com.example.user.peruriumroh.etc;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class AlertConnectionDialog {

    public static void showConnectionAlert(Activity activity){
        AlertDialog.Builder builder2 = new AlertDialog.Builder(activity);
        builder2.setTitle("Anda sedang offline");
        builder2.setMessage("Periksa koneksi WiFi atau data seluler dan coba lagi");
        builder2.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder2.show();
    }
}

package com.example.user.peruriumroh.feature.agen_profile_changepin3;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertDialog;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.agen_profile_changepin.AgenChangePINActivity;
import com.example.user.peruriumroh.feature.agen_profile_changepin2.AgenChangePIN2Activity;
import com.example.user.peruriumroh.model.ResponseChangePIN;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;
import com.goodiebag.pinview.Pinview;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgenChangePIN3Activity extends AppCompatActivity {

    Pinview pinview;

    String pin,newPIN;

    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();

    Preferences preferences;

    public static Activity act;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agen_change_pin3);

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        act = this;

        preferences = new Preferences(AgenChangePIN3Activity.this);

        progressDialog = new ProgressDialog(AgenChangePIN3Activity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                pin = null;
                newPIN = null;
            } else {
                pin = extras.getString("currentPIN");
                newPIN = extras.getString("newPIN");
            }
        } else {
            pin = (String) savedInstanceState.getSerializable("currentPIN");
            newPIN = (String) savedInstanceState.getSerializable("newPIN");
        }

        pinview = (Pinview)findViewById(R.id.pinview);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
                String retypePIN = pinview.getValue();
                if(retypePIN.equals(newPIN)){
                progressDialog.setMessage("Mengubah PIN...");
                progressDialog.show();

                String newPIN = pinview.getValue();

                Call<ResponseChangePIN> changePIN = apiClientInterface.changePIN(
                        preferences.getUserid(),
                        preferences.getToken(),
                        preferences.getIdTmoney(),
                        preferences.getIdFusion(),
                        preferences.getTokenppay(),
                        pin,
                        newPIN
                );

                changePIN.enqueue(new Callback<ResponseChangePIN>() {
                    @Override
                    public void onResponse(Call<ResponseChangePIN> call, Response<ResponseChangePIN> response) {
                        if(response.isSuccessful()){
                            if(response.code() == 200){
                                if(response.body().getErrorcode().equals("00000")){
                                    if(response.body().getData().getResultCode().equals("0")){
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(),response.body().getData().getResultDesc(),Toast.LENGTH_LONG).show();
                                        finish();
                                        AgenChangePIN2Activity.act.finish();
                                        AgenChangePINActivity.act.finish();
                                    }else if(response.body().getData().getResultCode().equals("CR-017")){
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(),response.body().getData().getResultDesc(),Toast.LENGTH_LONG).show();
                                        finish();
                                        AgenChangePIN2Activity.act.finish();
                                        AgenChangePINActivity.act.finish();
                                    }else if(response.body().getData().getResultCode().equals("GL-007")){
                                        progressDialog.dismiss();
                                        Logout.logout(AgenChangePIN3Activity.this);
                                    }
                                }
                            }else if(response.code() == 401){
                                progressDialog.dismiss();
                                Logout.logout(AgenChangePIN3Activity.this);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseChangePIN> call, Throwable t) {
                        if(t instanceof IOException){
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(),"Connection Error",Toast.LENGTH_SHORT).show();
                            finish();
                            AgenChangePIN2Activity.act.finish();
                            AgenChangePINActivity.act.finish();
                        }else {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(),"Parsing data Error",Toast.LENGTH_SHORT).show();
                            finish();
                            AgenChangePIN2Activity.act.finish();
                            AgenChangePINActivity.act.finish();
                        }
                    }
                });
                }else {
                    AlertDialog.showAlert(AgenChangePIN3Activity.this,"Umrah Safe","PIN tidak sama");
                }
            }
        });
    }
}

package com.example.user.peruriumroh.feature.pay_voucher_sukses;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.model.ListJamaah;
import com.example.user.peruriumroh.model.ResponseBank;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayVoucherSuksesActivity extends AppCompatActivity {

    Button btVoucherOK;
    Preferences preferences;
    TextView tvNamaJamaah, tvAmount, tvDeposit, tvSisaPembayaran;
    ImageView ivQR;
    ImageView ivBy;

    String jamaahId,amount;

    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();

    List<ListJamaah.Data> jamaahData = new ArrayList<ListJamaah.Data>();

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_voucher_sukses);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        preferences = new Preferences(PayVoucherSuksesActivity.this);

        tvNamaJamaah = (TextView)findViewById(R.id.tv_nama_jamaah);
        tvAmount = (TextView)findViewById(R.id.tv_pembayaran_value);
        tvDeposit = (TextView)findViewById(R.id.tv_total_deposit_val);
        tvSisaPembayaran = (TextView)findViewById(R.id.tv_sisa_pembayaran_val);
        ivQR = (ImageView)findViewById(R.id.iv_qr);
        ivBy = (ImageView)findViewById(R.id.iv_by);

        byte[] decodedString = Base64.decode(getIntent().getExtras().getString("SQRC"), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0,decodedString.length);
        ivQR.setImageBitmap(decodedByte);

        if (savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if(extras == null){
                jamaahId = null;
                amount = null;
            }else{
                jamaahId = extras.getString("JamaahId");
                amount = extras.getString("Amount");
            }
        } else {
            jamaahId = (String) savedInstanceState.getSerializable("JamaahId");
            amount = (String) savedInstanceState.getSerializable("Amount");
        }

        Call<ListJamaah> call = apiClientInterface.agenGetJamaahByIdJamaah(
                preferences.getUserid(),
                preferences.getToken(),
                "",
                "mobile",
                "",
                "",
                "",
                jamaahId
        );

        call.enqueue(new Callback<ListJamaah>() {
            @Override
            public void onResponse(Call<ListJamaah> call, Response<ListJamaah> response) {
                if(response.code() == 200 && response.body().getErrorcode().equals("00000")){
                    jamaahData = response.body().getData();
                    tvAmount.setText(CurrencyFormating.setToCurrency(amount));

                    tvNamaJamaah.setText(jamaahData.get(0).getJamaah_name());

                    String deposit = (jamaahData.get(0).getUmroh().get(0).getUmroh_deposit() == null) ? "0" : jamaahData.get(0).getUmroh().get(0).getUmroh_deposit();
                    tvDeposit.setText(CurrencyFormating.setToCurrency(deposit));

                    String debt = (jamaahData.get(0).getUmroh().get(0).getUmroh_debt() == null) ? "0" : String.valueOf(Integer.parseInt(jamaahData.get(0).getUmroh().get(0).getUmroh_debt())-(Integer.parseInt(deposit)));
                    tvSisaPembayaran.setText(CurrencyFormating.setToCurrency(debt));
                }else if(response.code() == 401){
                    Logout.logout(PayVoucherSuksesActivity.this);
                }
            }

            @Override
            public void onFailure(Call<ListJamaah> call, Throwable t) {
                finish();
            }
        });

        btVoucherOK = (Button)findViewById(R.id.bt_voucher_ok);
        btVoucherOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.menu.menu_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

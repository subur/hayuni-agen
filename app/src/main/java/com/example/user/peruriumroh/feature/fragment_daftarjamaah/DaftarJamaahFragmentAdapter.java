package com.example.user.peruriumroh.feature.fragment_daftarjamaah;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.etc.FormatingDate;
import com.example.user.peruriumroh.feature.jamaah_detailjamaah.DetailJamaahActivity;
import com.example.user.peruriumroh.feature.jamaah_listjamaah.ListJamaahAdapter;
import com.example.user.peruriumroh.model.Jamaah;
import com.example.user.peruriumroh.model.ListJamaah;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.support.constraint.Constraints.TAG;

public class DaftarJamaahFragmentAdapter extends RecyclerView.Adapter<DaftarJamaahFragmentAdapter.DaftarJamaahViewHolder> {

    Activity mact;
    List<ListJamaah.Data> allJemaah;

    public DaftarJamaahFragmentAdapter(Activity act,List<ListJamaah.Data> allJemaah)
    {
        mact = act;
        this.allJemaah = allJemaah;
    }

    @NonNull
    @Override
    public DaftarJamaahViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutItem = R.layout.item_daftar_jamaah;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachtoParentImmediately = false;

        View view = inflater.inflate(layoutItem,viewGroup,shouldAttachtoParentImmediately);
        DaftarJamaahFragmentAdapter.DaftarJamaahViewHolder viewHolder = new DaftarJamaahFragmentAdapter.DaftarJamaahViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DaftarJamaahViewHolder daftarJamaahViewHolder, final int i) {
        daftarJamaahViewHolder.tvNama.setText(allJemaah.get(i).getJamaah_name());
        daftarJamaahViewHolder.tvPaketUmroh.setText(allJemaah.get(i).getUmroh().get(0).getProduct_name());

        String strDate = FormatingDate.getDate(allJemaah.get(i).getUmroh().get(0).getProduct_departure());
        daftarJamaahViewHolder.tvTglBerangkat.setText(strDate);

        String deposit = "0";
        if(allJemaah.get(i).getUmroh().get(0).getUmroh_deposit() == null){
            deposit = "0";
        }
        else {
            deposit = allJemaah.get(i).getUmroh().get(0).getUmroh_deposit();
        }
        daftarJamaahViewHolder.tvSaldo.setText(CurrencyFormating.setToCurrency(deposit));
        DaftarJamaahViewHolder.bind(i);

        daftarJamaahViewHolder.llEachJamaah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),String.valueOf(i),Toast.LENGTH_SHORT).show();
                ArrayList<ListJamaah.Data> jemaahList = new ArrayList<ListJamaah.Data>();
                jemaahList.add(allJemaah.get(i));

                Intent intent = new Intent(mact,DetailJamaahActivity.class);
                intent.putExtra("kunci",jemaahList);
                mact.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return allJemaah.size();
    }

    public void updateJamaahList(List<ListJamaah.Data> searchJamaah) {
        this.allJemaah = new ArrayList<ListJamaah.Data>();
        this.allJemaah = searchJamaah;
        notifyDataSetChanged();
    }

    public static class DaftarJamaahViewHolder extends RecyclerView.ViewHolder {

        TextView tvNama,tvPaketUmroh, tvTglBerangkat, tvSaldo;

        LinearLayout llEachJamaah;

        public DaftarJamaahViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = (TextView)itemView.findViewById(R.id.tv_nama_jamaah);
            tvPaketUmroh = (TextView)itemView.findViewById(R.id.tv_paket_umroh);
            tvTglBerangkat = (TextView)itemView.findViewById(R.id.tv_tanggal_berangkat);
            tvSaldo = (TextView)itemView.findViewById(R.id.tv_saldo);

            llEachJamaah = (LinearLayout)itemView.findViewById(R.id.item_each_jamaah);
        }

        public static void bind(int i) {

        }
    }
}

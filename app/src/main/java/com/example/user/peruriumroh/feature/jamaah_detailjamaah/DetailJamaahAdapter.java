package com.example.user.peruriumroh.feature.jamaah_detailjamaah;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.etc.FormatingDate;
import com.example.user.peruriumroh.feature.saldo_ceksaldo.SaldoCekSaldoAdapter;
import com.example.user.peruriumroh.feature.voucher_detail.VoucherDetailActivity;
import com.example.user.peruriumroh.model.ListJamaah;
import com.example.user.peruriumroh.model.ResponseMutasi;

import java.util.List;

public class DetailJamaahAdapter extends RecyclerView.Adapter<DetailJamaahAdapter.DetailJamaahViewHolder> {
    Activity mact;
    List<ResponseMutasi.Data> mutasiJamaah;
    FrameLayout eachMutasi;

    public DetailJamaahAdapter(Activity act, List<ResponseMutasi.Data> mutasiJamaah){
        mact = act;
        this.mutasiJamaah= mutasiJamaah;
    }

    @NonNull
    @Override
    public DetailJamaahViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutItem = R.layout.item_ceksaldodetail;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachtoParentImmediately = false;

        View view = inflater.inflate(layoutItem,viewGroup,shouldAttachtoParentImmediately);
        DetailJamaahAdapter.DetailJamaahViewHolder viewHolder = new DetailJamaahAdapter.DetailJamaahViewHolder(view);

        eachMutasi = (FrameLayout)view.findViewById(R.id.fl_each_mutasi);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final DetailJamaahViewHolder detailJamaahViewHolder, int i) {
        //detailJamaahViewHolder.bind(i);
        detailJamaahViewHolder.tvDate.setText(FormatingDate.getDate(mutasiJamaah.get(i).getPayment_date()));
        detailJamaahViewHolder.tvValue.setText(CurrencyFormating.setToCurrency(mutasiJamaah.get(i).getPayment_amount()));
        detailJamaahViewHolder.tvFrom.setText(mutasiJamaah.get(i).getRole());

        eachMutasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
//                Toast.makeText(mact,i,Toast.LENGTH_LONG).show();
                Intent intent = new Intent(mact,VoucherDetailActivity.class);
                intent.putExtra("mutasi_sqrc",mutasiJamaah.get(detailJamaahViewHolder.getAdapterPosition()).getSqrc());
                intent.putExtra("mutasi_value",mutasiJamaah.get(detailJamaahViewHolder.getAdapterPosition()).getPayment_amount());
                intent.putExtra("mutasi_date",mutasiJamaah.get(detailJamaahViewHolder.getAdapterPosition()).getPayment_date());
                intent.putExtra("mutasi_voucher_id",mutasiJamaah.get(detailJamaahViewHolder.getAdapterPosition()).getVID());
                intent.putExtra("JamaahId",mutasiJamaah.get(detailJamaahViewHolder.getAdapterPosition()).getId());
                intent.putExtra("mutasi_bank_id",mutasiJamaah.get(detailJamaahViewHolder.getAdapterPosition()).getDealer().get(0).getBank_id());
                mact.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mutasiJamaah.size();
    }

    public class DetailJamaahViewHolder extends RecyclerView.ViewHolder {

        TextView tvDate,tvValue, tvFrom;

        public DetailJamaahViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDate = (TextView)itemView.findViewById(R.id.item_mutasi_tanggal);
            tvValue = (TextView)itemView.findViewById(R.id.item_mutasi_value);
            tvFrom = (TextView)itemView.findViewById(R.id.item_mutasi_from);
        }

        public void bind(int i) {
        }
    }
}

package com.example.user.peruriumroh.feature.pembayaranhome;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.user.peruriumroh.R;

public class PembayaranActivity extends AppCompatActivity {

    private DrawerLayout dl;
    private ActionBarDrawerToggle abdt;

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembayaran);

        ac = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        dl = (DrawerLayout)findViewById(R.id.drawer_pembayaran_umroh);
        abdt = new ActionBarDrawerToggle(this,dl,R.string.Open,R.string.Close);
        abdt.setDrawerIndicatorEnabled(true);

        dl.addDrawerListener(abdt);
        abdt.syncState();

        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);
        nav_view.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        menuItem.setChecked(true);
                        dl.closeDrawers();
                        int id = menuItem.getItemId();
                        switch (id){
                            case R.id.item_paketumroh:
                                Toast.makeText(getApplicationContext(),"Paket Umroh",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.item_Pembayaran:
                                Toast.makeText(getApplicationContext(),"Pembayaran",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.item_daftarjamaah:
                                Toast.makeText(getApplicationContext(),"Daftar Jamaah",Toast.LENGTH_SHORT).show();
                                break;
                        }

                        return false;
                    }
                }
        );
    }
}

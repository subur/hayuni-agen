package com.example.user.peruriumroh.feature.home;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.MainDrawer.DrawerActivity;
import com.example.user.peruriumroh.feature.agen_profile_setting.AgenProfileSettingActivity;
import com.example.user.peruriumroh.feature.fragment_daftarjamaah.DaftarJamaahFragmentAdapter;
import com.example.user.peruriumroh.feature.landing.WelcomeActivity;
import com.example.user.peruriumroh.feature.login.LoginActivity;
import com.example.user.peruriumroh.model.ListJamaah;
import com.example.user.peruriumroh.model.SignInPPay;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class HomeActivity extends AppCompatActivity {

    RelativeLayout menu_paket,menu_pembayaran,menu_daftarjamaah;
    TextView tvNamaUser;
    Preferences preferences;
    ImageButton ibSetting;
    CircleImageView civProfile;

    ProgressDialog progressBar;

    public static TextView tvSaldo;

    public static Activity ac;

    public static List<ListJamaah.Data> allJamaah = new ArrayList<>();

    //NETWORK INTERFACE
    ApiClient clientInterface = new ApiClient();
    ApiClientInterface apiClient = clientInterface.clientInterface();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ac = this;

        preferences = new Preferences(HomeActivity.this);

        //PROGRESS BAR
        progressBar = new ProgressDialog(HomeActivity.this);
        progressBar.setCancelable(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        checkToken();

        getJamaah();

        signINPeruri();

        //Inisiasi
        menu_paket = (RelativeLayout)findViewById(R.id.menu_paket_umroh);
        menu_pembayaran = (RelativeLayout)findViewById(R.id.menu_pembayaran);
        menu_daftarjamaah = (RelativeLayout)findViewById(R.id.menu_daftar_jamaah);

        menu_paket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openIntent(1);
            }
        });

        menu_pembayaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openIntent(2);
            }
        });

        menu_daftarjamaah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openIntent(3);
            }
        });

        tvSaldo = (TextView)findViewById(R.id.tv_saldo);
        if(preferences.getBalance() == null){

        }else {
            tvSaldo.setText(CurrencyFormating.setToCurrency(preferences.getBalance()));
        }

        civProfile = (CircleImageView)findViewById(R.id.civ_profile);
        Glide.with(this).load("http://13.228.32.47/umroh/api/" + preferences.getImages()).apply(RequestOptions.bitmapTransform(new RoundedCorners(10))).into(civProfile);

        tvNamaUser = (TextView)findViewById(R.id.tv_username);
        tvNamaUser.setText(preferences.getName());

        ibSetting = (ImageButton)findViewById(R.id.ib_setting);
        ibSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(HomeActivity.this,AgenProfileSettingActivity.class);
                startActivity(intent);
            }
        });
    }

    private void signINPeruri() {
        Call<SignInPPay> call = apiClient.agenSignInPPay(
                preferences.getUserid(),
                preferences.getToken(),
                "mobile",
                preferences.getEmail()
        );
        call.enqueue(new Callback<SignInPPay>(){
            @Override
            public void onResponse(Call<SignInPPay> call, Response<SignInPPay> response){
                if(response.code() == 200){
                    if(response.body().getErrorcode().equals("00000")){
                        if(response.body().getData() != null){
                            if(response.body().getData().getResultCode().equals("0")){
                                preferences.setIdTmoney(response.body().getData().getUser().getIdTmoney());
                                preferences.setIdFusion(response.body().getData().getUser().getIdFusion());
                                preferences.setTokenppay(response.body().getData().getUser().getToken());
                                preferences.setBalance(response.body().getData().getUser().getBalance());

                                if(preferences.getBalance() != null){
                                    tvSaldo.setText(CurrencyFormating.setToCurrency(preferences.getBalance()));
                                }
                            }else {
                                Logout.logout(HomeActivity.this);
                            }
                        }else {
                            Logout.logout(HomeActivity.this);
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),response.body().getData().getResultDesc(),Toast.LENGTH_LONG).show();
                        Logout.logout(HomeActivity.this);
                    }
                }else if(response.code() == 401){
                    Logout.logout(HomeActivity.this);
                }
            }

            @Override
            public void onFailure(Call<SignInPPay> call, Throwable t) {
//                Logout.logout(HomeActivity.this);
                AlertConnectionDialog.showConnectionAlert(HomeActivity.this);
            }
        });
    }

    private void getJamaah(){
        progressBar.setMessage("Tunggu Sebentar...");
        progressBar.show();
        ApiClient apiClient = new ApiClient();
        ApiClientInterface apiClientInterface = apiClient.clientInterface();

        String idUser = preferences.getUserid();
        String token = preferences.getToken();
        String agentId = preferences.getAgent_id();
        String platform = "mobile";

        Call<ListJamaah> call = apiClientInterface.agenGetJamaah(idUser,token,"",platform,"","","",agentId);
        call.enqueue(new Callback<ListJamaah>(){
            @Override
            public void onResponse(Call<ListJamaah> call, Response<ListJamaah> response) {
                if(response.code() == 200){
                    if(response.body().getData() == null){
                        progressBar.dismiss();
                    }else {
                        allJamaah = response.body().getData();
                        List<String> nullPackage = new ArrayList<>();
                        for (int i = 0; i < allJamaah.size();i++){
                            if(allJamaah.get(i).getUmroh() == null){
                                //allProduk.remove(i);
                                nullPackage.add(String.valueOf(i));
                                Log.d("TAG: " + i +"#"+ allJamaah.get(i).getJamaah_id(), "KOSONG");
                            }
                        }
                        Log.d("TAG : Size", String.valueOf(nullPackage.size()));
                        if(nullPackage.size() > 0){
                            for(int i = nullPackage.size()-1; i >= 0; i--){
                                allJamaah.remove(Integer.parseInt(nullPackage.get(i)));
                            }
                        }
                        Log.d("TAG : jamaah akhir", String.valueOf(allJamaah.size()));
                        progressBar.dismiss();
                    }
                }else if(response.code() == 401){
                    Logout.logout(HomeActivity.this);
                    progressBar.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ListJamaah> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }

    private void checkToken() {
        if(preferences.getToken() == null && !preferences.getToken().equals("")){
            finish();
            Intent intent = new Intent(HomeActivity.this,WelcomeActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        allJamaah = null;
        getJamaah();
        tvNamaUser.setText(preferences.getName());
        if(!TextUtils.isEmpty(preferences.getBalance())){
            tvSaldo.setText(CurrencyFormating.setToCurrency(preferences.getBalance()));
        }
        Glide.with(this).load("http://13.228.32.47/umroh/api/" + preferences.getImages()).apply(RequestOptions.bitmapTransform(new RoundedCorners(10))).into(civProfile);
        signINPeruri();
    }

    void openIntent(int menuChooser){
        Intent i = new Intent(HomeActivity.this, DrawerActivity.class);
        i.putExtra("INT_PAGE", menuChooser);
        startActivity(i);
    }

}

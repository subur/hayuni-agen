package com.example.user.peruriumroh.model;

import java.io.Serializable;
import java.util.List;

public class ResponseMutasi implements Serializable {
    public List<Data> data;

    public String errorcode;

    public String errormsg;

    public List<Data> getData() {
        return data;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public class Data implements Serializable{
        private List<Dealer> dealer;

        private String VID;

        private String umroh_id;

        private String role;

        private String payment_desc;

        private String payment_id;

        private String response;

        private String payment_amount;

        private String sqrc;

        private String id;

        private String payment_date;

        public String getVID() {
            return VID;
        }

        public String getUmroh_id() {
            return umroh_id;
        }

        public String getRole() {
            return role;
        }

        public String getPayment_desc() {
            return payment_desc;
        }

        public String getPayment_id() {
            return payment_id;
        }

        public String getResponse() {
            return response;
        }

        public String getPayment_amount() {
            return payment_amount;
        }

        public String getSqrc() {
            return sqrc;
        }

        public String getId() {
            return id;
        }

        public String getPayment_date() {
            return payment_date;
        }

        public List<Dealer> getDealer() {
            return dealer;
        }
    }

    public class Dealer {

        private String bank_id;

        public String getBank_id() {
            return bank_id;
        }
    }
}

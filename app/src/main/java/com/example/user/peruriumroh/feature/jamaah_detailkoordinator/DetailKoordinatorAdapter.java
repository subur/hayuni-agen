package com.example.user.peruriumroh.feature.jamaah_detailkoordinator;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.jamaah_detailjamaah.DetailJamaahAdapter;

public class DetailKoordinatorAdapter extends RecyclerView.Adapter<DetailKoordinatorAdapter.DetailKoordinatorViewHolder> {
    int numberRv = 0;
    public DetailKoordinatorAdapter(int numberRv){this.numberRv = numberRv;}

    @NonNull
    @Override
    public DetailKoordinatorViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutItem = R.layout.item_ceksaldodetail;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachtoParentImmediately = false;

        View view = inflater.inflate(layoutItem,viewGroup,shouldAttachtoParentImmediately);
        DetailKoordinatorAdapter.DetailKoordinatorViewHolder viewHolder = new DetailKoordinatorAdapter.DetailKoordinatorViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DetailKoordinatorViewHolder detailKoordinatorViewHolder, int i) {
        detailKoordinatorViewHolder.bind(i);
    }

    @Override
    public int getItemCount() {
        return numberRv;
    }

    public class DetailKoordinatorViewHolder extends RecyclerView.ViewHolder {
        public DetailKoordinatorViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public void bind(int i) {
        }
    }
}

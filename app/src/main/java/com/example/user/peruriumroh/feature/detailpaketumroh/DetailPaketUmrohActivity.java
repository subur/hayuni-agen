package com.example.user.peruriumroh.feature.detailpaketumroh;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.CheckNull;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.etc.FormatingDate;
import com.example.user.peruriumroh.etc.LocaleID;
import com.example.user.peruriumroh.feature.registerjamaah1.RegisterJamaah1Activity;
import com.example.user.peruriumroh.feature.registerjamaahbranch.RegisterJamaahBranchActivity;
import com.example.user.peruriumroh.model.PaketUmroh;
import com.example.user.peruriumroh.model.Produk;
import com.example.user.peruriumroh.model.userData;
import com.example.user.peruriumroh.preference.Preferences;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import co.ceryle.radiorealbutton.RadioRealButton;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;

public class DetailPaketUmrohActivity extends AppCompatActivity {

    ArrayList<Produk.Data> objects;
    ImageView ivPoster;
    TextView tvTitlePaket,tvTanggal,tvBulanTahun,tvKuotaTersisa, tvKuotaTotal, tvFeatureValue, tvNamaBersama, tvKeteranganBersama, tvTanggalKeberangkatan, tvPenginapanKeberangkatan, tvKontenKajian;
    TextView tvDp, tvBulanan;
    RadioGroup radioGroup;
    RadioRealButtonGroup rrbGroup;
    Button btRegisterJamaah;
    Preferences preferences;
    String paketId,produkId;
    Map<Integer,Integer> radioMap = new HashMap<>();

    public static Activity ac;

    int buttonPosition = 0;
    int radioPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_paket_umroh);

        ac = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        preferences = new Preferences(DetailPaketUmrohActivity.this);

        objects = (ArrayList<Produk.Data>)getIntent().getSerializableExtra("kunci");

        ivPoster = (ImageView)findViewById(R.id.iv_poster);
        tvTitlePaket = (TextView)findViewById(R.id.tv_title_paket);
        tvTanggal = (TextView)findViewById(R.id.tv_tanggal);
        tvBulanTahun = (TextView)findViewById(R.id.tv_bulan_tahun);
        tvKuotaTersisa = (TextView)findViewById(R.id.tv_kuotatersisa);
        tvKuotaTotal = (TextView)findViewById(R.id.tv_kuotatotal);
        tvFeatureValue = (TextView)findViewById(R.id.tv_feature_value);
        tvNamaBersama = (TextView)findViewById(R.id.tv_nama_bersama);
        tvKeteranganBersama = (TextView)findViewById(R.id.tv_keterangan_bersama);
        tvTanggalKeberangkatan = (TextView)findViewById(R.id.tv_tanggal_keberangkatan);
        tvPenginapanKeberangkatan = (TextView)findViewById(R.id.tv_Penginapan_keberangkatan);
        tvKontenKajian = (TextView)findViewById(R.id.tv_konten_kajian);
        tvDp = (TextView)findViewById(R.id.tv_dp);
        tvBulanan = (TextView)findViewById(R.id.tv_bulanan);
        radioGroup = (RadioGroup)findViewById(R.id.rg_paket);
        btRegisterJamaah = (Button)findViewById(R.id.bt_register_jamaah);

        Glide.with(this).load("http://13.228.32.47/umroh/api/"+objects.get(0).getProduct_image()).apply(RequestOptions.bitmapTransform(new RoundedCorners(10))).into(ivPoster);
        tvTitlePaket.setText(objects.get(0).getProduct_name());

        tvKuotaTersisa.setText(objects.get(0).getQuota().get(0).getQuota_available() + " Slot");

        String quotaBook = "0";
        if(objects.get(0).getQuota().get(0).getQuota_book() == null){
            quotaBook = "0";
        }else {
            quotaBook = objects.get(0).getQuota().get(0).getQuota_book();
        }
        tvKuotaTotal.setText(String.valueOf(Integer.parseInt(objects.get(0).getQuota().get(0).getQuota_available()) + Integer.parseInt(quotaBook)) + " Slot");

        Log.d("TAG", "onCreate: Kuota" + objects.get(0).getQuota().get(0).getQuota_available());
        tvFeatureValue.setText(objects.get(0).getProduct_desc());
        tvNamaBersama.setText(objects.get(0).getMentor().get(0).getMentor_name());
        tvKeteranganBersama.setText(objects.get(0).getMentor().get(0).getMentor_desc());

        String strDate = FormatingDate.getDate(objects.get(0).getProduct_departure());
        String[] splitDate = strDate.split(" ");
        tvTanggal.setText(splitDate[0]);
        tvBulanTahun.setText(splitDate[1] + " '" +splitDate[2].substring(2));

        String strDateCompleteMonth = FormatingDate.getDateCompleteMonth(objects.get(0).getProduct_departure());
        tvTanggalKeberangkatan.setText(strDateCompleteMonth);
        tvPenginapanKeberangkatan.setText(objects.get(0).getProduct_rute_desc());
        tvKontenKajian.setText(objects.get(0).getProduct_desc());

        for (int i = 0; i < objects.get(0).getPaket().size(); i++) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(objects.get(0).getPaket().get(i).getPackage_name() + " - " + CurrencyFormating.setToCurrency(objects.get(0).getPaket().get(i).getPackage_price()));
            radioButton.setId(Integer.parseInt(objects.get(0).getPaket().get(i).getPackage_id()));
            radioButton.setTextSize(20);
            radioButton.setTextColor(getResources().getColor(R.color.colorAccent));
            if(i == 0){
                radioButton.setChecked(true);
            }
            radioMap.put(Integer.parseInt(objects.get(0).getPaket().get(i).getPackage_id()),i);
            radioGroup.addView(radioButton);
        }

        calculateUmroh();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int i = radioGroup.getCheckedRadioButtonId();
                radioPosition = radioMap.get(i);

                calculateUmroh();
            }
        });

        rrbGroup = (RadioRealButtonGroup)findViewById(R.id.bt_group);
        rrbGroup.setOnPositionChangedListener(new RadioRealButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(RadioRealButton button, int currentPosition, int lastPosition) {
                buttonPosition = currentPosition;

                calculateUmroh();
            }
        });

        btRegisterJamaah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                produkId = objects.get(0).getProduct_id();
                paketId = String.valueOf(radioGroup.getCheckedRadioButtonId());
                preferences.setProdukId(produkId);
                preferences.setPaketId(paketId);
                Intent intent = new Intent(DetailPaketUmrohActivity.this,RegisterJamaahBranchActivity.class);
                startActivity(intent);
            }
        });
    }

    private void calculateUmroh() {
        String uangmuka = CurrencyFormating.setToCurrency(objects.get(0).getPaket().get(radioPosition).getDp());

        tvDp.setText(uangmuka);

        Integer costwDp = Integer.parseInt(objects.get(0).getPaket().get(radioPosition).getPackage_price()) - Integer.parseInt(objects.get(0).getPaket().get(radioPosition).getDp());

        if (buttonPosition == 0){
            String montly = String.valueOf(costwDp / 6);
            tvBulanan.setText(CurrencyFormating.setToCurrency(montly));
        }else if(buttonPosition == 1){
            String montly = String.valueOf(costwDp / 12);
            tvBulanan.setText(CurrencyFormating.setToCurrency(montly));
        }else if(buttonPosition == 2){
            String montly = String.valueOf(costwDp / 24);
            tvBulanan.setText(CurrencyFormating.setToCurrency(montly));
        }else if(buttonPosition == 3){
            String montly = String.valueOf(costwDp / 36);
            tvBulanan.setText(CurrencyFormating.setToCurrency(montly));
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_close:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

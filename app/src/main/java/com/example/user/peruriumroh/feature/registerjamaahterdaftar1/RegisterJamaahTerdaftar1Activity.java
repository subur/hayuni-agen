package com.example.user.peruriumroh.feature.registerjamaahterdaftar1;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.densowave.dwqrkit.DWQRKit;
import com.densowave.dwqrkit.DWQRScanMode;
import com.densowave.dwqrkit.DWQRScanView;
import com.densowave.dwqrkit.listener.DWQRCodeNoticeListener;
import com.densowave.dwqrkit.listener.DWQRLicenseListener;
import com.densowave.dwqrkit.listener.DWQRScanViewCreateListener;
import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.registerjamaahterdaftar2.RegisterJamaahTerdaftar2Activity;
import com.example.user.peruriumroh.feature.saldo_scanSQRC.SaldoScanSQRCActivity;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class RegisterJamaahTerdaftar1Activity extends AppCompatActivity implements ViewTreeObserver.OnGlobalLayoutListener,
        DWQRCodeNoticeListener,DWQRScanViewCreateListener,DWQRLicenseListener,android.view.View.OnClickListener{

    protected static final String TAG_CAMERA_FRAGMENT = "camera";
    private int REQUEST_CODE_CAMERA_PERMISSION = 0x01;
    private FrameLayout fl_camera;
    private DWQRScanView preview;
    private DWQRKit dwqrKit;

    public static Activity ac;

    Integer openStatus = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_jamaah_terdaftar1);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        setupSqrc(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupSqrc(Bundle savedInstanceState) {
        dwqrKit = DWQRKit.sharedManager();
        dwqrKit.isVibrationEnable = false;
        dwqrKit.isAudioEnable = false;
        dwqrKit.isFlashEnable = false;
        dwqrKit.isServerConnectionEnable = true;
        dwqrKit.isLogoDisplayEnable = false;
        dwqrKit.setMode(DWQRScanMode.DWQR_NOMALMODE.getValue());

        if(Build.VERSION.SDK_INT >= 23){
            if(PermissionChecker.checkSelfPermission(RegisterJamaahTerdaftar1Activity.this,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(RegisterJamaahTerdaftar1Activity.this,new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                },REQUEST_CODE_CAMERA_PERMISSION);
            }
        }

        if(savedInstanceState == null){
            fl_camera = (FrameLayout)findViewById(R.id.frameScanSQRC);
            fl_camera.getViewTreeObserver().addOnGlobalLayoutListener(this);
        }

        AssetManager assetManager = this.getAssets();
        String filePath = this.getApplicationContext().getFilesDir().toString() + "/license.txt";
        File file = new File(filePath);

        try {
            InputStream in = assetManager.open("license.txt");
            OutputStream out = new BufferedOutputStream(new FileOutputStream(file));

            copyFile(in,out);
            in.close();
            out.flush();
            out.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[8196];
        int read;
        while ((read = in.read(buffer)) != -1){
            out.write(buffer,0,read);
        }
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onGlobalLayout() {
        preview = new DWQRScanView();
        fl_camera.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        preview.setOnQRCodeFoundNoticeListener(this);
        preview.setDwQrScanViewCreateListener(this);
        preview.setDwQrLicenseListener(this);
        getSupportFragmentManager().beginTransaction().add(R.id.frameScanSQRC,preview,TAG_CAMERA_FRAGMENT).commit();
    }

    @Override
    public void onNoticeDecodeResult(String decodeData, String codeType, byte[] bytes, int i, String decodePrivateData, int i1) {
        if(openStatus == 0) {

            openStatus = 1;

            String[] splitPublic = decodeData.split("\n");
            Intent intent = new Intent(RegisterJamaahTerdaftar1Activity.this,RegisterJamaahTerdaftar2Activity.class);
            intent.putExtra("nama",splitPublic[0]);
            intent.putExtra("alamat",splitPublic[1]);
            intent.putExtra("notelp",splitPublic[2]);
            intent.putExtra("nohp",splitPublic[3]);
            intent.putExtra("ktp",decodePrivateData);
            startActivityForResult(intent,1);
        }
    }

    @Override
    public void onNoticeLicenseExpired(int i) {

    }

    @Override
    public void onCreateScanView(FrameLayout frameLayout) {
        fl_camera = frameLayout;
        preview.readLicenseFile();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == 1){
            openStatus = 0;
        }
    }
}

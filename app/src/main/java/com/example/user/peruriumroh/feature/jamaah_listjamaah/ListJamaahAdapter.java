package com.example.user.peruriumroh.feature.jamaah_listjamaah;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.saldo_ceksaldo.SaldoCekSaldoAdapter;

public class ListJamaahAdapter extends RecyclerView.Adapter<ListJamaahAdapter.ListJamaahViewHolder> {

    int numberRv = 0;
    public ListJamaahAdapter(int numberRv){this.numberRv = numberRv;}

    @NonNull
    @Override
    public ListJamaahViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutItem = R.layout.item_daftar_jamaah;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachtoParentImmediately = false;

        View view = inflater.inflate(layoutItem,viewGroup,shouldAttachtoParentImmediately);
        ListJamaahAdapter.ListJamaahViewHolder viewHolder = new ListJamaahAdapter.ListJamaahViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListJamaahViewHolder listJamaahViewHolder, int i) {
        listJamaahViewHolder.bind(i);
    }

    @Override
    public int getItemCount() {
        return numberRv;
    }

    class ListJamaahViewHolder extends RecyclerView.ViewHolder {

        public ListJamaahViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        void bind(int i){

        }
    }
}

package com.example.user.peruriumroh.feature.paketumroh;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.user.peruriumroh.R;

public class PaketUmrohActivity extends AppCompatActivity {

    PaketUmrohAdapter adapterPaketUmroh;
    RecyclerView rvPaketUmroh;

    private DrawerLayout dl;
    private ActionBarDrawerToggle abdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paket_umroh);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        dl = (DrawerLayout)findViewById(R.id.drawer_paket_umroh);
        abdt = new ActionBarDrawerToggle(this,dl,R.string.Open,R.string.Close);
        abdt.setDrawerIndicatorEnabled(true);

        dl.addDrawerListener(abdt);
        abdt.syncState();

        rvPaketUmroh = (RecyclerView) findViewById(R.id.rv_paket_umroh);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvPaketUmroh.setLayoutManager(layoutManager);

        rvPaketUmroh.setHasFixedSize(true);

        adapterPaketUmroh = new PaketUmrohAdapter(100);
        rvPaketUmroh.setAdapter(adapterPaketUmroh);

        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);
        nav_view.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        menuItem.setChecked(true);
                        dl.closeDrawers();
                        int id = menuItem.getItemId();
                        switch (id){
                            case R.id.item_paketumroh:
                                Toast.makeText(getApplicationContext(),"Paket Umroh",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.item_Pembayaran:
                                Toast.makeText(getApplicationContext(),"Pembayaran",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.item_daftarjamaah:
                                Toast.makeText(getApplicationContext(),"Daftar Jamaah",Toast.LENGTH_SHORT).show();
                                break;
                        }

                        return false;
                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_paketumroh:
                Toast.makeText(getApplicationContext(),"PAKET UMROH CLICKED", Toast.LENGTH_SHORT).show();
                return true;
        }
        return abdt.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }
}

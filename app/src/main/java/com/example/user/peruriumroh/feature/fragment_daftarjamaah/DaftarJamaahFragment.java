package com.example.user.peruriumroh.feature.fragment_daftarjamaah;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.BottomSheetConnectionDialog;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.home.HomeActivity;
import com.example.user.peruriumroh.feature.login.LoginActivity;
import com.example.user.peruriumroh.model.ListJamaah;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class DaftarJamaahFragment extends Fragment {
    RecyclerView rv_list_jamaah;
    DaftarJamaahFragmentAdapter daftarJamaahAdapter;
    Preferences preferences;
    RelativeLayout rlEmpty;

    EditText etCariJamaah;

    List<ListJamaah.Data> allJamaah = new ArrayList<>();

    public DaftarJamaahFragment(){
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Daftar Jamaah");

        View rootView = inflater.inflate(R.layout.fragment_daftar_jamaah, container, false);

        //VIEW VARIABLE

        rlEmpty = (RelativeLayout)rootView.findViewById(R.id.rl_empty);

        rv_list_jamaah = (RecyclerView)rootView.findViewById(R.id.rv_list_jamaah);

        preferences = new Preferences(this.getActivity());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rv_list_jamaah.setLayoutManager(layoutManager);

        rv_list_jamaah.setHasFixedSize(true);

        getJamaah();

        etCariJamaah = (EditText)rootView.findViewById(R.id.et_cari_jamaah);
        etCariJamaah.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String input = s.toString().toLowerCase();

                List<ListJamaah.Data> searchJamaah = new ArrayList<ListJamaah.Data>();

                if(input.length() > 0){
                    Log.d(TAG, "onTextChanged: Masuk > 0");
                    for(int i = 0;i<allJamaah.size();i++){
                        if(allJamaah.get(i).getJamaah_name().toLowerCase().contains(input)){
                            searchJamaah.add(allJamaah.get(i));
                        }
                    }
                    daftarJamaahAdapter.updateJamaahList(searchJamaah);
                }else {
                    daftarJamaahAdapter.updateJamaahList(allJamaah);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    private void getJamaah() {
        ApiClient apiClient = new ApiClient();
        ApiClientInterface apiClientInterface = apiClient.clientInterface();

        String idUser = preferences.getUserid();
        String token = preferences.getToken();
        String agentId = preferences.getAgent_id();
        String platform = "mobile";

        Call<ListJamaah> call = apiClientInterface.agenGetJamaah(idUser,token,"",platform,"","","",agentId);
        call.enqueue(new Callback<ListJamaah>() {
            @Override
            public void onResponse(Call<ListJamaah> call, Response<ListJamaah> response) {
                Log.d(TAG, "onResponse: OK");
                if(response.code() == 200){
                    Log.d(TAG, "onResponse: OK");
                    if(response.body().getData() == null){
                        rlEmpty.setVisibility(View.VISIBLE);
                    }else {
                        rv_list_jamaah.setVisibility(View.VISIBLE);
                        allJamaah = response.body().getData();
                        List<String> nullPackage = new ArrayList<>();
                        for (int i = 0; i < allJamaah.size();i++){
                            if(allJamaah.get(i).getUmroh() == null){
                                //allProduk.remove(i);
                                nullPackage.add(String.valueOf(i));
                                Log.d("TAG: " + i +"#"+ allJamaah.get(i).getJamaah_id(), "KOSONG");
                            }
                        }
                        Log.d("TAG : Size", String.valueOf(nullPackage.size()));
                        if(nullPackage.size() > 0){
                            for(int i = nullPackage.size()-1; i >= 0; i--){
                                allJamaah.remove(Integer.parseInt(nullPackage.get(i)));
                            }
                        }
                        Log.d("TAG : Jamaah akhir", String.valueOf(allJamaah.size()));
                        daftarJamaahAdapter = new DaftarJamaahFragmentAdapter(getActivity(),allJamaah);
                        rv_list_jamaah.setAdapter(daftarJamaahAdapter);

                        daftarJamaahAdapter.notifyDataSetChanged();
                    }
                }else if(response.code() == 401){
                    Logout.logout(getActivity());
                }
            }

            @Override
            public void onFailure(Call<ListJamaah> call, Throwable t) {
                BottomSheetConnectionDialog bottomSheet = new BottomSheetConnectionDialog();
                bottomSheet.show(getFragmentManager(),"bottomConnection");
            }
        });
    }
}

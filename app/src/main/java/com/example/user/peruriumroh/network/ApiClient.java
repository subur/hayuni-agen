package com.example.user.peruriumroh.network;

import com.example.user.peruriumroh.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    public ApiClient(){

    }

    public ApiClientInterface clientInterface(){
        //okHttpClient builder
        OkHttpClient.Builder okHttpClientBuilder =  new OkHttpClient.Builder();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        if(BuildConfig.DEBUG){
            okHttpClientBuilder.addInterceptor(logging);
        }

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://13.228.32.47/umroh/api/b1/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build());

        Retrofit retrofit = builder.build();
        ApiClientInterface apiClient =  retrofit.create(ApiClientInterface.class);
        return apiClient;
    }
}

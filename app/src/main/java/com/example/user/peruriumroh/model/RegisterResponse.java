package com.example.user.peruriumroh.model;

public class RegisterResponse
{
    private Data data;

    private String errorcode;

    private String errormsg;

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    public String getErrorcode ()
    {
        return errorcode;
    }

    public void setErrorcode (String errorcode)
    {
        this.errorcode = errorcode;
    }

    public String getErrormsg ()
    {
        return errormsg;
    }

    public void setErrormsg (String errormsg)
    {
        this.errormsg = errormsg;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", errorcode = "+errorcode+", errormsg = "+errormsg+"]";
    }

    public class Data
    {
        private String umroh_id;

        private String jamaah_id;

        private String user_id;

        private String paspor_id;

        private String peruri_pay_id;

        private String sqrc;

        private String sqrc_id;

        public String getUmroh_id ()
        {
            return umroh_id;
        }

        public void setUmroh_id (String umroh_id)
        {
            this.umroh_id = umroh_id;
        }

        public String getJamaah_id ()
        {
            return jamaah_id;
        }

        public void setJamaah_id (String jamaah_id)
        {
            this.jamaah_id = jamaah_id;
        }

        public String getUser_id ()
        {
            return user_id;
        }

        public void setUser_id (String user_id)
        {
            this.user_id = user_id;
        }

        public String getPaspor_id ()
        {
            return paspor_id;
        }

        public void setPaspor_id (String paspor_id)
        {
            this.paspor_id = paspor_id;
        }

        public String getPeruri_pay_id ()
        {
            return peruri_pay_id;
        }

        public void setPeruri_pay_id (String peruri_pay_id)
        {
            this.peruri_pay_id = peruri_pay_id;
        }

        public String getSqrc ()
        {
            return sqrc;
        }

        public void setSqrc (String sqrc)
        {
            this.sqrc = sqrc;
        }

        public String getSqrc_id ()
        {
            return sqrc_id;
        }

        public void setSqrc_id (String sqrc_id)
        {
            this.sqrc_id = sqrc_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [umroh_id = "+umroh_id+", jamaah_id = "+jamaah_id+", user_id = "+user_id+", paspor_id = "+paspor_id+", peruri_pay_id = "+peruri_pay_id+", sqrc = "+sqrc+", sqrc_id = "+sqrc_id+"]";
        }
    }
}
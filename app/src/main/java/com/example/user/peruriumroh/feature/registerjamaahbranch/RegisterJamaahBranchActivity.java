package com.example.user.peruriumroh.feature.registerjamaahbranch;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.registerjamaahbaru.RegisterJamaahBaruActivity;
import com.example.user.peruriumroh.feature.registerjamaahterdaftar1.RegisterJamaahTerdaftar1Activity;

public class RegisterJamaahBranchActivity extends AppCompatActivity {

    Button btJamaahBaru,btJamaahLama;
    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_jamaah_branch);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        //Button Jamaah Baru
        btJamaahBaru = (Button)findViewById(R.id.bt_jamaah_baru);
        btJamaahBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterJamaahBranchActivity.this,RegisterJamaahBaruActivity.class);
                startActivity(intent);
            }
        });

        //Button Jamaah Lama
        btJamaahLama = (Button)findViewById(R.id.bt_jamaah_lama);
        btJamaahLama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(RegisterJamaahBranchActivity.this,RegisterJamaahTerdaftar1Activity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.example.user.peruriumroh.model;

public class ResponsePayment {
    private Data data;

    private String errorcode;

    private String errormsg;

    public Data getData() {
        return data;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public class Data {
        private Peruripayment peruripayment;

        private String sqrc;

        private String payment;

        public Peruripayment getPeruripayment() {
            return peruripayment;
        }

        public String getPayment() {
            return payment;
        }

        public String getSqrc() {
            return sqrc;
        }
    }

    public class Peruripayment {
        public String getLastBalance() {
            return lastBalance;
        }

        public String getTimeStamp() {
            return timeStamp;
        }

        public String getRefNo() {
            return refNo;
        }

        public String getFeeAmount() {
            return feeAmount;
        }

        public String getTotalAmount() {
            return totalAmount;
        }

        public String getDestName() {
            return destName;
        }

        public String getAmount() {
            return amount;
        }

        public String getDestCode() {
            return destCode;
        }

        public String getResultCode() {
            return resultCode;
        }

        public String getResultDesc() {
            return resultDesc;
        }

        public String getTransactionID() {
            return transactionID;
        }

        private String lastBalance;

        private String timeStamp;

        private String refNo;

        private String feeAmount;

        private String totalAmount;

        private String destName;

        private String amount;

        private String destCode;

        private String resultCode;

        private String resultDesc;

        private String transactionID;
    }
}

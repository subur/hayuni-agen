package com.example.user.peruriumroh.preference;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Preferences {
    SharedPreferences mSettings;
    SharedPreferences.Editor editor;
    String reg1NamaLengkap = "", reg1NomorKtp = "", reg1TempatLahir = "",
            reg1TanggalLahir = "", reg1PendidikanTerakhir = "", reg1Pekerjaan = "",
            reg1PenyakitKhusus = "", reg1PeruriName = "", reg1PeruriEmail = "",
            reg1GolonganDarah = "", reg1StatusKawin = "",reg1Koordinator = "",
            reg2NomorPaspor = "",reg2MasaBerlakuPaspor = "",reg2TempatDikeluarkanPaspor = "",
            reg2TanggalDikeluarkanPaspor = "", reg2UmrohKeberapa = "", reg2KeberangkatanTerakhir = "",
            reg3AlamatTempatTinggal = "", reg3AlamatKabupatenKota = "", reg3AlamatProvinsi = "", reg3AlamatEmail = "", reg3NomorTelRumah = "",
            reg3NomorHpWa = "", reg3NamaKontakDarurat = "", reg3NomorKontakDarurat = "",reg3HubunganKontak = "";

    String token = "",userid = "",name = "",email = "",
            telp = "",images = "",agent_id = "",paketId = "",
            produkId = "", ppayusername="", balance="", idTmoney="", idFusion="",tokenppay="", travelId="",qr="";

    //DATA USER LOGIN


    public String getTravelId() {
        return mSettings.getString("travelId",null);
    }

    public void setTravelId(String travelId){
        editor.putString("travelId",travelId);
        editor.apply();
    }

    public String getQr() {
        return mSettings.getString("qr",null);
    }

    public void setQr(String qr) {
        editor.putString("qr",qr);
        editor.apply();
    }

    public String getBalance() {
        return mSettings.getString("balance",null);
    }

    public void setBalance(String balance) {
        editor.putString("balance",balance);
        editor.apply();
    }

    public String getIdTmoney() {
        return mSettings.getString("idTmoney",null);
    }

    public void setIdTmoney(String idTmoney) {
        editor.putString("idTmoney",idTmoney);
        editor.apply();
    }

    public String getIdFusion() {
        return mSettings.getString("idFusion",null);
    }

    public void setIdFusion(String idFusion) {
        editor.putString("idFusion",idFusion);
        editor.apply();
    }

    public String getTokenppay() {
        return mSettings.getString("tokenppay",null);
    }

    public void setTokenppay(String tokenppay) {
        editor.putString("tokenppay",tokenppay);
        editor.apply();
    }

    public String getPpayusername() {
        return mSettings.getString("ppayusername",null);
    }

    public void setPpayusername(String ppayusername) {
        editor.putString("ppayusername",ppayusername);
        editor.apply();
    }

    public String getToken() {
        return mSettings.getString("token",null);
    }

    public void setToken(String token) {
        editor.putString("token",token);
        editor.apply();
    }

    public String getUserid() {
        return mSettings.getString("userid",null);
    }

    public void setUserid(String userid) {
        editor.putString("userid",userid);
        editor.apply();
    }

    public String getName() {
        return mSettings.getString("name",null);
    }

    public void setName(String name) {
        editor.putString("name",name);
        editor.apply();
    }

    public String getEmail() {
        return mSettings.getString("email",null);
    }

    public void setEmail(String email) {
        editor.putString("email",email);
        editor.apply();
    }

    public String getTelp() {
        return mSettings.getString("telp",null);
    }

    public void setTelp(String telp) {
        editor.putString("telp",telp);
        editor.apply();
    }

    public String getImages() {
        return mSettings.getString("images",null);
    }

    public void setImages(String images) {
        editor.putString("images",images);
        editor.apply();
    }

    public String getAgent_id() {
        return mSettings.getString("agent_id",null);
    }

    public void setAgent_id(String agent_id) {
        editor.putString("agent_id",agent_id);
        editor.apply();
    }

    public String getPaketId() {
        return mSettings.getString("paketId",null);
    }

    public void setPaketId(String paketId) {
        editor.putString("paketId",paketId);
        editor.apply();
    }

    public String getProdukId() {
        return mSettings.getString("produkId",null);
    }

    public void setProdukId(String produkId) {
        editor.putString("produkId",produkId);
        editor.apply();
    }


    //DATA REGISTER
    public String getReg3HubunganKontak() {
        return mSettings.getString("reg3HubunganKontak",null);
    }

    public void setReg3HubunganKontak(String reg3HubunganKontak) {
        editor.putString("reg3HubunganKontak",reg3HubunganKontak);
        editor.apply();
    }

    public String getReg3AlamatTempatTinggal() {
        return mSettings.getString("reg3AlamatTempatTinggal",null);
    }

    public void setReg3AlamatTempatTinggal(String reg3AlamatTempatTinggal) {
        editor.putString("reg3AlamatTempatTinggal",reg3AlamatTempatTinggal);
        editor.apply();
    }

    public String getReg3AlamatKabupatenKota() {
        return mSettings.getString("reg3AlamatKabupatenKota",null);
    }

    public void setReg3AlamatKabupatenKota(String reg3AlamatKabupatenKota) {
        editor.putString("reg3AlamatKabupatenKota",reg3AlamatKabupatenKota);
        editor.apply();
    }

    public String getReg3AlamatProvinsi() {
        return mSettings.getString("reg3AlamatProvinsi",null);
    }

    public void setReg3AlamatProvinsi(String reg3AlamatProvinsi) {
        editor.putString("reg3AlamatProvinsi",reg3AlamatProvinsi);
        editor.apply();
    }

    public String getReg3AlamatEmail() {
        return mSettings.getString("reg3AlamatEmail",null);
    }

    public void setReg3AlamatEmail(String reg3AlamatEmail) {
        editor.putString("reg3AlamatEmail",reg3AlamatEmail);
        editor.apply();
    }

    public String getReg3NomorTelRumah() {
        return mSettings.getString("reg3NomorTelRumah",null);
    }

    public void setReg3NomorTelRumah(String reg3NomorTelRumah) {
        editor.putString("reg3NomorTelRumah",reg3NomorTelRumah);
        editor.apply();
    }

    public String getReg3NomorHpWa() {
        return mSettings.getString("reg3NomorHpWa",null);
    }

    public void setReg3NomorHpWa(String reg3NomorHpWa) {
        editor.putString("reg3NomorHpWa",reg3NomorHpWa);
        editor.apply();
    }

    public String getReg3NamaKontakDarurat() {
        return mSettings.getString("reg3NamaKontakDarurat",null);
    }

    public void setReg3NamaKontakDarurat(String reg3NamaKontakDarurat) {
        editor.putString("reg3NamaKontakDarurat",reg3NamaKontakDarurat);
        editor.apply();
    }

    public String getReg3NomorKontakDarurat() {
        return mSettings.getString("reg3NomorKontakDarurat",null);
    }

    public void setReg3NomorKontakDarurat(String reg3NomorKontakDarurat) {
        editor.putString("reg3NomorKontakDarurat",reg3NomorKontakDarurat);
        editor.apply();
    }

    public String getReg2NomorPaspor(){
        return mSettings.getString("reg2NomorPaspor",null);
    }

    public void setReg2NomorPaspor(String reg2NomorPaspor){
        editor.putString("reg2NomorPaspor",reg2NomorPaspor);
        editor.apply();
    }

    public String getReg2MasaBerlakuPaspor(){
        return mSettings.getString("reg2MasaBerlakuPaspor",null);
    }

    public void setReg2MasaBerlakuPaspor(String reg2MasaBerlakuPaspor) {
        editor.putString("reg2MasaBerlakuPaspor",reg2MasaBerlakuPaspor);
        editor.apply();
    }

    public String getReg2TempatDikeluarkanPaspor(){
        return mSettings.getString("reg2TempatDikeluarkanPaspor",null);
    }

    public void setReg2TempatDikeluarkanPaspor(String reg2TempatDikeluarkanPaspor) {
        editor.putString("reg2TempatDikeluarkanPaspor",reg2TempatDikeluarkanPaspor);
        editor.apply();
    }

    public String getReg2TanggalDikeluarkanPaspor(){
        return mSettings.getString("reg2TanggalDikeluarkanPaspor",null);
    }

    public void setReg2TanggalDikeluarkanPaspor(String reg2TanggalDikeluarkanPaspor) {
        editor.putString("reg2TanggalDikeluarkanPaspor",reg2TanggalDikeluarkanPaspor);
        editor.apply();
    }

    public String getReg2UmrohKeberapa() {
        return mSettings.getString("reg2UmrohKeberapa",null);
    }

    public void setReg2UmrohKeberapa(String reg2UmrohKeberapa) {
        editor.putString("reg2UmrohKeberapa",reg2UmrohKeberapa);
        editor.apply();
    }

    public String getReg2KeberangkatanTerakhir() {
        return mSettings.getString("reg2KeberangkatanTerakhir",null);
    }

    public void setReg2KeberangkatanTerakhir(String reg2KeberangkatanTerakhir) {
        editor.putString("reg2KeberangkatanTerakhir",reg2KeberangkatanTerakhir);
        editor.apply();
    }

    public String getReg1NamaLengkap() {
        return mSettings.getString("reg1NamaLengkap",null);
    }

    public String getReg1NomorKtp() {
        return mSettings.getString("reg1NomorKtp",null);
    }

    public String getReg1TempatLahir() {
        return mSettings.getString("reg1TempatLahir",null);
    }

    public String getReg1TanggalLahir() {
        return mSettings.getString("reg1TanggalLahir",null);
    }

    public String getReg1PendidikanTerakhir() {
        return mSettings.getString("reg1PendidikanTerakhir",null);
    }

    public String getReg1Pekerjaan() {
        return mSettings.getString("reg1Pekerjaan",null);
    }

    public String getReg1PenyakitKhusus() {
        return mSettings.getString("reg1PenyakitKhusus",null);
    }

    public String getReg1PeruriName() {
        return mSettings.getString("reg1PeruriName",null);
    }

    public String getReg1PeruriEmail() {
        return mSettings.getString("reg1PeruriEmail",null);
    }

    public String getReg1GolonganDarah() {
        return mSettings.getString("reg1GolonganDarah",null);
    }

    public String getReg1StatusKawin() {
        return mSettings.getString("reg1StatusKawin",null);
    }

    public String getReg1Koordinator() {
        return mSettings.getString("reg1Koordinator",null);
    }

    public void setReg1NamaLengkap(String reg1NamaLengkap) {
        editor.putString("reg1NamaLengkap",reg1NamaLengkap);
        editor.apply();
    }

    public void setReg1NomorKtp(String reg1NomorKtp) {
        editor.putString("reg1NomorKtp",reg1NomorKtp);
        editor.apply();
    }

    public void setReg1TempatLahir(String reg1TempatLahir) {
        editor.putString("reg1TempatLahir",reg1TempatLahir);
        editor.apply();
    }

    public void setReg1TanggalLahir(String reg1TanggalLahir) {
        editor.putString("reg1TanggalLahir",reg1TanggalLahir);
        editor.apply();
    }

    public void setReg1PendidikanTerakhir(String reg1PendidikanTerakhir) {
        editor.putString("reg1PendidikanTerakhir",reg1PendidikanTerakhir);
        editor.apply();
    }

    public void setReg1Pekerjaan(String reg1Pekerjaan) {
        editor.putString("reg1Pekerjaan",reg1Pekerjaan);
        editor.apply();
    }

    public void setReg1PenyakitKhusus(String reg1PenyakitKhusus) {
        editor.putString("reg1PenyakitKhusus",reg1PenyakitKhusus);
        editor.apply();
    }

    public void setReg1PeruriName(String reg1PeruriName) {
        editor.putString("reg1PeruriName",reg1PeruriName);
        editor.apply();
    }

    public void setReg1PeruriEmail(String reg1PeruriEmail) {
        editor.putString("reg1PeruriEmail",reg1PeruriEmail);
        editor.apply();
    }

    public void setReg1GolonganDarah(String reg1GolonganDarah) {
        editor.putString("reg1GolonganDarah",reg1GolonganDarah);
        editor.apply();
    }

    public void setReg1StatusKawin(String reg1StatusKawin) {
        editor.putString("reg1StatusKawin",reg1StatusKawin);
        editor.apply();
    }

    public void setReg1Koordinator(String reg1Koordinator) {
        editor.putString("reg1Koordinator",reg1Koordinator);
        editor.apply();
    }

    public Preferences(Activity act){
        mSettings = act.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        editor = mSettings.edit();
    }

    public void SettingSetNull(){
        editor.putString("token","");
        editor.putString("userid","");
        editor.putString("name","");
        editor.putString("email","");
        editor.putString("telp","");
        editor.putString("images","");
        editor.putString("agent_id","");
        editor.putString("travelId","");
        editor.apply();
    }

    public void SettingSetTMoneyNull(){
        editor.putString("tokenppay","");
        editor.putString("idFusion","");
        editor.putString("idTmoney","");
        editor.putString("balance","");
        editor.apply();
    }

//    public void SettingAddData(String token, String userId, String name, String email, String telp, String images, String agentId){
//        editor.putString("token",token);
//        editor.putString("userid",userId);
//        editor.putString("name",name);
//        editor.putString("email",email);
//        editor.putString("telp",telp);
//        editor.putString("images",images);
//        editor.putString("agent_id",agentId);
//        editor.apply();
//    }
//
//    public List<String> SettingGetData(){
//        List<String> dataSetting = new ArrayList<String>();
//        dataSetting.add(mSettings.getString("token",null));
//        dataSetting.add(mSettings.getString("userid",null));
//        dataSetting.add(mSettings.getString("name",null));
//        dataSetting.add(mSettings.getString("email",null));
//        dataSetting.add(mSettings.getString("telp",null));
//        dataSetting.add(mSettings.getString("images",null));
//        dataSetting.add(mSettings.getString("agent_id",null));
//        return dataSetting;
//    }
}

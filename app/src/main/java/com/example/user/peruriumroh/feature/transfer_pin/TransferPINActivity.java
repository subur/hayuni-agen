package com.example.user.peruriumroh.feature.transfer_pin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.AlertDialog;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.feature.transfer_branch.TransferBranchActivity;
import com.example.user.peruriumroh.feature.transfer_manual.TransferManualActivity;
import com.example.user.peruriumroh.feature.transfer_manual_amount.TransferManualAmountActivity;
import com.example.user.peruriumroh.feature.transfer_scan.TransferScanActivity;
import com.example.user.peruriumroh.feature.transfer_success.TransferSuccessActivity;
import com.example.user.peruriumroh.model.ResponsePayment;
import com.example.user.peruriumroh.model.ResponseTransfer;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;
import com.goodiebag.pinview.Pinview;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransferPINActivity extends AppCompatActivity {

    TextView tvAmount;
    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();
    Preferences preferences;
    String paymentAmount,destinationId,destinationRole,transactionId,refNo;

    ProgressDialog progressBar;

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_pin);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        preferences = new Preferences(TransferPINActivity.this);

        //PROGRESS BAR
        progressBar = new ProgressDialog(TransferPINActivity.this);
        progressBar.setCancelable(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        //VARIABLE VIEW
        tvAmount = (TextView)findViewById(R.id.tv_amount);

        if (savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if(extras == null){
                paymentAmount = null;
                destinationId = null;
                destinationRole = null;
                transactionId = null;
                refNo = null;
            }else{
                paymentAmount = extras.getString("paymentAmount");
                destinationId = extras.getString("destinationId");
                destinationRole = extras.getString("destinationRole");
                transactionId = extras.getString("transactionId");
                refNo = extras.getString("refNo");
            }
        } else {
            paymentAmount = (String) savedInstanceState.getSerializable("paymentAmount");
            destinationId = (String) savedInstanceState.getSerializable("destinationId");
            destinationRole = (String) savedInstanceState.getSerializable("destinationRole");
            transactionId = (String) savedInstanceState.getSerializable("transactionId");
            refNo = (String) savedInstanceState.getSerializable("refNo");
        }

        tvAmount.setText(CurrencyFormating.setToCurrency(paymentAmount));

        Pinview pv = (Pinview)findViewById(R.id.pinview);
        pv.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
                String pin = pinview.getValue();

                View view = TransferPINActivity.this.getCurrentFocus();
                if(view != null){
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(),0);
                }

                progressBar.setMessage("Memproses transaksi.");
                progressBar.show();

                Call<ResponseTransfer> transactionPayment = apiClientInterface.transactionPostTransfer(
                        preferences.getUserid(),
                        preferences.getToken(),
                        destinationId,
                        destinationRole,
                        paymentAmount,
                        preferences.getAgent_id(),
                        "Agent",
                        preferences.getIdTmoney(),
                        preferences.getIdFusion(),
                        preferences.getTokenppay(),
                        pin,
                        transactionId,
                        refNo
                );

                transactionPayment.enqueue(new Callback<ResponseTransfer>() {
                    @Override
                    public void onResponse(Call<ResponseTransfer> call, Response<ResponseTransfer> response) {
                        if(response.code() == 200){
                            if(response.body().getData() != null){
                                if(response.body().getErrorcode().equals("00000")){
                                    //Done Payment
                                    progressBar.dismiss();

                                    Intent intent = new Intent(TransferPINActivity.this, TransferSuccessActivity.class);
                                    intent.putExtra("amount",paymentAmount);
                                    startActivity(intent);

                                    try{
                                        TransferManualActivity.ac.finish();
                                    }catch (Exception e){
                                        Log.d("FAIL", "onResponse: closing Manual Transfer");
                                    }

                                    try{
                                        TransferScanActivity.ac.finish();
                                    }catch (Exception e){
                                        Log.d("FAIL", "onResponse: closing Scan Transfer");
                                    }

                                    TransferManualAmountActivity.ac.finish();
                                    TransferBranchActivity.ac.finish();
                                    finish();
                                }else if(response.body().getErrorcode().equals("00170")){
                                    //Failed Payment
                                    progressBar.dismiss();
                                    Toast.makeText(getApplicationContext(),"PIN Peruri Pay yang anda masukkan tidak valid di dalam sistem kami",Toast.LENGTH_LONG).show();
                                    finish();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseTransfer> call, Throwable t) {
                        progressBar.dismiss();
                        if(t instanceof IOException){
                            AlertConnectionDialog.showConnectionAlert(TransferPINActivity.this);
                            finish();
                        }else {
                            AlertDialog.showAlert(TransferPINActivity.this,"Peruri Umroh","Terjadi kesalahan sistem");
                            finish();
                        }
                    }
                });
            }
        });
    }
}

package com.example.user.peruriumroh.model;

import java.util.List;

public class KoordinatorId {

    private List<Data> data;

    private String errorcode;

    private String errormsg;

    public List<Data> getData() {
        return data;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public class Data {
        private String jamaah_id;

        private String jamaah_name;

        public String getJamaah_id() {
            return jamaah_id;
        }

        public String getJamaah_name() {
            return jamaah_name;
        }
    }
}

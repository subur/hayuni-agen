package com.example.user.peruriumroh.feature.jamaah_listjamaahbykoordinator;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.jamaah_listjamaah.ListJamaahAdapter;

public class ListJamaahKoordinatorAdapter extends RecyclerView.Adapter<ListJamaahKoordinatorAdapter.ListJamaahKoordinatorViewHolder> {

    int numberRv = 0;
    public ListJamaahKoordinatorAdapter(int numberRv){this.numberRv = numberRv;}

    @NonNull
    @Override
    public ListJamaahKoordinatorViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutItem = R.layout.item_daftar_jamaah;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachtoParentImmediately = false;

        View view = inflater.inflate(layoutItem,viewGroup,shouldAttachtoParentImmediately);
        ListJamaahKoordinatorAdapter.ListJamaahKoordinatorViewHolder viewHolder = new ListJamaahKoordinatorAdapter.ListJamaahKoordinatorViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListJamaahKoordinatorViewHolder listJamaahKoordinatorViewHolder, int i) {
        listJamaahKoordinatorViewHolder.bind(i);
    }

    @Override
    public int getItemCount() {
        return numberRv;
    }

    public class ListJamaahKoordinatorViewHolder extends RecyclerView.ViewHolder {
        public ListJamaahKoordinatorViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public void bind(int i) {
        }
    }
}

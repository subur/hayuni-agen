package com.example.user.peruriumroh.model;

import java.io.Serializable;

public class Jamaah implements Serializable {

    public Jamaah(String jemaahNamaLengkap, String jemaahKtpNik, String jemaahNomorHp, String jemaahPaketUmroh, String jemaahSaldo, String jemaahSisaCicilan, String jemaahQRCode, String jemaahTglBerangkat) {
        JemaahNamaLengkap = jemaahNamaLengkap;
        JemaahKtpNik = jemaahKtpNik;
        JemaahNomorHp = jemaahNomorHp;
        JemaahPaketUmroh = jemaahPaketUmroh;
        JemaahSaldo = jemaahSaldo;
        JemaahSisaCicilan = jemaahSisaCicilan;
        JemaahQRCode = jemaahQRCode;
        JemaahTglBerangkat = jemaahTglBerangkat;
    }

    String JemaahNamaLengkap;
    String JemaahKtpNik;
    String JemaahNomorHp;
    String JemaahPaketUmroh;
    String JemaahSaldo;
    String JemaahSisaCicilan;
    String JemaahQRCode;
    String JemaahTglBerangkat;

    public String getJemaahNamaLengkap() {
        return JemaahNamaLengkap;
    }

    public void setJemaahNamaLengkap(String jemaahNamaLengkap) {
        JemaahNamaLengkap = jemaahNamaLengkap;
    }

    public String getJemaahKtpNik() {
        return JemaahKtpNik;
    }

    public void setJemaahKtpNik(String jemaahKtpNik) {
        JemaahKtpNik = jemaahKtpNik;
    }

    public String getJemaahNomorHp() {
        return JemaahNomorHp;
    }

    public void setJemaahNomorHp(String jemaahNomorHp) {
        JemaahNomorHp = jemaahNomorHp;
    }

    public String getJemaahPaketUmroh() {
        return JemaahPaketUmroh;
    }

    public void setJemaahPaketUmroh(String jemaahPaketUmroh) {
        JemaahPaketUmroh = jemaahPaketUmroh;
    }

    public String getJemaahSaldo() {
        return JemaahSaldo;
    }

    public void setJemaahSaldo(String jemaahSaldo) {
        JemaahSaldo = jemaahSaldo;
    }

    public String getJemaahSisaCicilan() {
        return JemaahSisaCicilan;
    }

    public void setJemaahSisaCicilan(String jemaahSisaCicilan) {
        JemaahSisaCicilan = jemaahSisaCicilan;
    }

    public String getJemaahQRCode() {
        return JemaahQRCode;
    }

    public void setJemaahQRCode(String jemaahQRCode) {
        JemaahQRCode = jemaahQRCode;
    }

    public String getJemaahTglBerangkat() {
        return JemaahTglBerangkat;
    }

    public void setJemaahTglBerangkat(String jemaahTglBerangkat) {
        JemaahTglBerangkat = jemaahTglBerangkat;
    }
}

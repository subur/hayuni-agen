package com.example.user.peruriumroh.feature.jamaah_detailjamaah;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.saldo_ceksaldo.SaldoCekSaldoAdapter;
import com.example.user.peruriumroh.model.Jamaah;
import com.example.user.peruriumroh.model.ListJamaah;
import com.example.user.peruriumroh.model.ResponseMutasi;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailJamaahActivity extends AppCompatActivity {

    RecyclerView rvMutasiJamaahDetail;
    DetailJamaahAdapter detailJamaahAdapter;
    TextView tvNama, tvKtp, tvNomorHp, tvPaket, tvSaldo,tvSisaPembayaran;
    ArrayList<ListJamaah.Data> objects;
    ImageView ivSQRCJamaah;

    Preferences preferences;

    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_jamaah);

        ac = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        preferences = new Preferences(DetailJamaahActivity.this);

        objects = (ArrayList<ListJamaah.Data>)getIntent().getSerializableExtra("kunci");

        getSupportActionBar().setTitle(objects.get(0).getJamaah_name());

        tvNama = (TextView)findViewById(R.id.tv_detail_nama_jamaah);
        tvNama.setText(objects.get(0).getJamaah_name());
        tvKtp = (TextView)findViewById(R.id.tv_detail_ktp);
        tvKtp.setText(objects.get(0).getJamaah_ktp());
        tvNomorHp = (TextView)findViewById(R.id.tv_detail_nomorhp);
        tvNomorHp.setText(objects.get(0).getContact().get(0).getContact_handphone());
        tvPaket = (TextView)findViewById(R.id.tv_detail_paket);
        tvPaket.setText(objects.get(0).getUmroh().get(0).getProduct_name() + " - " + objects.get(0).getUmroh().get(0).getPackage_name());
        String saldo = "0";
        if(objects.get(0).getUmroh().get(0).getUmroh_deposit() == null){
            saldo = "0";
        }else {
            saldo = objects.get(0).getUmroh().get(0).getUmroh_deposit();
        }
        tvSaldo = (TextView)findViewById(R.id.tv_detail_saldo);
        tvSaldo.setText("Deposit Jamaah : " + CurrencyFormating.setToCurrency(saldo));
        String debt = "0";
        if(objects.get(0).getUmroh().get(0).getUmroh_debt().equals(null)){
            debt = "0";
        }else {
            debt = String.valueOf(Integer.parseInt(objects.get(0).getUmroh().get(0).getUmroh_debt())-(Integer.parseInt(saldo)));
        }
        tvSisaPembayaran = (TextView)findViewById(R.id.tv_detail_sisapembayaran);
        tvSisaPembayaran.setText(CurrencyFormating.setToCurrency(debt));

        ivSQRCJamaah = (ImageView)findViewById(R.id.iv_sqrc_jamaah);
        byte[] decodedString = Base64.decode(objects.get(0).getSqrc().get(0).getSqrc_kode(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0,decodedString.length);
        ivSQRCJamaah.setImageBitmap(decodedByte);

        rvMutasiJamaahDetail = (RecyclerView)findViewById(R.id.rv_mutasi_jamaah_detail);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvMutasiJamaahDetail.setLayoutManager(layoutManager);

        rvMutasiJamaahDetail.setHasFixedSize(true);

        Call<ResponseMutasi> call = apiClientInterface.agenGetMutasi(
                preferences.getUserid(),
                preferences.getToken(),
                "",
                "mobile",
                "",
                "",
                objects.get(0).getUmroh().get(0).getUmroh_id()
        );
        call.enqueue(new Callback<ResponseMutasi>() {
            @Override
            public void onResponse(Call<ResponseMutasi> call, Response<ResponseMutasi> response) {
                if(response.code() == 200){
                    if(response.body().getErrorcode().equals("00000")){
                        List<ResponseMutasi.Data> mutasiJamaah = new ArrayList<ResponseMutasi.Data>();
                        mutasiJamaah = response.body().getData();
                        detailJamaahAdapter = new DetailJamaahAdapter(DetailJamaahActivity.this,mutasiJamaah);
                        rvMutasiJamaahDetail.setAdapter(detailJamaahAdapter);
                    }
                }else if(response.code() == 401){
                    Logout.logout(DetailJamaahActivity.this);
                }
            }

            @Override
            public void onFailure(Call<ResponseMutasi> call, Throwable t){

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result","finish");
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
}

package com.example.user.peruriumroh.feature.registerjamaah4;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.FileUtils;
import com.example.user.peruriumroh.feature.home.HomeActivity;
import com.example.user.peruriumroh.feature.landing.WelcomeActivity;
import com.example.user.peruriumroh.feature.login.LoginActivity;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import org.w3c.dom.Text;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterJamaah4Activity extends AppCompatActivity {
    public static final int ACTION_REQUEST_GALLERY = 0;
    public static final int ACTION_REQUEST_CAMERA = 1;

    Button btSubmit;
    TextView tvImagePaspor,tvImageKK,tvImageKTP,tvImagePasPoto,tvImageBiometrik,tvImageBukuKuning;
    ImageButton ibtPaspor, ibtKartuKeluarga, ibtKtp, ibtPasFoto, ibtBiometrik, ibtBukuKuning;
    Uri uriImagePaspor,uriImageKK,uriImageKTP,uriImagePasPoto,uriImageBiometrik,uriImageBukuKuning;
    Preferences preferences;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_jamaah4);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        tvImagePaspor = (TextView)findViewById(R.id.tv_image_paspor);
        tvImageKK = (TextView)findViewById(R.id.tv_image_kk);
        tvImageKTP = (TextView)findViewById(R.id.tv_image_ktp);
        tvImagePasPoto = (TextView)findViewById(R.id.tv_image_paspoto);
        tvImageBiometrik = (TextView)findViewById(R.id.tv_image_biometrik);
        tvImageBukuKuning = (TextView)findViewById(R.id.tv_image_bukukuning);

        preferences = new Preferences(RegisterJamaah4Activity.this);

        ibtPaspor = (ImageButton)findViewById(R.id.ibt_paspor);
        ibtPaspor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openIntent("Unggah Foto Paspor",1);
            }
        });

        ibtKartuKeluarga = (ImageButton)findViewById(R.id.ibt_kartukeluarga);
        ibtKartuKeluarga.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openIntent("Unggah Foto Kartu Keluarga", 2);
            }
        });

        ibtKtp = (ImageButton)findViewById(R.id.ibt_ktp);
        ibtKtp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openIntent("Unggah Foto KTP",3);
            }
        });

        ibtPasFoto = (ImageButton)findViewById(R.id.ibt_pasfoto);
        ibtPasFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openIntent("Unggah Foto Pas Foto",4);
            }
        });

        ibtBiometrik = (ImageButton)findViewById(R.id.ibt_biometrik);
        ibtBiometrik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openIntent("Unggah Foto Surat Rekam Biometrik",5);
            }
        });

        ibtBukuKuning = (ImageButton)findViewById(R.id.ibt_bukukuning);
        ibtBukuKuning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openIntent("Unggah Foto Buku Kuning",6);
            }
        });

        btSubmit = (Button)findViewById(R.id.bt_submit);
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvImagePaspor.getText().equals("")){
                    tvImagePaspor.requestFocus();
                    tvImagePaspor.setError("Dokumen Paspor Harus Terisi");
                }else if(tvImageKK.getText().equals("")){
                    tvImageKK.requestFocus();
                    tvImageKK.setError("Dokumen Kartu Keluarga Harus Terisi");
                }else if(tvImageKTP.getText().equals("")){
                    tvImageKTP.requestFocus();
                    tvImageKTP.setError("Dokumen KTP/NIK Harus Terisi");
                }else if(tvImagePasPoto.getText().equals("")){
                    tvImagePasPoto.requestFocus();
                    tvImagePasPoto.setError("Dokumen Pas Foto Harus Terisi");
                }else if(tvImageBiometrik.getText().equals("")){
                    tvImageBiometrik.requestFocus();
                    tvImageBiometrik.setError("Dokumen Rekam Biometrik Harus Terisi");
                }else if(tvImageBukuKuning.getText().equals("")){
                    tvImageBukuKuning.requestFocus();
                    tvImageBukuKuning.setError("Dokumen Buku Kuning Harus Terisi");
                }else {
                    registerUser();
                }
//                Intent intent = new Intent(RegisterJamaah4Activity.this,RegisterJamaah5Activity.class);
//                startActivity(intent);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void registerUser() {
        Toast.makeText(getApplicationContext(),preferences.getReg1NamaLengkap(),Toast.LENGTH_LONG).show();
    }

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Tunggu Sebentar..");
        progressDialog.setMessage("Mendaftarkan Jamaah ...");
        progressDialog.show();
    }

    private void dismissProgressDialog() {
//         To Dismiss progress dialog
        progressDialog.dismiss();
    }

//    private void registerUser() {
//        ApiClient apiClient = new ApiClient();
//        ApiClientInterface apiClientInterface = apiClient.clientInterface();
//
//        List<String> listData = preferences.SettingGetData();
//        showProgressDialog();
//        Call<ResponseBody> call = apiClientInterface.registerJamaah(
//                listData.get(1),
//                listData.get(0),
//                "mobile",
//                listData.get(6),
//                preferences.getReg1NamaLengkap(),
//                preferences.getReg1NomorKtp(),
//                preferences.getReg1TempatLahir(),
//                preferences.getReg1TanggalLahir(),
//                preferences.getReg1GolonganDarah(),
//                preferences.getReg1StatusKawin(),
//                preferences.getReg1PendidikanTerakhir(),
//                preferences.getReg1Pekerjaan(),
//                preferences.getReg1PenyakitKhusus(),
//                preferences.getReg1Koordinator(),
//                preferences.getPaketId(),
//                preferences.getReg3AlamatTempatTinggal(),
//                preferences.getReg3AlamatKabupatenKota(),
//                preferences.getReg3AlamatProvinsi(),
//                preferences.getReg3AlamatEmail(),
//                preferences.getReg3NomorTelRumah(),
//                preferences.getReg3NomorHpWa(),
//                preferences.getReg3NamaKontakDarurat(),
//                preferences.getReg3NomorKontakDarurat(),
//                preferences.getReg3HubunganKontak(),
//                preferences.getReg2NomorPaspor(),
//                preferences.getReg2MasaBerlakuPaspor(),
//                preferences.getReg2TanggalDikeluarkanPaspor(),
//                preferences.getReg2UmrohKeberapa(),
//                preferences.getReg2KeberangkatanTerakhir(),
//                preferences.getReg1PeruriEmail(),
//                preferences.getReg1PeruriName(),
//                convertToPart(uriImagePaspor),
//                convertToPart(uriImageKK),
//                convertToPart(uriImageKTP),
//                convertToPart(uriImagePasPoto),
//                convertToPart(uriImageBiometrik),
//                convertToPart(uriImageBukuKuning)
//        );
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                dismissProgressDialog();
//                try {
//                    tvError.setText(response.body().string());
//                }catch (Exception e){
//                    tvError.setText(e.getMessage());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                dismissProgressDialog();
//            }
//        });
//    }

    private MultipartBody.Part convertToPart(Uri uriImage){
        File original = FileUtils.getFile(this,uriImage);
        RequestBody filePart = RequestBody.create(
                MediaType.parse(getContentResolver().getType(uriImage)),
                original);

        MultipartBody.Part file = MultipartBody.Part.createFormData("photo",original.getName(),filePart);
        return file;
    }

    private void openIntent(final String title, final int uploader){
        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterJamaah4Activity.this);
        builder.setTitle(title);
        builder.setItems(new CharSequence[]{"Gallery", "Camera"},
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                //IMAGE DARI GALLERY

                                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                                intent.setType("image/*");
                                intent.putExtra("uploadCode",uploader);
                                setIntent(intent);
                                startActivityForResult(intent,ACTION_REQUEST_GALLERY);

                                break;

                            case 1:

                                break;

                            default:

                                break;
                        }
                    }
                });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case ACTION_REQUEST_GALLERY:
                        if(data!=null){
                            int currentUploaderCode = getIntent().getIntExtra("uploadCode",-1);
                            switch (currentUploaderCode){
                                case 1:
                                    uriImagePaspor = data.getData();
                                    File original1 = FileUtils.getFile(this,uriImagePaspor);
                                    String[] imageName = original1.toString().split("/");
                                    tvImagePaspor.setText(imageName[imageName.length-1]);
                                    break;

                                case 2:
                                    uriImageKK = data.getData();
                                    File original2 = FileUtils.getFile(this,uriImageKK);
                                    String[] imageName2 = original2.toString().split("/");
                                    tvImageKK.setText(imageName2[imageName2.length-1]);
                                    break;

                                case 3:
                                    uriImageKTP = data.getData();
                                    File original3 = FileUtils.getFile(this,uriImageKTP);
                                    String[] imageName3 = original3.toString().split("/");
                                    tvImageKTP.setText(imageName3[imageName3.length-1]);
                                    break;

                                case 4:
                                    uriImagePasPoto = data.getData();
                                    File original4 = FileUtils.getFile(this,uriImagePasPoto);
                                    String[] imageName4 = original4.toString().split("/");
                                    tvImagePasPoto.setText(imageName4[imageName4.length-1]);
                                    break;

                                case 5:
                                    uriImageBiometrik = data.getData();
                                    File original5 = FileUtils.getFile(this,uriImageBiometrik);
                                    String[] imageName5 = original5.toString().split("/");
                                    tvImageBiometrik.setText(imageName5[imageName5.length-1]);
                                    break;

                                case 6:
                                    uriImageBukuKuning = data.getData();
                                    File original6 = FileUtils.getFile(this,uriImageBukuKuning);
                                    String[] imageName6 = original6.toString().split("/");
                                    tvImageBukuKuning.setText(imageName6[imageName6.length-1]);
                                    break;

                                default:
                                    break;
                            }
                        }
                    break;

                case ACTION_REQUEST_CAMERA:

                    break;
            }
        }
    }

    //    private void openIntent(String title) {
//        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT,null);
//        galleryIntent.setType("image/*");
//        galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
//
//        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//
//        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
//        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
//        chooser.putExtra(Intent.EXTRA_TITLE, title);
//
//        Intent[] intentArray =  {cameraIntent};
//        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
//        startActivityForResult(chooser,100);
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        if(requestCode == 100){
//            if(resultCode == RESULT_OK){
//
//            }
////            if(resultCode == RESULT_OK){
////                if(data != null){
////                    Uri mImageUri = data.getData();
////
//////                    // Removes Uri Permission so that when you restart the device, it will be allowed to reload.
//////                    this.grantUriPermission(this.getPackageName(), mImageUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
//////                    final int takeFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION;
//////                    this.getContentResolver().takePersistableUriPermission(mImageUri, takeFlags);
////
////                    Toast.makeText(getApplicationContext(),mImageUri.toString(),Toast.LENGTH_LONG).show();
////
////                }
////            }
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }
}

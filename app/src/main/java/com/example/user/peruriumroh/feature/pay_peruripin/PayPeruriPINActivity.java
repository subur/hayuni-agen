package com.example.user.peruriumroh.feature.pay_peruripin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.BottomSheetConnectionDialog;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.home.HomeActivity;
import com.example.user.peruriumroh.feature.login.LoginActivity;
import com.example.user.peruriumroh.feature.pay_manualjamaah.PayManualJamaahActivity;
import com.example.user.peruriumroh.feature.pay_voucher_sukses.PayVoucherSuksesActivity;
import com.example.user.peruriumroh.feature.transfer_manual_amount.TransferManualAmountActivity;
import com.example.user.peruriumroh.model.ResponseInquiry;
import com.example.user.peruriumroh.model.ResponsePayment;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;
import com.goodiebag.pinview.Pinview;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayPeruriPINActivity extends AppCompatActivity implements BottomSheetConnectionDialog.BottomSheetListener {

    TextView tvAmount;
    Button btLakukanPembayaran;
    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();
    Preferences preferences;
    String nominal,jamaahId,umrohId,transactionId,refNo,jenisPembayaran,from;

    ProgressDialog progressBar;

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_peruri_pin);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        preferences = new Preferences(PayPeruriPINActivity.this);

        //PROGRESS BAR
        progressBar = new ProgressDialog(PayPeruriPINActivity.this);
        progressBar.setCancelable(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        if (savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if(extras == null){
                nominal = "0";
                jamaahId = null;
                umrohId = null;
                jenisPembayaran = null;
                from = null;
                transactionId = null;
                refNo = null;
            }else{
                nominal = extras.getString("Nominal");
                jamaahId = extras.getString("JamaahId");
                umrohId = extras.getString("UmrohId");
                from = extras.getString("from");
                transactionId = extras.getString("transactionId");
                refNo = extras.getString("refNo");
                String valPembayaran = extras.getString("JenisPembayaran");
                if(valPembayaran.equals("0")){
                    jenisPembayaran = "book";
                }else if(valPembayaran.equals("1")){
                    jenisPembayaran = "dp";
                }else if(valPembayaran.equals("2")){
                    jenisPembayaran = "cicilan";
                }
            }
        } else {
            nominal = (String) savedInstanceState.getSerializable("Nominal");
            jamaahId = (String) savedInstanceState.getSerializable("JamaahId");
            umrohId = (String) savedInstanceState.getSerializable("UmrohId");
            from = (String) savedInstanceState.getSerializable("from");
            transactionId = (String) savedInstanceState.getSerializable("transactionId");
            refNo = (String) savedInstanceState.getSerializable("refNo");
            String valPembayaran = (String) savedInstanceState.getSerializable("JenisPembayaran");
            if(valPembayaran.equals("0")){
                jenisPembayaran = "book";
            }else if(valPembayaran.equals("1")){
                jenisPembayaran = "dp";
            }else if(valPembayaran.equals("2")){
                jenisPembayaran = "cicilan";
            }
        }

        //VARIABLE VIEW
        tvAmount = (TextView)findViewById(R.id.tv_amount);
        tvAmount.setText(CurrencyFormating.setToCurrency(nominal));

        Pinview pv = (Pinview)findViewById(R.id.pinview);
        pv.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
                String data = pinview.getValue();

                View view = PayPeruriPINActivity.this.getCurrentFocus();
                if(view != null){
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(),0);
                }

                progressBar.setMessage("Tunggu Sebentar...");
                progressBar.show();

                Call<ResponsePayment> call = apiClientInterface.agenPostPayment(
                        preferences.getUserid(),
                        preferences.getToken(),
                        "mobile",
                        umrohId,
                        nominal,
                        preferences.getAgent_id(),
                        "Agent",
                        preferences.getIdTmoney(),
                        preferences.getIdFusion(),
                        preferences.getTokenppay(),
                        data,
                        transactionId,
                        refNo,
                        jenisPembayaran
                );

                call.enqueue(new Callback<ResponsePayment>() {
                    @Override
                    public void onResponse(Call<ResponsePayment> call, Response<ResponsePayment> response) {
                        if (response.code() == 200){
                            if(response.body().getErrorcode().equals("00000")){
                                Toast.makeText(getApplicationContext(),response.body().getData().getPeruripayment().getResultDesc(),Toast.LENGTH_SHORT).show();
                                preferences.setBalance(response.body().getData().getPeruripayment().getLastBalance());
                                progressBar.dismiss();

                                Intent intent = new Intent(PayPeruriPINActivity.this,PayVoucherSuksesActivity.class);
                                intent.putExtra("Amount",response.body().getData().getPeruripayment().getAmount());
                                intent.putExtra("JamaahId",jamaahId);
                                intent.putExtra("SQRC",response.body().getData().getSqrc());
                                startActivity(intent);

                                finish();
                            }else if(response.body().getErrorcode().equals("00170")){
                                progressBar.dismiss();
                                Toast.makeText(getApplicationContext(),"PIN Peruri Pay yang anda masukkan tidak valid di dalam sistem kami",Toast.LENGTH_LONG).show();
                                finish();
                            }else {
                                progressBar.dismiss();
                                finish();
                            }
                        }else if(response.code() == 401){
                            Logout.logout(PayPeruriPINActivity.this);
                        } else {
                            progressBar.dismiss();
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponsePayment> call, Throwable t){
                        progressBar.dismiss();
                        finish();
                    }
                });

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onButtonClick(String text){
        finish();
    }
}

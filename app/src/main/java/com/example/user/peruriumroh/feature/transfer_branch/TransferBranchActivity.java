package com.example.user.peruriumroh.feature.transfer_branch;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.transfer_manual.TransferManualActivity;
import com.example.user.peruriumroh.feature.transfer_scan.TransferScanActivity;

public class TransferBranchActivity extends AppCompatActivity {

    Button btTransferManual,btTransferScan;

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_branch);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Transfer Peruri Tunai");

        btTransferManual = (Button)findViewById(R.id.bt_transfer_input_manual);
        btTransferManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(v.getContext(),TransferManualActivity.class);
                startActivity(intent);
            }
        });

        btTransferScan = (Button)findViewById(R.id.bt_transfer_scan_qr);
        btTransferScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),TransferScanActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.example.user.peruriumroh.feature.pay_scansqrcjamaah;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.media.Image;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.densowave.dwqrkit.DWQRKit;
import com.densowave.dwqrkit.DWQRScanMode;
import com.densowave.dwqrkit.DWQRScanView;
import com.densowave.dwqrkit.listener.DWQRCodeNoticeListener;
import com.densowave.dwqrkit.listener.DWQRLicenseListener;
import com.densowave.dwqrkit.listener.DWQRScanViewCreateListener;
import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.AlertDialog;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.jamaah_detailjamaah.DetailJamaahActivity;
import com.example.user.peruriumroh.feature.pay_konfirmasi.PayKonfirmasiJamaahPembayaranActivity;
import com.example.user.peruriumroh.feature.pay_scanjamaah.PayScanJamaahActivity;
import com.example.user.peruriumroh.feature.saldo_scanSQRC.SaldoScanSQRCActivity;
import com.example.user.peruriumroh.model.ListJamaah;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayScanSQRCJamaahActivity extends AppCompatActivity implements ViewTreeObserver.OnGlobalLayoutListener,
        DWQRCodeNoticeListener,DWQRScanViewCreateListener,DWQRLicenseListener,android.view.View.OnClickListener {


    protected static final String TAG_CAMERA_FRAGMENT = "camera";
    private int REQUEST_CODE_CAMERA_PERMISSION = 0x01;
    private FrameLayout fl_camera;
    private DWQRScanView preview;
    private DWQRKit dwqrKit;

    public static Activity ac;

    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();

    Preferences settings;

    ProgressDialog progressDialog;

    Integer openStatus = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_scan_sqrcjamaah);

        ac = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        settings = new Preferences(PayScanSQRCJamaahActivity.this);

        progressDialog = new ProgressDialog(PayScanSQRCJamaahActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        setupSqrc(savedInstanceState);
    }

    private void setupSqrc(Bundle savedInstanceState) {
        dwqrKit = DWQRKit.sharedManager();
        dwqrKit.isVibrationEnable = false;
        dwqrKit.isAudioEnable = false;
        dwqrKit.isFlashEnable = false;
        dwqrKit.isServerConnectionEnable = true;
        dwqrKit.isLogoDisplayEnable = false;
        dwqrKit.setMode(DWQRScanMode.DWQR_NOMALMODE.getValue());

        if(Build.VERSION.SDK_INT >= 23){
            if(PermissionChecker.checkSelfPermission(PayScanSQRCJamaahActivity.this,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(PayScanSQRCJamaahActivity.this,new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                },REQUEST_CODE_CAMERA_PERMISSION);
            }
        }

        if(savedInstanceState == null){
            fl_camera = (FrameLayout)findViewById(R.id.frameScanSQRC);
            fl_camera.getViewTreeObserver().addOnGlobalLayoutListener(this);
        }

        AssetManager assetManager = this.getAssets();
        String filePath = this.getApplicationContext().getFilesDir().toString() + "/license.txt";
        File file = new File(filePath);

        try {
            InputStream in = assetManager.open("license.txt");
            OutputStream out = new BufferedOutputStream(new FileOutputStream(file));

            copyFile(in,out);
            in.close();
            out.flush();
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[8196];
        int read;
        while ((read = in.read(buffer)) != -1){
            out.write(buffer,0,read);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        if (requestCode == REQUEST_CODE_CAMERA_PERMISSION && grantResults[0] != PackageManager.PERMISSION_GRANTED){
            finish();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_close:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onGlobalLayout(){
        preview = new DWQRScanView();
        fl_camera.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        preview.setOnQRCodeFoundNoticeListener(this);
        preview.setDwQrScanViewCreateListener(this);
        preview.setDwQrLicenseListener(this);
        getSupportFragmentManager().beginTransaction().add(R.id.frameScanSQRC,preview,TAG_CAMERA_FRAGMENT).commit();
    }

    @Override
    public void onNoticeDecodeResult(String decodeData, String codeType, byte[] bytes, int i, String decodePrivateData, int i1) {
        try {
            String[] explode1 = decodeData.split(",");
            String regex = "\\s+";
            String role = explode1[3].replaceAll(regex,"");
            String roleexplode[] = role.split(":");
            String roleval = roleexplode[1];

            String email = explode1[2].replaceAll(regex,"");

            Log.d("TAG", roleval+"/"+email);

            if(roleval.equals("Jamaah")){

                if(openStatus == 0) {

                    openStatus = 1;

                    //GET DEPOSIT JAMAAH
                    progressDialog.setMessage("Mencari Jamaah..");
                    progressDialog.show();

                    Call<ListJamaah> getJamaah = apiClientInterface.jamaahGetJamaahByEmail(
                            settings.getUserid(),
                            settings.getToken(),
                            "mobile",
                            email
                    );
                    getJamaah.enqueue(new Callback<ListJamaah>(){
                        @Override
                        public void onResponse(Call<ListJamaah> call, Response<ListJamaah> response) {
                            if(response.code() == 200){
                                if(response.body().getData() != null){
                                    if(response.body().getErrormsg().equals("OK")){
                                        progressDialog.dismiss();

                                        // Open Detail Jamaah
                                        ArrayList<ListJamaah.Data> jemaahList = new ArrayList<ListJamaah.Data>();
                                        jemaahList.add(response.body().getData().get(0));

                                        //MUST EDIT
                                        Intent intent = new Intent(PayScanSQRCJamaahActivity.this,PayScanJamaahActivity.class);
                                        intent.putExtra("kunci",jemaahList);
                                        startActivityForResult(intent,1);
                                    }
                                }else {
                                    progressDialog.dismiss();
                                    AlertDialog.showAlert(PayScanSQRCJamaahActivity.this,"Peruri Umroh","Jamaah tidak ditemukan");
                                }
                            }else if(response.code() == 401){
                                progressDialog.dismiss();
                                Logout.logout(PayScanSQRCJamaahActivity.this);
                            } else {
                                progressDialog.dismiss();
                                AlertDialog.showAlert(PayScanSQRCJamaahActivity.this,"Peruri Umroh","Jamaah tidak ditemukan");
                            }
                        }

                        @Override
                        public void onFailure(Call<ListJamaah> call, Throwable t) {
                            progressDialog.dismiss();
                            if(t instanceof IOException){
                                AlertConnectionDialog.showConnectionAlert(PayScanSQRCJamaahActivity.this);
                            }else {
                                AlertDialog.showAlert(PayScanSQRCJamaahActivity.this,"Peruri Umroh", "Terjadi kesalahan sistem");
                            }
                        }
                    });
                }
            }
        }catch (Exception e){
            Log.d("TAG", "onNoticeDecodeResult: "+e.getMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == 1){
            openStatus = 0;
        }
    }

    @Override
    public void onNoticeLicenseExpired(int i) {

    }

    @Override
    public void onCreateScanView(FrameLayout frameLayout) {
        fl_camera = frameLayout;
        preview.readLicenseFile();
    }
}

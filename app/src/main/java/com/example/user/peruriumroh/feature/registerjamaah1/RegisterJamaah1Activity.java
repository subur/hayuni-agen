package com.example.user.peruriumroh.feature.registerjamaah1;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telecom.Call;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.LocaleID;
import com.example.user.peruriumroh.feature.registerjamaah2.RegisterJamaah2Activity;
import com.example.user.peruriumroh.preference.Preferences;

import java.lang.invoke.CallSite;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.zip.Inflater;

public class RegisterJamaah1Activity extends AppCompatActivity {

    EditText reg1NamaLengkap, reg1NomorKtp, reg1TempatLahir, reg1TanggalLahir, reg1PendidikanTerakhir, reg1Pekerjaan, reg1PenyakitKhusus, reg1PeruriName, reg1PeruriEmail;
    Spinner reg1SpinnerGolonganDarah, reg1SpinnerStatusKawin;
    CheckBox reg1CheckboxKoordinator;
    Preferences preferences;
    String paketId;


    public static Activity fa;

    RelativeLayout rlDataPaspor;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_jamaah1);
        fa = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

//        if (savedInstanceState == null) {
//            Bundle extras = getIntent().getExtras();
//            if(extras == null) {
//                paketId= null;
//            } else {
//                paketId= extras.getString("paketId");
//            }
//        } else {
//            paketId= (String)savedInstanceState.getSerializable("paketId");
//        }
//
//        Toast.makeText(getApplicationContext(),paketId,Toast.LENGTH_LONG).show();

        preferences = new Preferences(RegisterJamaah1Activity.this);

        //TextView
        reg1NamaLengkap = (EditText) findViewById(R.id.reg1_nama_lengkap);
        reg1NomorKtp = (EditText) findViewById(R.id.reg1_nomor_ktp);
        reg1TempatLahir = (EditText) findViewById(R.id.reg1_tempat_lahir);
        reg1TanggalLahir = (EditText) findViewById(R.id.reg1_tanggal_lahir);
        reg1PendidikanTerakhir = (EditText)findViewById(R.id.reg1_pendidikan_terakhir);
        reg1Pekerjaan = (EditText)findViewById(R.id.reg1_pekerjaan);
        reg1PenyakitKhusus = (EditText)findViewById(R.id.reg1_penyakit_khusus);
        reg1PeruriName = (EditText)findViewById(R.id.reg1_peruri_name);
        reg1PeruriEmail = (EditText)findViewById(R.id.reg1_peruri_email);

        //Spinner
        reg1SpinnerGolonganDarah = (Spinner)findViewById(R.id.reg1_spinner_golongan_darah);
        reg1SpinnerStatusKawin = (Spinner)findViewById(R.id.reg1_spinner_status_kawin);

        //Checkbox
        reg1CheckboxKoordinator = (CheckBox)findViewById(R.id.reg1_checkbox_koordinator);

        Spinner spinner = (Spinner) findViewById(R.id.spinner_paket_umroh);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.paket_arrays, R.layout.spinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        Spinner spinnerGolDar = (Spinner) findViewById(R.id.reg1_spinner_golongan_darah);
        ArrayAdapter<CharSequence> adapterGolDar = ArrayAdapter.createFromResource(this,
                R.array.goldar_arrays, R.layout.spinner_form);
        adapterGolDar.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGolDar.setAdapter(adapterGolDar);

        Spinner spinnerKawin = (Spinner) findViewById(R.id.reg1_spinner_status_kawin);
        ArrayAdapter<CharSequence> adapterKawin = ArrayAdapter.createFromResource(this,
                R.array.kawin_arrays, R.layout.spinner_form);
        adapterKawin.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerKawin.setAdapter(adapterKawin);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", LocaleID.getLocaleId());

        reg1TanggalLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        rlDataPaspor = (RelativeLayout)findViewById(R.id.rl_data_paspor);
        rlDataPaspor.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(reg1NamaLengkap.getText().length() == 0){
                    reg1NamaLengkap.setError("Nama tidak boleh kosong");
                }else if(reg1NomorKtp.getText().length() == 0){
                    reg1NomorKtp.setError("Nomor KTP tidak boleh kosong");
                }else if(reg1TempatLahir.getText().length() == 0){
                    reg1TempatLahir.setError("Tempat lahir tidak boleh kosong");
                }else if(reg1TanggalLahir.getText().length() == 0){
                    reg1TanggalLahir.setError("Tanggal lahir tidak boleh kosong");
                }else if(reg1PendidikanTerakhir.getText().length() == 0){
                    reg1PendidikanTerakhir.setError("Pendidikan terakhir tidak boleh kosong");
                }else if(reg1Pekerjaan.getText().length() == 0){
                    reg1Pekerjaan.setError("Pekerjaan tidak boleh kosong");
                }else if(reg1PeruriEmail.getText().length() == 0){
                    reg1PeruriEmail.setError("Email app peruri tidak boleh kosong");
                }else {
                    preferences.setReg1NamaLengkap(reg1NamaLengkap.getText().toString());
                    preferences.setReg1NomorKtp(reg1NomorKtp.getText().toString());
                    preferences.setReg1TempatLahir(reg1TempatLahir.getText().toString());
                    preferences.setReg1TanggalLahir(reg1TanggalLahir.getText().toString());
                    preferences.setReg1GolonganDarah(reg1SpinnerGolonganDarah.getSelectedItem().toString());
                    preferences.setReg1StatusKawin(reg1SpinnerStatusKawin.getSelectedItem().toString());
                    preferences.setReg1PendidikanTerakhir(reg1PendidikanTerakhir.getText().toString());
                    preferences.setReg1Pekerjaan(reg1Pekerjaan.getText().toString());
                    preferences.setReg1PenyakitKhusus(reg1PenyakitKhusus.getText().toString());
                    preferences.setReg1PeruriName(reg1PeruriName.getText().toString());
                    preferences.setReg1PeruriEmail(reg1PeruriEmail.getText().toString());
                    if(reg1CheckboxKoordinator.isChecked() == true){
                        preferences.setReg1Koordinator("1");
                    }else {
                        preferences.setReg1Koordinator("0");
                    }
                    Intent intent = new Intent(RegisterJamaah1Activity.this,RegisterJamaah2Activity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void showDateDialog() {
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);

                reg1TanggalLahir.setText(dateFormatter.format(newDate.getTime()));
                reg1TanggalLahir.requestFocus();
            }
        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close_white,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume(){
        super.onResume();
        Toast.makeText(getApplicationContext(),preferences.getReg1NamaLengkap(),Toast.LENGTH_LONG).show();
        reg1NamaLengkap.setText(preferences.getReg1NamaLengkap());
        reg1NomorKtp.setText(preferences.getReg1NomorKtp());
        reg1TempatLahir.setText(preferences.getReg1TempatLahir());
        reg1TanggalLahir.setText(preferences.getReg1TanggalLahir());
        reg1PendidikanTerakhir.setText(preferences.getReg1PendidikanTerakhir());
        reg1Pekerjaan.setText(preferences.getReg1Pekerjaan());
        reg1PenyakitKhusus.setText(preferences.getReg1PenyakitKhusus());
        reg1PeruriName.setText(preferences.getReg1PeruriName());
        reg1PeruriEmail.setText(preferences.getReg1PeruriEmail());
    }
}

package com.example.user.peruriumroh.model;

import java.io.Serializable;
import java.util.List;

public class Produk implements Serializable
{
    private List<Data> data = null;

    private String errorcode = "";

    private String errormsg = "";

    public String getErrorcode() {
        return errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public List<Data> getData() {
        return data;
    }

    public int getDataSize() {
        return data.size();
    }

    public class Data implements Serializable{

        private String travel_id  = "";

        private List<Mentor> mentor = null;

        private List<Paket> paket = null;

        private String product_image = "";

        private String product_quota = "";

        private String product_status = "";

        private String product_name = "";

        private String product_desc = "";

        private String mentor_id = "";

        private String product_departure = "";

        private String product_id = "";

        private String product_rute_desc = "";

        private List<Quota> quota = null;

        private String product_payment_deadline  = "";

        public String getTravel_id() {
            return travel_id;
        }

        public List<Mentor> getMentor() {
            return mentor;
        }

        public List<Paket> getPaket() {
            return paket;
        }

        public String getProduct_image() {
            return product_image;
        }

        public String getProduct_quota() {
            return product_quota;
        }

        public String getProduct_status() {
            return product_status;
        }

        public String getProduct_name() {
            return product_name;
        }

        public String getProduct_desc() {
            return product_desc;
        }

        public String getMentor_id() {
            return mentor_id;
        }

        public String getProduct_departure() {
            return product_departure;
        }

        public String getProduct_id() {
            return product_id;
        }

        public String getProduct_rute_desc() {
            return product_rute_desc;
        }

        public List<Quota> getQuota() {
            return quota;
        }

        public String getProduct_payment_deadline() {
            return product_payment_deadline;
        }
    }

    public class Mentor implements Serializable{

        private String mentor_desc = "";

        private String mentor_id = "";

        private String mentor_telp = "";

        private String mentor_email = "";

        private String mentor_address = "";

        private String mentor_name = "";

        public String getMentor_desc() {
            return mentor_desc;
        }

        public String getMentor_id() {
            return mentor_id;
        }

        public String getMentor_telp() {
            return mentor_telp;
        }

        public String getMentor_email() {
            return mentor_email;
        }

        public String getMentor_address() {
            return mentor_address;
        }

        public String getMentor_name() {
            return mentor_name;
        }
    }

    public class Paket implements Serializable{

        private String product_id = "";

        private String package_name = "";

        private String package_id = "";

        private String package_price = "";

        private String dp = "";

        public String getDp() {
            return dp;
        }

        public String getProduct_id() {
            return product_id;
        }

        public String getPackage_name() {
            return package_name;
        }

        public String getPackage_id() {
            return package_id;
        }

        public String getPackage_price() {
            return package_price;
        }
    }

    public class Quota implements Serializable{

        private String quota_available = "";

        private String quota_book = "";

        private String product_id = "";

        private String quota_id = "";

        public String getQuota_available() {
            return quota_available;
        }

        public String getQuota_book() {
            return quota_book;
        }

        public String getProduct_id() {
            return product_id;
        }

        public String getQuota_id() {
            return quota_id;
        }
    }
}

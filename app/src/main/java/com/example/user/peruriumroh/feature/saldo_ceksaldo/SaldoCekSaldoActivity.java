package com.example.user.peruriumroh.feature.saldo_ceksaldo;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.paketumroh.PaketUmrohAdapter;

public class SaldoCekSaldoActivity extends AppCompatActivity {
    SaldoCekSaldoAdapter adapterSaldoJamaah;
    RecyclerView rvMutasiJamaah;

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saldo_cek_saldo);

        ac = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvMutasiJamaah = (RecyclerView)findViewById(R.id.rv_mutasi_jamaah);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvMutasiJamaah.setLayoutManager(layoutManager);

        rvMutasiJamaah.setHasFixedSize(true);

        adapterSaldoJamaah = new SaldoCekSaldoAdapter(10);
        rvMutasiJamaah.setAdapter(adapterSaldoJamaah);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close_white,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_close:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

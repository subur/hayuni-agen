package com.example.user.peruriumroh.feature.agen_profile_setting;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.FileUtils;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.MainDrawer.DrawerActivity;
import com.example.user.peruriumroh.feature.agen_profile_changepin.AgenChangePINActivity;
import com.example.user.peruriumroh.feature.agen_profile_password.AgenProfilePasswordActivity;
import com.example.user.peruriumroh.feature.agen_profile_qrcode.AgenProfileQRActivity;
import com.example.user.peruriumroh.feature.registerjamaah4.RegisterJamaah4Activity;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class AgenProfileSettingActivity extends AppCompatActivity {

    public static final int ACTION_REQUEST_GALLERY = 0;
    public static final int ACTION_REQUEST_CAMERA = 1;
    private static final int RC_PERMISSIONS = 2;

    Preferences preferences;
    EditText etNama,etEmail, etNoTelp;
    Button btSave,btChangePassword,btChangePIN, btSignOut, btQr;
    TextView tvGantiProfil;
    String pathToFile;
    CircleImageView civProfile;

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agen_profile_setting);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Edit Profile");

        etNama = (EditText)findViewById(R.id.et_nama);
        etEmail = (EditText)findViewById(R.id.et_email);
        etNoTelp = (EditText)findViewById(R.id.et_telephone);
        civProfile = (CircleImageView)findViewById(R.id.civ_profile);
        tvGantiProfil = (TextView)findViewById(R.id.tv_ganti_profil);
        tvGantiProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openIntent("Unggah Foto Profil");
            }
        });

        preferences = new Preferences(AgenProfileSettingActivity.this);

        Glide.with(AgenProfileSettingActivity.this).load("http://13.228.32.47/umroh/api/" + preferences.getImages()).apply(RequestOptions.bitmapTransform(new RoundedCorners(10))).into(civProfile);

        etNama.setText(preferences.getName());
        etEmail.setText(preferences.getEmail());
        etNoTelp.setText(preferences.getTelp());

        btSave = (Button)findViewById(R.id.bt_save);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiClient apiClient = new ApiClient();
                ApiClientInterface apiClientInterface = apiClient.clientInterface();

                Map<String,Object> map = new ArrayMap<>();
                map.put("msisdn",etNoTelp.getText().toString());
                map.put("email",etEmail.getText().toString());
                map.put("name",preferences.getName());

                RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (new JSONObject(map)).toString());
                Call<ResponseBody> call = apiClientInterface.agenEdit(preferences.getUserid(),preferences.getToken(),"mobile",body);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            String strResponse = response.body().string();
                        } catch (IOException e){
                            e.printStackTrace();
                        }
                        if(response.code() == 200){
                            preferences.setName(etNama.getText().toString());
                            preferences.setEmail(etEmail.getText().toString());
                            preferences.setTelp(etNoTelp.getText().toString());
                            Toast.makeText(getApplicationContext(),"Sukses Update Data",Toast.LENGTH_SHORT).show();
                        }else if(response.code() == 401){
                            Logout.logout(AgenProfileSettingActivity.this);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        AlertConnectionDialog.showConnectionAlert(AgenProfileSettingActivity.this);
                    }
                });
            }
        });

        btChangePassword = (Button)findViewById(R.id.bt_change_password);
        btChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AgenProfileSettingActivity.this,AgenProfilePasswordActivity.class);
                startActivity(intent);
            }
        });

        btChangePIN = (Button)findViewById(R.id.bt_change_pin);
        btChangePIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(AgenProfileSettingActivity.this,AgenChangePINActivity.class);
                startActivity(intent);
            }
        });

        btQr = (Button)findViewById(R.id.bt_qr);
        btQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(AgenProfileSettingActivity.this, AgenProfileQRActivity.class);
                startActivity(intent);
            }
        });

        btSignOut = (Button)findViewById(R.id.bt_logout);
        btSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Logout.logoutNoAlert(AgenProfileSettingActivity.this);
            }
        });
    }

    private void openIntent(final String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AgenProfileSettingActivity.this);
        builder.setTitle(title);
        builder.setItems(new CharSequence[]{"Gallery", "Camera"},
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                startIntentGallery();

                                break;

                            case 1:
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    if(ContextCompat.checkSelfPermission(AgenProfileSettingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                                            ContextCompat.checkSelfPermission(AgenProfileSettingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                                            ContextCompat.checkSelfPermission(AgenProfileSettingActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                                        startIntentCamera();
                                    }else {
                                        requestPermission();
                                    }
                                }else {
                                    if(ContextCompat.checkSelfPermission(AgenProfileSettingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                                            ContextCompat.checkSelfPermission(AgenProfileSettingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                                            ContextCompat.checkSelfPermission(AgenProfileSettingActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                                        startIntentCamera();
                                    }
                                }
                                break;

                            default:

                                break;
                        }
                    }
                });
        builder.show();
    }

    private void requestPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE) &&
                ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA)){
            new AlertDialog.Builder(this)
                    .setTitle("Membutuhkan Izin Perangkat")
                    .setMessage("Izin Camera, Write Storage dan Read Storage")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(AgenProfileSettingActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA},RC_PERMISSIONS);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create().show();
        }else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA},RC_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RC_PERMISSIONS){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                startIntentCamera();
            }
        }
    }

    public void startIntentGallery(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        setIntent(intent);
        startActivityForResult(intent,ACTION_REQUEST_GALLERY);
    }

    public void startIntentCamera(){
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePicture.resolveActivity(getPackageManager()) != null){
            File photoFile = null;
            photoFile = createPhotoFile();

            if(photoFile != null){
                pathToFile = photoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(AgenProfileSettingActivity.this,"com.example.user.peruriumroh",photoFile);
                takePicture.putExtra(MediaStore.EXTRA_OUTPUT,photoURI);
                startActivityForResult(takePicture, ACTION_REQUEST_CAMERA);
            }
        }
    }

    private File createPhotoFile() {
        String name = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
//        File storageDirektori = new File(Environment.getExternalStorageDirectory() + "/" +"com.example.user.peruriumroh/");
        File storageDirektori = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/Peruri");
        if (!storageDirektori.exists()) {
            Log.d("PERURI UMROH :", "createPhotoFile: Tidak EXIST" + "DIR : " + storageDirektori);
            storageDirektori.mkdir();
        }
        File image = null;
        try {
            image = File.createTempFile("PERURI_"+name,".jpg",storageDirektori);
        }catch (IOException e){
            Log.d("PERURI", "Exception: " + e.toString());
        }
        return image;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode){
            case ACTION_REQUEST_GALLERY:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                    changeProfile(selectedImage);
//                    File original1 = FileUtils.getFile(this,selectedImage);
//                    String[] imageName = original1.toString().split("/");
//                    Toast.makeText(getApplicationContext(),imageName[imageName.length-1],Toast.LENGTH_LONG).show();
                }
                break;

            case ACTION_REQUEST_CAMERA:
                if(resultCode == RESULT_OK){
//                    Uri cameraUri = Uri.fromFile(new File(pathToFile));
//
//                    Log.d("TAG", "onActivityResult: " + cameraUri);
//                    changeProfile(cameraUri);
                    Bitmap bitmap = BitmapFactory.decodeFile(pathToFile);
                    String path = MediaStore.Images.Media.insertImage(getApplicationContext().getContentResolver(), bitmap, "Title", null);
                    Uri tempUri = Uri.parse(path);
                    Log.d("TAG", "onActivityResult: " + tempUri);
                    changeProfile(tempUri);
                    ;

//                    String[] imageName = pathToFile.toString().split("/");
//                    Toast.makeText(getApplicationContext(),imageName[imageName.length - 1],Toast.LENGTH_LONG).show();
//                    Uri selectedImage = data.getData();
//                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
//                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//
//                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
//                    File destination = new File(Environment.getExternalStorageDirectory() + "/" +
//                            "com.example.user.peruriumroh/");
//                    destination.mkdir();
//                    destination = new File(Environment.getExternalStorageDirectory() + "/" +
//                            "com.example.user.peruriumroh/", "IMG_" + timeStamp + ".jpg");
//                    Log.e("Activity", "Pick from Camera::>>> " + destination);
//                    FileOutputStream fo;
//                    try {
//                        destination.createNewFile();
//                        fo = new FileOutputStream(destination);
//                        fo.write(bytes.toByteArray());
//                        fo.close();
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    String imgPath = destination.getAbsolutePath();
//                    String[] imageName = imgPath.toString().split("/");
//                    Toast.makeText(getApplicationContext(),imgPath,Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void changeProfile(Uri fileUri){
        ApiClient apiClient = new ApiClient();
        ApiClientInterface apiClientInterface = apiClient.clientInterface();

        Call<ResponseBody> call = apiClientInterface.agenEditPP(
                preferences.getUserid(),
                preferences.getToken(),
                "mobile",
                convertToPart(fileUri)
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(getApplicationContext(),"DAPAT RESPON GANTI PP",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AlertConnectionDialog.showConnectionAlert(AgenProfileSettingActivity.this);
            }
        });
    }

    private MultipartBody.Part convertToPart(Uri uriImage){
        File original = FileUtils.getFile(this,uriImage);
        Log.d("TAG", "Original Name File: " + original);
        RequestBody filePart = RequestBody.create(
                MediaType.parse(getContentResolver().getType(uriImage)),
                original);

        MultipartBody.Part file = MultipartBody.Part.createFormData("image",original.getName(),filePart);
        return file;
    }
}

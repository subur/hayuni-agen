package com.example.user.peruriumroh.feature.paketumroh;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.peruriumroh.R;

public class PaketUmrohAdapter extends RecyclerView.Adapter<PaketUmrohAdapter.PaketViewHolder> {

    private int mNumberItems;
    public PaketUmrohAdapter(int mNumberItems){
        this.mNumberItems = mNumberItems;
    }

    @NonNull
    @Override
    public PaketViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutItem = R.layout.item_paketumroh;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachtoParentImmediately = false;

        View view = inflater.inflate(layoutItem,viewGroup,shouldAttachtoParentImmediately);
        PaketViewHolder viewHolder = new PaketViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PaketViewHolder paketViewHolder, int i) {
        paketViewHolder.bind(i);
    }

    @Override
    public int getItemCount() {
        return mNumberItems;
    }

    class PaketViewHolder extends RecyclerView.ViewHolder{

        public PaketViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        void bind(int listIndex){

        }
    }
}

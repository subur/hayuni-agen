package com.example.user.peruriumroh.model;

import java.io.Serializable;
import java.util.List;

public class PaketUmroh implements Serializable {

    String umrohTitle;
    String umrohTanggal;
    String umrohImg;
    String umrohSimpleDescription;
    String umrohStartFrom;
    String umrohSlotTersisa;
    String umrohSlotTotal;
    String umrohBiayadll;
    String umrohPembimbing;
    String umrohPembimbingTitle;
    String umrohRoute;
    String umrohTemaKajian;
    List<String> umrohPaket;

    public PaketUmroh(String umrohTitle, String umrohTanggal, String umrohImg, String umrohSimpleDescription, String umrohStartFrom, String umrohSlotTersisa, String umrohSlotTotal, String umrohBiayadll, String umrohPembimbing, String umrohPembimbingTitle, String umrohRoute, String umrohTemaKajian, List<String> umrohPaket) {
        this.umrohTitle = umrohTitle;
        this.umrohTanggal = umrohTanggal;
        this.umrohImg = umrohImg;
        this.umrohSimpleDescription = umrohSimpleDescription;
        this.umrohStartFrom = umrohStartFrom;
        this.umrohSlotTersisa = umrohSlotTersisa;
        this.umrohSlotTotal = umrohSlotTotal;
        this.umrohBiayadll = umrohBiayadll;
        this.umrohPembimbing = umrohPembimbing;
        this.umrohPembimbingTitle = umrohPembimbingTitle;
        this.umrohRoute = umrohRoute;
        this.umrohTemaKajian = umrohTemaKajian;
        this.umrohPaket = umrohPaket;
    }

    public String getUmrohTitle() {
        return umrohTitle;
    }

    public void setUmrohTitle(String umrohTitle) {
        this.umrohTitle = umrohTitle;
    }

    public String getUmrohTanggal() {
        return umrohTanggal;
    }

    public void setUmrohTanggal(String umrohTanggal) {
        this.umrohTanggal = umrohTanggal;
    }

    public String getUmrohImg() {
        return umrohImg;
    }

    public void setUmrohImg(String umrohImg) {
        this.umrohImg = umrohImg;
    }

    public String getUmrohSimpleDescription() {
        return umrohSimpleDescription;
    }

    public void setUmrohSimpleDescription(String umrohSimpleDescription) {
        this.umrohSimpleDescription = umrohSimpleDescription;
    }

    public String getUmrohStartFrom() {
        return umrohStartFrom;
    }

    public void setUmrohStartFrom(String umrohStartFrom) {
        this.umrohStartFrom = umrohStartFrom;
    }

    public String getUmrohSlotTersisa() {
        return umrohSlotTersisa;
    }

    public void setUmrohSlotTersisa(String umrohSlotTersisa) {
        this.umrohSlotTersisa = umrohSlotTersisa;
    }

    public String getUmrohSlotTotal() {
        return umrohSlotTotal;
    }

    public void setUmrohSlotTotal(String umrohSlotTotal) {
        this.umrohSlotTotal = umrohSlotTotal;
    }

    public String getUmrohBiayadll() {
        return umrohBiayadll;
    }

    public void setUmrohBiayadll(String umrohBiayadll) {
        this.umrohBiayadll = umrohBiayadll;
    }

    public String getUmrohPembimbing() {
        return umrohPembimbing;
    }

    public void setUmrohPembimbing(String umrohPembimbing) {
        this.umrohPembimbing = umrohPembimbing;
    }

    public String getUmrohPembimbingTitle() {
        return umrohPembimbingTitle;
    }

    public void setUmrohPembimbingTitle(String umrohPembimbingTitle) {
        this.umrohPembimbingTitle = umrohPembimbingTitle;
    }

    public String getUmrohRoute() {
        return umrohRoute;
    }

    public void setUmrohRoute(String umrohRoute) {
        this.umrohRoute = umrohRoute;
    }

    public String getUmrohTemaKajian() {
        return umrohTemaKajian;
    }

    public void setUmrohTemaKajian(String umrohTemaKajian) {
        this.umrohTemaKajian = umrohTemaKajian;
    }

    public List<String> getUmrohPaket() {
        return umrohPaket;
    }

    public void setUmrohPaket(List<String> umrohPaket) {
        this.umrohPaket = umrohPaket;
    }
}

package com.example.user.peruriumroh.etc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.ArrayMap;
import android.util.Log;

import com.example.user.peruriumroh.feature.MainDrawer.DrawerActivity;
import com.example.user.peruriumroh.feature.agen_profile_changepin.AgenChangePINActivity;
import com.example.user.peruriumroh.feature.agen_profile_changepin2.AgenChangePIN2Activity;
import com.example.user.peruriumroh.feature.agen_profile_changepin3.AgenChangePIN3Activity;
import com.example.user.peruriumroh.feature.agen_profile_password.AgenProfilePasswordActivity;
import com.example.user.peruriumroh.feature.agen_profile_setting.AgenProfileSettingActivity;
import com.example.user.peruriumroh.feature.detailpaketumroh.DetailPaketUmrohActivity;
import com.example.user.peruriumroh.feature.home.HomeActivity;
import com.example.user.peruriumroh.feature.jamaah_detailjamaah.DetailJamaahActivity;
import com.example.user.peruriumroh.feature.login.LoginActivity;
import com.example.user.peruriumroh.feature.pay_chooser.PeruriPayChooserActivity;
import com.example.user.peruriumroh.feature.pay_manualjamaah.PayManualJamaahActivity;
import com.example.user.peruriumroh.feature.pay_peruripin.PayPeruriPINActivity;
import com.example.user.peruriumroh.feature.pay_scansqrcjamaah.PayScanSQRCJamaahActivity;
import com.example.user.peruriumroh.feature.pay_voucher_sukses.PayVoucherSuksesActivity;
import com.example.user.peruriumroh.feature.pembayaranhome.PembayaranActivity;
import com.example.user.peruriumroh.feature.registerjamaahbaru.RegisterJamaahBaruActivity;
import com.example.user.peruriumroh.feature.registerjamaahbranch.RegisterJamaahBranchActivity;
import com.example.user.peruriumroh.feature.registerjamaahterdaftar1.RegisterJamaahTerdaftar1Activity;
import com.example.user.peruriumroh.feature.registerjamaahterdaftar2.RegisterJamaahTerdaftar2Activity;
import com.example.user.peruriumroh.feature.saldo_scanSQRC.SaldoScanSQRCActivity;
import com.example.user.peruriumroh.feature.topup_input_amount.TopUpInputAmountActivity;
import com.example.user.peruriumroh.feature.transfer_branch.TransferBranchActivity;
import com.example.user.peruriumroh.feature.transfer_manual.TransferManualActivity;
import com.example.user.peruriumroh.feature.transfer_manual_amount.TransferManualAmountActivity;
import com.example.user.peruriumroh.feature.transfer_pin.TransferPINActivity;
import com.example.user.peruriumroh.feature.voucher_detail.VoucherDetailActivity;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class Logout {
    //NETWORK INTERFACE
    ApiClient clientInterface = new ApiClient();
    ApiClientInterface apiClient = clientInterface.clientInterface();

    public static void logout(final Activity activity){
        final Preferences settings = new Preferences(activity);

        AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
        builder1.setMessage("Sesi Anda Telah Habis");
        builder1.setCancelable(false);
        builder1.setNeutralButton("LOGIN", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Map<String, Object> map = new ArrayMap<>();
                map.put("user_id", settings.getUserid());
                map.put("access_token", settings.getToken());
                map.put("location", "");
                map.put("platform", "mobile");

                RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(map)).toString());

                ApiClient clientInterface = new ApiClient();
                ApiClientInterface apiClient = clientInterface.clientInterface();
                Call<ResponseBody> call = apiClient.agenLogout(body);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.code() == 201){
                            settings.SettingSetNull();
                            activity.finish();
                            closeIntent();
                            Intent intent = new Intent(activity,LoginActivity.class);
                            activity.startActivity(intent);
                        }else {
                            settings.SettingSetNull();
                            activity.finish();
                            closeIntent();
                            Intent intent = new Intent(activity,LoginActivity.class);
                            activity.startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
//                        settings.SettingSetNull();
//                        activity.finish();
//                        closeIntent();
//                        Intent intent = new Intent(activity,LoginActivity.class);
//                        activity.startActivity(intent);
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(activity);
                        builder2.setTitle("Anda sedang offline");
                        builder2.setMessage("Periksa koneksi WiFi atau data seluler dan coba lagi");
                        builder2.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder2.show();
                    }
                });
            }
        });
        builder1.show();
    }

    public static void logoutNoAlert(final Activity activity){
        final Preferences settings = new Preferences(activity);

        Map<String, Object> map = new ArrayMap<>();
        map.put("user_id", settings.getUserid());
        map.put("access_token", settings.getToken());
        map.put("location", "");
        map.put("platform", "mobile");

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(map)).toString());

        ApiClient clientInterface = new ApiClient();
        ApiClientInterface apiClient = clientInterface.clientInterface();
        Call<ResponseBody> call = apiClient.agenLogout(body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 201){
                    settings.SettingSetNull();
                    activity.finish();
                    closeIntent();
                    Intent intent = new Intent(activity,LoginActivity.class);
                    activity.startActivity(intent);
                }else {
                    settings.SettingSetNull();
                    activity.finish();
                    closeIntent();
                    Intent intent = new Intent(activity,LoginActivity.class);
                    activity.startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AlertConnectionDialog.showConnectionAlert(activity);
            }
        });
    }

    private static void closeIntent() {
        //Agen Profile
        try {
            AgenProfilePasswordActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Agen Setting
        try {
            AgenProfileSettingActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Detail Paket Umroh
        try {
            DetailPaketUmrohActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Home
        try {
            HomeActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Jamaah Detail Activity
        try {
            DetailJamaahActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Drawer Activity
        try {
            DrawerActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Pay Chooser
        try {
            PeruriPayChooserActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Pay Manual Jamaah
        try {
            PayManualJamaahActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Pay Peruri PIN
        try {
            PayPeruriPINActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Pay Scan SQRC
        try {
            PayScanSQRCJamaahActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Pay Voucher Sukses
        try {
            PayVoucherSuksesActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Pembayaran Home
        try {
            PembayaranActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Register Jamaah Baru
        try {
            RegisterJamaahBaruActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Register Jamaah Branch
        try {
            RegisterJamaahBranchActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Register Jamaah Terdaftar1
        try {
            RegisterJamaahTerdaftar1Activity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Register Jamaah Terdaftar 2
        try {
            RegisterJamaahTerdaftar2Activity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Scan Saldo
        try {
            SaldoScanSQRCActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //TopupINput Amount
        try {
            TopUpInputAmountActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Voucher Detail
        try {
            VoucherDetailActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Transfer Branch
        try {
            TransferBranchActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Transfer Manual Search Email
        try {
            TransferManualActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Transfer Manual Amount
        try {
            TransferManualAmountActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Transfer PIN
        try {
            TransferPINActivity.ac.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Change PIN
        try {
            AgenChangePINActivity.act.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Change PIN 2
        try {
            AgenChangePIN2Activity.act.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }

        //Change PIN 3
        try {
            AgenChangePIN3Activity.act.finish();
        }catch (Exception e){
            Log.d(TAG, "closeIntent: False");
        }
    }
}

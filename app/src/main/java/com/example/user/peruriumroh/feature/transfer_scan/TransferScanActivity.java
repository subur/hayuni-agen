package com.example.user.peruriumroh.feature.transfer_scan;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.densowave.dwqrkit.DWQRKit;
import com.densowave.dwqrkit.DWQRScanMode;
import com.densowave.dwqrkit.DWQRScanView;
import com.densowave.dwqrkit.listener.DWQRCodeNoticeListener;
import com.densowave.dwqrkit.listener.DWQRLicenseListener;
import com.densowave.dwqrkit.listener.DWQRScanViewCreateListener;
import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.AlertDialog;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.transfer_manual.TransferManualActivity;
import com.example.user.peruriumroh.feature.transfer_manual_amount.TransferManualAmountActivity;
import com.example.user.peruriumroh.model.EachAgenLogin;
import com.example.user.peruriumroh.model.ListJamaah;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransferScanActivity extends AppCompatActivity implements ViewTreeObserver.OnGlobalLayoutListener,
        DWQRCodeNoticeListener,DWQRScanViewCreateListener,DWQRLicenseListener,android.view.View.OnClickListener{

    protected static final String TAG_CAMERA_FRAGMENT = "camera";
    private int REQUEST_CODE_CAMERA_PERMISSION = 0x01;
    private FrameLayout fl_camera;
    private DWQRScanView preview;
    private DWQRKit dwqrKit;

    public static Activity ac;

    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();

    Preferences settings;

    ProgressDialog progressDialog;

    Button btSubmit;

    Integer openStatus = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_scan);

        ac = this;

        settings = new Preferences(TransferScanActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Transfer Peruri Tunai");

        progressDialog = new ProgressDialog(TransferScanActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        setupSqrc(savedInstanceState);
    }

    private void setupSqrc(Bundle savedInstanceState) {
        dwqrKit = DWQRKit.sharedManager();
        dwqrKit.isVibrationEnable = false;
        dwqrKit.isAudioEnable = false;
        dwqrKit.isFlashEnable = false;
        dwqrKit.isServerConnectionEnable = true;
        dwqrKit.isLogoDisplayEnable = false;
        dwqrKit.setMode(DWQRScanMode.DWQR_NOMALMODE.getValue());

        if(Build.VERSION.SDK_INT >= 23){
            if(PermissionChecker.checkSelfPermission(TransferScanActivity.this,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(TransferScanActivity.this,new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                },REQUEST_CODE_CAMERA_PERMISSION);
            }
        }

        if(savedInstanceState == null){
            fl_camera = (FrameLayout)findViewById(R.id.frameScanSQRC);
            fl_camera.getViewTreeObserver().addOnGlobalLayoutListener(this);
        }

        AssetManager assetManager = this.getAssets();
        String filePath = this.getApplicationContext().getFilesDir().toString() + "/license.txt";
        File file = new File(filePath);

        try {
            InputStream in = assetManager.open("license.txt");
            OutputStream out = new BufferedOutputStream(new FileOutputStream(file));

            copyFile(in,out);
            in.close();
            out.flush();
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[8196];
        int read;
        while ((read = in.read(buffer)) != -1){
            out.write(buffer,0,read);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_CAMERA_PERMISSION && grantResults[0] != PackageManager.PERMISSION_GRANTED){
            finish();
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onGlobalLayout() {
        preview = new DWQRScanView();
        fl_camera.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        preview.setOnQRCodeFoundNoticeListener(this);
        preview.setDwQrScanViewCreateListener(this);
        preview.setDwQrLicenseListener(this);
        getSupportFragmentManager().beginTransaction().add(R.id.frameScanSQRC,preview,TAG_CAMERA_FRAGMENT).commit();
    }

    @Override
    public void onNoticeDecodeResult(String s, String s1, byte[] bytes, int i, String s2, int i1) {
        try{
            String[] explode1 = s.split(",");
            String regex = "\\s+";
            String role = explode1[3].replaceAll(regex,"");
            String roleexplode[] = role.split(":");
            String roleval = roleexplode[1];

            String email = explode1[2].replaceAll(regex,"");

            Log.d("TAG", roleval+"/"+email);

            if(roleval.equals("Jamaah")){

                if(openStatus == 0) {

                    openStatus = 1;

                    //Transfer Jamaah
                    progressDialog.setMessage("Mencari Pengguna...");
                    progressDialog.show();
                    Call<ListJamaah> getJamaah = apiClientInterface.jamaahGetJamaahByEmail(
                            settings.getUserid(),
                            settings.getToken(),
                            "mobile",
                            email
                    );
                    getJamaah.enqueue(new Callback<ListJamaah>(){
                        @Override
                        public void onResponse(Call<ListJamaah> call, Response<ListJamaah> response) {
                            if(response.code() == 200){
                                if(response.body().getData() != null){
                                    if(response.body().getErrormsg().equals("OK")){
                                        progressDialog.dismiss();
                                        Intent intent = new Intent(TransferScanActivity.this,TransferManualAmountActivity.class);
                                        intent.putExtra("role","Jamaah");
                                        intent.putExtra("id",response.body().getData().get(0).getJamaah_id());
                                        intent.putExtra("nama",response.body().getData().get(0).getJamaah_name());
                                        intent.putExtra("email",response.body().getData().get(0).getContact().get(0).getContact_email());
                                        startActivityForResult(intent,1);
                                    }
                                }else {
                                    progressDialog.dismiss();
                                    AlertDialog.showAlert(TransferScanActivity.this,"Transfer Peruri Tunai","Email Pengguna tidak ditemukan");
                                }
                            }else if(response.code() == 401){
                                progressDialog.dismiss();
                                Logout.logout(TransferScanActivity.this);
                            } else {
                                progressDialog.dismiss();
                                AlertDialog.showAlert(TransferScanActivity.this,"Transfer Peruri Tunai","Email Pengguna tidak ditemukan");
                            }
                        }

                        @Override
                        public void onFailure(Call<ListJamaah> call, Throwable t) {
                            progressDialog.dismiss();
                            if(t instanceof IOException){
                                AlertConnectionDialog.showConnectionAlert(TransferScanActivity.this);
                            }else {
                                AlertDialog.showAlert(TransferScanActivity.this,"Peruri Umroh", "Terjadi kesalahan sistem");
                            }
                        }
                    });
                }

            }else if(roleval.equals("Agent")){

                if(openStatus == 0) {

                    openStatus = 1;

                    //Transfer Agen
                    progressDialog.setMessage("Mencari Pengguna...");
                    progressDialog.show();

                    Call<EachAgenLogin> getAgenData = apiClientInterface.getAgenByEmail(
                            settings.getUserid(),
                            settings.getToken(),
                            "mobile",
                            email
                    );

                    getAgenData.enqueue(new Callback<EachAgenLogin>() {
                        @Override
                        public void onResponse(Call<EachAgenLogin> call, Response<EachAgenLogin> response) {
                            if(response.code() == 200){
                                //PARSING DATA AGEN
                                if(response.body().getErrorcode().equals("00000")){
                                    if(response.body().getData() != null){
                                        if(response.body().getErrormsg().equals("OK")){
                                            progressDialog.dismiss();
                                            Intent intent = new Intent(TransferScanActivity.this,TransferManualAmountActivity.class);
                                            intent.putExtra("role","Agent");
                                            intent.putExtra("id",response.body().getData().get(0).getAgent_id());
                                            intent.putExtra("nama",response.body().getData().get(0).getAgent_name());
                                            intent.putExtra("email",response.body().getData().get(0).getAgent_email());
                                            startActivityForResult(intent,1);
                                        }
                                    }
                                }else if(response.body().getErrorcode().equals("00001")){
                                    progressDialog.dismiss();
                                    AlertDialog.showAlert(TransferScanActivity.this,"Transfer Peruri Tunai","Email Pengguna tidak ditemukan");
                                }
                                //OPEN AMOUNT PAGE
                            }else if(response.code() == 401){
                                progressDialog.dismiss();
                                Logout.logout(TransferScanActivity.this);
                            } else {
                                progressDialog.dismiss();
                                AlertDialog.showAlert(TransferScanActivity.this,"Transfer Peruri Tunai","Email Pengguna tidak ditemukan");
                            }
                        }

                        @Override
                        public void onFailure(Call<EachAgenLogin> call, Throwable t) {
                            if(t instanceof IOException){
                                progressDialog.dismiss();
                                AlertConnectionDialog.showConnectionAlert(TransferScanActivity.this);
                            }else {
                                progressDialog.dismiss();
                                AlertDialog.showAlert(TransferScanActivity.this,"Peruri Umroh", "Terjadi kesalahan sistem");
                            }
                        }
                    });
                }
            }
        }catch (Exception e){
            Log.d("TAG", "onNoticeDecodeResult: " + e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int curItem = item.getItemId();
        switch (curItem){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == 1){
            openStatus = 0;
        }
    }

    @Override
    public void onNoticeLicenseExpired(int i) {

    }

    @Override
    public void onCreateScanView(FrameLayout frameLayout) {
        fl_camera = frameLayout;
        preview.readLicenseFile();
    }
}

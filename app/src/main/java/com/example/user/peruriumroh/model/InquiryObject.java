package com.example.user.peruriumroh.model;

public class InquiryObject {
    String umrohId;
    String jamaahId;
    String jamaahName;

    public String getUmrohId() {
        return umrohId;
    }

    public void setUmrohId(String umrohId) {
        this.umrohId = umrohId;
    }

    public String getJamaahId() {
        return jamaahId;
    }

    public void setJamaahId(String jamaahId) {
        this.jamaahId = jamaahId;
    }

    public String getJamaahName() {
        return jamaahName;
    }

    public void setJamaahName(String jamaahName) {
        this.jamaahName = jamaahName;
    }

    public InquiryObject(String v1, String v2, String v3) {
        umrohId = v1;
        jamaahId = v2;
        jamaahName = v3;
    }

    @Override
    public String toString() {
        return jamaahName;
    }
}

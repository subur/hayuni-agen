package com.example.user.peruriumroh.feature.agen_profile_changepin;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.agen_profile_changepin2.AgenChangePIN2Activity;
import com.goodiebag.pinview.Pinview;

public class AgenChangePINActivity extends AppCompatActivity {

    Pinview pinview;

    public static Activity act;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agen_change_pin);

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        act = this;

        pinview = (Pinview)findViewById(R.id.pinview);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
                String pin = pinview.getValue();

                Intent intent = new Intent(AgenChangePINActivity.this, AgenChangePIN2Activity.class);
                intent.putExtra("currentPIN", pin);
                startActivity(intent);
            }
        });
    }
}

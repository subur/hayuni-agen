package com.example.user.peruriumroh.feature.jamaah_detailkoordinator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.jamaah_detailjamaah.DetailJamaahAdapter;

public class DetailKoordinatorActivity extends AppCompatActivity {
    RecyclerView rvMutasiKoordinatorDetail;
    DetailKoordinatorAdapter detailKoordinatorAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_koordinator);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvMutasiKoordinatorDetail = (RecyclerView)findViewById(R.id.rv_mutasi_koordinator_detail);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvMutasiKoordinatorDetail.setLayoutManager(layoutManager);

        rvMutasiKoordinatorDetail.setHasFixedSize(true);

        detailKoordinatorAdapter = new DetailKoordinatorAdapter(100);
        rvMutasiKoordinatorDetail.setAdapter(detailKoordinatorAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close_white,menu);
        return true;
    }
}

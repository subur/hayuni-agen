package com.example.user.peruriumroh.feature.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.AlertDialog;
import com.example.user.peruriumroh.etc.BottomSheetConnectionDialog;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.etc.ValidEmail;
import com.example.user.peruriumroh.feature.home.HomeActivity;
import com.example.user.peruriumroh.feature.landing.WelcomeActivity;
import com.example.user.peruriumroh.feature.transfer_pin.TransferPINActivity;
import com.example.user.peruriumroh.model.AgentData;
import com.example.user.peruriumroh.model.EachAgenLogin;
import com.example.user.peruriumroh.model.SignInPPay;
import com.example.user.peruriumroh.model.userData;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity{
    Button btLoginApp;
    EditText etUsername,etPassword;
    Preferences preferences;
    List<AgentData> agentData = new ArrayList<AgentData>();

    ApiClient clientInterface = new ApiClient();
    ApiClientInterface apiClient = clientInterface.clientInterface();

    ProgressDialog progressBar;

    ImageView ivLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        preferences = new Preferences(LoginActivity.this);

        //PROGRESS BAR
        progressBar = new ProgressDialog(LoginActivity.this);
        progressBar.setCancelable(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        etUsername = (EditText) findViewById(R.id.et_username);
        etPassword = (EditText) findViewById(R.id.et_password);

        ivLogo = (ImageView)findViewById(R.id.iv_logo);
        Glide.with(this).load(R.drawable.hayuni_logo_panjang).into(ivLogo);

        checkToken();

        btLoginApp = (Button)findViewById(R.id.bt_login);
        btLoginApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                boolean pattern = ValidEmail.isValidEmail(etUsername.getText());
                if(pattern){
                    btLoginApp.setEnabled(false);
                    loginRequest();
                }else {
                    etUsername.setError("Masukan Email");
                }
            }
        });
    }

    private void loginRequest(){
        progressBar.setMessage("Tunggu Sebentar...");
        progressBar.show();
        Call<userData> call = apiClient.agenLogin(etUsername.getText().toString(),etPassword.getText().toString(),"Agent","mobile");
        call.enqueue(new Callback<userData>(){
            @Override
            public void onResponse(Call<userData> call, Response<userData> response) {
                if(response.code() == 201){
                    try {
                        if(response.body().getData().getUser().getRelated_key().equals("agent_id")){
                            preferences.setToken(response.body().getData().getAccess_token());
                            preferences.setUserid(response.body().getData().getUser().getUser_id());
                            preferences.setName(response.body().getData().getUser().getName());
                            preferences.setEmail(response.body().getData().getUser().getEmail());
                            preferences.setTelp(response.body().getData().getUser().getMsisdn());
                            preferences.setImages(response.body().getData().getUser().getUser_image());
                            preferences.setAgent_id(response.body().getData().getUser().getRelated_id());
                            preferences.setPpayusername(response.body().getData().getPeruri_pay().get(0).getUsername());
                            preferences.setTravelId(response.body().getData().getAgent().getTravel_id());
                            preferences.setQr(response.body().getData().getAgent().getSqrc());
                            prerequisitePeruriTunai();
                        }else {
                            Toast.makeText(getApplicationContext(),"Data Tidak Ditemukan",Toast.LENGTH_SHORT).show();
                            preferences.SettingSetNull();
                            preferences.SettingSetTMoneyNull();
                            progressBar.dismiss();
                            btLoginApp.setEnabled(true);
                        }
                    }catch (Exception e){
                        Toast.makeText(getApplicationContext(),"Email atau password Salah",Toast.LENGTH_SHORT).show();
                        preferences.SettingSetNull();
                        preferences.SettingSetTMoneyNull();
                        progressBar.dismiss();
                        btLoginApp.setEnabled(true);
                    }
                }else if(response.code() == 401){
                    Toast.makeText(getApplicationContext(),"Email atau password Salah",Toast.LENGTH_SHORT).show();
                    progressBar.dismiss();
                    btLoginApp.setEnabled(true);
                }else {
                    AlertDialog.showAlert(LoginActivity.this,"Peruri Umroh","Agen belum terdaftar");
                    progressBar.dismiss();
                    btLoginApp.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<userData> call, Throwable t) {
                progressBar.dismiss();
                AlertConnectionDialog.showConnectionAlert(LoginActivity.this);
                btLoginApp.setEnabled(true);
            }
        });
    }

    private void checkToken(){
        if(preferences.getToken() != null && !preferences.getToken().equals("")){
            prerequisitePeruriTunai();
        }
    }

    private void prerequisitePeruriTunai(){
        Call<EachAgenLogin> getAgenData = apiClient.getAgenByIdAgent(
          preferences.getUserid(),
          preferences.getToken(),
          "mobile",
          preferences.getAgent_id()
        );
        getAgenData.enqueue(new Callback<EachAgenLogin>(){
            @Override
            public void onResponse(Call<EachAgenLogin> call, Response<EachAgenLogin> response) {
                if(response.code() == 200){

                    //Jika Sudah Registrasi
                    if(response.body().getData().get(0).getPeruri_pay().get(0).getStatus_registrasi().equals("1")){

                        //Jika Sudah Integrasi
                        if(response.body().getData().get(0).getPeruri_pay().get(0).getStatus_integrasi().equals("1")){

                            //SignIn PeruriPay
                            signInPeruriTunai();
                        }

                        //Jika Belum Integrasi
                        else if(response.body().getData().get(0).getPeruri_pay().get(0).getStatus_integrasi().equals("0")){

                            //Open WhiteLabel
                            openWhiteLabel();

                        }
                    }
                    //Jika Belum Registrasi
                    else if(response.body().getData().get(0).getPeruri_pay().get(0).getStatus_registrasi().equals("0")){
                        AlertDialog.showAlert(LoginActivity.this,"Peruri Tunai belum terdaftar","Silahkan hubungi travel untuk registrasi akun peruri tunai.");
                        preferences.SettingSetNull();
                        preferences.SettingSetTMoneyNull();
                        progressBar.dismiss();
                        btLoginApp.setEnabled(true);
                    }
                }else {
                    //Error Dialog
                    AlertDialog.showAlert(LoginActivity.this,"Koneksi Bermasalah","Silahkan coba beberapa saat lagi.");
                    preferences.SettingSetNull();
                    preferences.SettingSetTMoneyNull();
                    progressBar.dismiss();
                    btLoginApp.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<EachAgenLogin> call, Throwable t) {
                //Error Dialog
                AlertConnectionDialog.showConnectionAlert(LoginActivity.this);
                preferences.SettingSetNull();
                preferences.SettingSetTMoneyNull();
                progressBar.dismiss();
                btLoginApp.setEnabled(true);
            }
        });
    }

    private void openWhiteLabel() {

        //NEED TO UPDATE

        progressBar.dismiss();
        preferences.SettingSetTMoneyNull();
        preferences.SettingSetNull();
        btLoginApp.setEnabled(true);
        Intent intent = new Intent(LoginActivity.this,WhiteLabelActivity.class);
        startActivity(intent);
    }

    private void signInPeruriTunai(){
        Call<SignInPPay> call = apiClient.agenSignInPPay(
            preferences.getUserid(),
            preferences.getToken(),
            "mobile",
            preferences.getPpayusername()
        );
        call.enqueue(new Callback<SignInPPay>(){
            @Override
            public void onResponse(Call<SignInPPay> call, Response<SignInPPay> response) {
                if(response.code() == 200){
                    if(response.body().getErrorcode().equals("00000")){
                        if(response.body().getData() != null){
                            if(response.body().getData().getResultCode().equals("0")){
                                if (response.body().getData().getLogin().equals("true")){
                                    //Peruri Pay Sign In Preference
                                    preferences.setBalance(response.body().getData().getUser().getBalance());
                                    preferences.setIdTmoney(response.body().getData().getUser().getIdTmoney());
                                    preferences.setIdFusion(response.body().getData().getUser().getIdFusion());
                                    preferences.setTokenppay(response.body().getData().getUser().getToken());
                                    progressBar.dismiss();
                                    WelcomeActivity.fa.finish();
                                    finish();
                                    Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
                                    startActivity(intent);
                                }else {
                                    Toast.makeText(getApplicationContext(),"Data Tidak Ditemukan",Toast.LENGTH_SHORT).show();
                                    preferences.SettingSetNull();
                                    preferences.SettingSetTMoneyNull();
                                    progressBar.dismiss();
                                    btLoginApp.setEnabled(true);
                                }
                            }
                            else {
                                AlertDialog.showAlert(LoginActivity.this,"Peruri Tunai",response.body().getData().getResultDesc());
                                preferences.SettingSetNull();
                                preferences.SettingSetTMoneyNull();
                                progressBar.dismiss();
                                btLoginApp.setEnabled(true);
                            }
                        }else {
                            AlertDialog.showAlert(LoginActivity.this,"Koneksi Server Bermasalah","Silahkan coba beberapa saat lagi.");
                            preferences.SettingSetNull();
                            preferences.SettingSetTMoneyNull();
                            progressBar.dismiss();
                            btLoginApp.setEnabled(true);
                        }
                    }else {
                        AlertDialog.showAlert(LoginActivity.this,"Peruri Umroh","Respon sedang gangguan, coba beberapa saat lagi");
                        preferences.SettingSetNull();
                        preferences.SettingSetTMoneyNull();
                        progressBar.dismiss();
                        btLoginApp.setEnabled(true);
                    }
                }else if(response.code() == 401){
                    AlertDialog.showAlert(LoginActivity.this,"Peruri Umroh","Respon sedang gangguan, coba beberapa saat lagi");
                    preferences.SettingSetNull();
                    preferences.SettingSetTMoneyNull();
                    progressBar.dismiss();
                    btLoginApp.setEnabled(true);
                }
                else {
                    AlertDialog.showAlert(LoginActivity.this,"Peruri Umroh","Respon sedang gangguan, coba beberapa saat lagi");
                    preferences.SettingSetNull();
                    preferences.SettingSetTMoneyNull();
                    progressBar.dismiss();
                    btLoginApp.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<SignInPPay> call, Throwable t){
                progressBar.dismiss();
                AlertConnectionDialog.showConnectionAlert(LoginActivity.this);
                preferences.SettingSetNull();
                preferences.SettingSetTMoneyNull();
                btLoginApp.setEnabled(true);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int curItem = item.getItemId();
        switch (curItem){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

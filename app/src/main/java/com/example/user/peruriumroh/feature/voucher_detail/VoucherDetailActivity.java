package com.example.user.peruriumroh.feature.voucher_detail;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.etc.FormatingDate;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.model.ListJamaah;
import com.example.user.peruriumroh.model.ResponseBank;
import com.example.user.peruriumroh.model.ResponseMutasi;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VoucherDetailActivity extends AppCompatActivity {

    ImageView ivQR;
    ImageView ivBy;
    TextView tvPembayaranValue, tvVoucherId, tvTanggalTransaksi, tvNamaJamaah, tvPaketUmroh, tvNamaAgen;
    Button btOK;
    String mutasiValue,mutasiSQRC,mutasiDate,mutasiVoucherId, jamaahId,bankId;

    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();

    Preferences preferences;

    List<ListJamaah.Data> eachJamaah = new ArrayList<ListJamaah.Data>();

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher_detail);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        preferences = new Preferences(VoucherDetailActivity.this);

//        VARIABLE VIEW

        ivQR = (ImageView)findViewById(R.id.iv_qr);

        ivBy = (ImageView)findViewById(R.id.iv_by);

        tvPembayaranValue = (TextView)findViewById(R.id.tv_pembayaran_value);
        tvVoucherId = (TextView)findViewById(R.id.tv_voucher_id);
        tvTanggalTransaksi = (TextView)findViewById(R.id.tv_tanggal_transaksi);
        tvNamaJamaah = (TextView)findViewById(R.id.tv_nama_jamaah);
        tvPaketUmroh = (TextView)findViewById(R.id.tv_paket_umroh);
        tvNamaAgen = (TextView)findViewById(R.id.tv_nama_agen);

        if (savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if(extras == null){
                mutasiVoucherId = null;
                mutasiDate = null;
                mutasiSQRC = null;
                mutasiValue = null;
                jamaahId = null;
                bankId = null;
            }else{
                jamaahId = extras.getString("JamaahId");
                mutasiVoucherId = extras.getString("mutasi_voucher_id");
                mutasiDate = extras.getString("mutasi_date");
                mutasiSQRC = extras.getString("mutasi_sqrc");
                mutasiValue = extras.getString("mutasi_value");
                bankId = extras.getString("mutasi_bank_id");
            }
        } else {
            jamaahId = (String) savedInstanceState.getSerializable("JamaahId");
            mutasiVoucherId = (String) savedInstanceState.getSerializable("mutasi_voucher_id");
            mutasiDate = (String) savedInstanceState.getSerializable("mutasi_date");
            mutasiSQRC = (String) savedInstanceState.getSerializable("mutasi_sqrc");
            mutasiValue = (String) savedInstanceState.getSerializable("mutasi_value");
            bankId = (String) savedInstanceState.getSerializable("mutasi_bank_id");
        }

//        Call<ResponseBank> getbank = apiClientInterface.getBank(
//                bankId,
//                preferences.getUserid(),
//                preferences.getToken(),
//                "mobile"
//        );
//        getbank.enqueue(new Callback<ResponseBank>() {
//            @Override
//            public void onResponse(Call<ResponseBank> call, Response<ResponseBank> response) {
//                if(response.code() == 200){
//                    if(response.body().getErrorcode().equals("00000")){
//                        Glide.with(VoucherDetailActivity.this).load(response.body().getData().get(0).getLogo_path()).into(ivBy);
//                    }
//                }else if(response.code()==401){
//                    Logout.logout(VoucherDetailActivity.this);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBank> call, Throwable t) {
//                if(t instanceof IOException){
//                    AlertConnectionDialog.showConnectionAlert(VoucherDetailActivity.this);
//                }else {
//
//                }
//            }
//        });

        Call<ListJamaah> call = apiClientInterface.agenGetJamaahByIdJamaah(
                preferences.getUserid(),
                preferences.getToken(),
                "",
                "mobile",
                "",
                "",
                "",
                jamaahId
        );

        call.enqueue(new Callback<ListJamaah>(){
            @Override
            public void onResponse(Call<ListJamaah> call, Response<ListJamaah> response) {
                Log.d("TAG", "onResponse: Berhasil Masuk response");
                if(response.code() == 200){
                    Log.d("TAG", "onResponse: Berhasil Masuk response 200");
                    if(response.body().getErrorcode().equals("00000")){
                        Log.d("TAG", "onResponse: Berhasil Masuk 00000");
                        eachJamaah = response.body().getData();

                        if(!TextUtils.isEmpty(mutasiSQRC)){
                            byte[] decodedString = Base64.decode(mutasiSQRC, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0,decodedString.length);
                            ivQR.setImageBitmap(decodedByte);
                        }

                        tvPembayaranValue.setText(CurrencyFormating.setToCurrency(mutasiValue));

                        tvVoucherId.setText(mutasiVoucherId);

                        String[] tanggal = mutasiDate.split(" ");

                        tvTanggalTransaksi.setText(FormatingDate.getDate(tanggal[0]) +" "+tanggal[1]);

                        Log.d("NAME", "onResponse: "+ eachJamaah.get(0).getJamaah_name());

                        tvNamaJamaah.setText(eachJamaah.get(0).getJamaah_name());

                        tvNamaAgen.setText(eachJamaah.get(0).getAgent_name());

                        tvPaketUmroh.setText(eachJamaah.get(0).getTravel_name() +" - "+ eachJamaah.get(0).getUmroh().get(0).getProduct_name() +" - "+ eachJamaah.get(0).getUmroh().get(0).getPackage_name());
                    }
                }else if(response.code()==401){
                    Logout.logout(VoucherDetailActivity.this);
                }
            }

            @Override
            public void onFailure(Call<ListJamaah> call, Throwable t) {
                if(t instanceof IOException){
                    AlertConnectionDialog.showConnectionAlert(VoucherDetailActivity.this);
                }else {

                }
            }
        });

        btOK = (Button)findViewById(R.id.bt_voucher_ok);
        btOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.example.user.peruriumroh.feature.saldo_ceksaldo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.peruriumroh.R;

public class SaldoCekSaldoAdapter extends RecyclerView.Adapter<SaldoCekSaldoAdapter.SaldoViewHolder> {
    int numberRv = 0;
    public SaldoCekSaldoAdapter(int numberRv){this.numberRv = numberRv;}

    @NonNull
    @Override
public SaldoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutItem = R.layout.item_ceksaldo;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachtoParentImmediately = false;

        View view = inflater.inflate(layoutItem,viewGroup,shouldAttachtoParentImmediately);
        SaldoCekSaldoAdapter.SaldoViewHolder viewHolder = new SaldoCekSaldoAdapter.SaldoViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SaldoViewHolder saldoViewHolder, int i) {
        saldoViewHolder.bind(i);
    }

    @Override
    public int getItemCount() {
        return numberRv;
    }

    class SaldoViewHolder extends RecyclerView.ViewHolder {

        public SaldoViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        void bind(int i){

        }
    }
}



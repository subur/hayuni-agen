package com.example.user.peruriumroh.feature.fragment_pembayaran;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.pay_chooser.PeruriPayChooserActivity;
import com.example.user.peruriumroh.feature.pay_scansqrcjamaah.PayScanSQRCJamaahActivity;
import com.example.user.peruriumroh.feature.saldo_scanSQRC.SaldoScanSQRCActivity;
import com.example.user.peruriumroh.feature.topup_input_amount.TopUpInputAmountActivity;
import com.example.user.peruriumroh.feature.transfer_branch.TransferBranchActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PembayaranFragment extends Fragment {

    RelativeLayout rlPembayaranPeruri,rlPembayaranCekSaldo,rlPembayaranTopUp, rlPembayaranTransfer;

    public PembayaranFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pembayaran, container, false);

        // Inflate the layout for this fragment
        getActivity().setTitle("Transaksi");
        rlPembayaranPeruri = (RelativeLayout)rootView.findViewById(R.id.pembayaran_peruri_pay);
        rlPembayaranPeruri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),PeruriPayChooserActivity.class);
                startActivity(intent);
            }
        });

        rlPembayaranCekSaldo = (RelativeLayout)rootView.findViewById(R.id.pembayaran_cek_saldo);
        rlPembayaranCekSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),SaldoScanSQRCActivity.class);
                startActivity(intent);
            }
        });

        rlPembayaranTopUp = (RelativeLayout)rootView.findViewById(R.id.pembayaran_top_up);
        rlPembayaranTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),TopUpInputAmountActivity.class);
                startActivity(intent);
            }
        });

        rlPembayaranTransfer = (RelativeLayout)rootView.findViewById(R.id.pembayaran_transfer);
        rlPembayaranTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),TransferBranchActivity.class);
                startActivity(intent);
            }
        });

        return rootView;
    }

}

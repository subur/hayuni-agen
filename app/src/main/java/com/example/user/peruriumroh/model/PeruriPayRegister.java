package com.example.user.peruriumroh.model;

public class PeruriPayRegister {
    private Data data;

    private String errorcode;

    private String errormsg;

    public Data getData() {
        return data;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public class Data {
        private String timeStamp;

        private String messageType;

        private String resultCode;

        private String resultDesc;

        public String getTimeStamp() {
            return timeStamp;
        }

        public String getMessageType() {
            return messageType;
        }

        public String getResultCode() {
            return resultCode;
        }

        public String getResultDesc() {
            return resultDesc;
        }
    }
}

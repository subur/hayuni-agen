package com.example.user.peruriumroh.feature.landing;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.home.HomeActivity;
import com.example.user.peruriumroh.feature.login.LoginActivity;
import com.example.user.peruriumroh.preference.Preferences;

import io.fabric.sdk.android.Fabric;
import java.util.List;

public class WelcomeActivity extends AppCompatActivity {
    Button btLogin;
    Preferences preferences;
    public static Activity fa;

    TextView tvVersion;

    ImageView ivLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_welcome);
        fa = this;

        preferences = new Preferences(WelcomeActivity.this);
        checkToken();

        ivLogo = (ImageView)findViewById(R.id.iv_logo);
        Glide.with(this).load(R.drawable.hayuni_logo_square).into(ivLogo);

        tvVersion = (TextView)findViewById(R.id.tv_version);
        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            tvVersion.setText(version);
        } catch (PackageManager.NameNotFoundException e){
            e.printStackTrace();
        }

        btLogin = (Button)findViewById(R.id.bt_login);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(WelcomeActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void checkToken() {
        if(preferences.getToken() != null && !preferences.getToken().equals("")){
            finish();
            Intent intent = new Intent(WelcomeActivity.this,HomeActivity.class);
            startActivity(intent);
        }
    }
}

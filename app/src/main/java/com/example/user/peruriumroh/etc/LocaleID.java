package com.example.user.peruriumroh.etc;

import java.util.Locale;

public class LocaleID {
    public static Locale getLocaleId() {
        return new Locale("in", "ID");
    }
}

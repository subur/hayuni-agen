package com.example.user.peruriumroh.feature.pay_manualjamaah;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.icu.util.CurrencyAmount;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.AlertDialogFinish;
import com.example.user.peruriumroh.etc.BottomSheetConnectionDialog;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.home.HomeActivity;
import com.example.user.peruriumroh.feature.pay_peruripin.PayPeruriPINActivity;
import com.example.user.peruriumroh.model.InquiryObject;
import com.example.user.peruriumroh.model.ListJamaah;
import com.example.user.peruriumroh.model.ResponseInquiry;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import co.ceryle.radiorealbutton.RadioRealButton;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayManualJamaahActivity extends AppCompatActivity {

    Preferences preference;

    Button btMasukanPin;
    public static TextView tvSaldoJamaah;
    TextView tvNominalPembayaran;
    EditText etNominal;
    Spinner spinner;
    RadioRealButtonGroup btGroup;
    RadioRealButton btbookingfee,btdp,btpelunasan;

    String debt = "0";
    String deposit= "0";

    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();

    public List<ListJamaah.Data> allJamaah = new ArrayList<ListJamaah.Data>();
    private String current = "";

    ProgressDialog progressDialog;

    ArrayList<InquiryObject> name = new ArrayList<InquiryObject>();

    public static Activity ac;

    private String posisi;

    int moneybook = 0;
    int moneydp = 0;

    String nominal;
    String jamaahId;
    String umrohId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_manual_jamaah);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        preference = new Preferences(PayManualJamaahActivity.this);

        allJamaah = HomeActivity.allJamaah;

        if(allJamaah == null){
            AlertDialogFinish.showAlertFinish(PayManualJamaahActivity.this,"Umrah Safe","Belum ada jamaah..");
        }

        //VARIABLE VIEW

        progressDialog = new ProgressDialog(PayManualJamaahActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);

        btdp = (RadioRealButton) findViewById(R.id.rrb_booking_dp);
        btpelunasan = (RadioRealButton) findViewById(R.id.rrb_booking_pelunasan);

        btGroup = (RadioRealButtonGroup)findViewById(R.id.bt_group);
        btGroup.setOnPositionChangedListener(new RadioRealButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(RadioRealButton button, int currentPosition, int lastPosition) {

            }
        });

        tvNominalPembayaran = (TextView)findViewById(R.id.tv_nominal_pembayaran);

        tvSaldoJamaah = (TextView)findViewById(R.id.tv_saldo_jamaah);
        tvSaldoJamaah.setText(CurrencyFormating.setToCurrency(preference.getBalance()));

        spinner = (Spinner) findViewById(R.id.spinner_jamaah);
        if(allJamaah != null){
            for (int i = 0;i<allJamaah.size();i++){
                InquiryObject IO = new InquiryObject(allJamaah.get(i).getUmroh().get(0).getUmroh_id(),allJamaah.get(i).getJamaah_id(), allJamaah.get(i).getJamaah_name());
                name.add(IO);
            }

            ArrayAdapter<InquiryObject> arrayAdapter = new ArrayAdapter<InquiryObject>(this,R.layout.spinner,name);
            arrayAdapter.setDropDownViewResource(R.layout.spinner);
            spinner.setAdapter(arrayAdapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if(allJamaah.get(spinner.getSelectedItemPosition()).getUmroh().get(0).getDebt_book() !=null){
                        moneybook = Integer.parseInt(allJamaah.get(spinner.getSelectedItemPosition()).getUmroh().get(0).getDebt_book());
                    }

                    if(allJamaah.get(spinner.getSelectedItemPosition()).getUmroh().get(0).getDebt_dp() !=null){
                        moneydp = Integer.parseInt(allJamaah.get(spinner.getSelectedItemPosition()).getUmroh().get(0).getDebt_dp());
                    }

                    if(moneybook != 0 && moneydp != 0){

                        //Set Enable
//                        btbookingfee.setEnabled(true);
                        btdp.setEnabled(false);
                        btpelunasan.setEnabled(false);

                        //Set Checkable
//                        btbookingfee.setChecked(true);
                        btdp.setChecked(false);
                        btpelunasan.setChecked(false);

                        posisi = "0";
                        tvNominalPembayaran.setText("Nominal Pembayaran : " + CurrencyFormating.setToCurrency(String.valueOf(moneybook)));
                    }else if(moneybook == 0 && moneydp != 0){
                        //Set Enable
//                        btbookingfee.setEnabled(false);
                        btdp.setEnabled(true);
                        btpelunasan.setEnabled(false);

                        //Set Checkable
//                        btbookingfee.setChecked(false);
                        btdp.setChecked(true);
                        btpelunasan.setChecked(false);

                        posisi = "1";
                        tvNominalPembayaran.setText("Nominal Pembayaran : " + CurrencyFormating.setToCurrency(String.valueOf(moneydp)));
                    }else if(moneybook == 0 && moneydp == 0){
                        //Set Enable
//                        btbookingfee.setEnabled(false);
                        btdp.setEnabled(false);
                        btpelunasan.setEnabled(true);

                        //Set Enable
//                        btbookingfee.setChecked(false);
                        btdp.setChecked(false);
                        btpelunasan.setChecked(true);

                        posisi = "2";
                        tvNominalPembayaran.setText("Nominal Pembayaran");
                    }

                    if(allJamaah.get(spinner.getSelectedItemPosition()).getUmroh().get(0).getUmroh_debt() == null){
                        debt = "0";
                    }else{
                        debt = allJamaah.get(spinner.getSelectedItemPosition()).getUmroh().get(0).getUmroh_debt();
                    }

                    if(allJamaah.get(spinner.getSelectedItemPosition()).getUmroh().get(0).getUmroh_deposit() == null){
                        deposit = "0";
                    }
                    else {
                        deposit = allJamaah.get(spinner.getSelectedItemPosition()).getUmroh().get(0).getUmroh_deposit();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        btMasukanPin = (Button)findViewById(R.id.bt_masukan_pin);
        btMasukanPin.setEnabled(false);

        etNominal = (EditText)findViewById(R.id.et_nominal);
        etNominal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(current)){
                    String formatted = "0";
                    etNominal.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");

                    if (cleanString.equals("")){
                        formatted = CurrencyFormating.setToCurrency("0");
                        btMasukanPin.setEnabled(false);
                    }else {
                        formatted = CurrencyFormating.setToCurrency(cleanString);
                        if(posisi.equals("0")){
                            if(cleanString.equals(String.valueOf(moneybook))){
                                btMasukanPin.setEnabled(true);
                            }else {
                                btMasukanPin.setEnabled(false);
                            }
                        }else if(posisi.equals("1")){
                            if(cleanString.equals(String.valueOf(moneydp))){
                                btMasukanPin.setEnabled(true);
                            }else {
                                btMasukanPin.setEnabled(false);
                            }
                        }else if(posisi.equals("2")){
                            if(cleanString.length() >= 3 && Double.valueOf(cleanString) <= (Double.valueOf(debt) - Double.valueOf(deposit))){
                                btMasukanPin.setEnabled(true);
                            }else {
                                btMasukanPin.setEnabled(false);
                            }
                        }
                    }

                    current = formatted;
                    etNominal.setText(formatted);
                    etNominal.setSelection(formatted.length());

                    etNominal.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btMasukanPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog.setMessage("Memuat Transaksi..");
                progressDialog.show();

                nominal = etNominal.getText().toString().replaceAll("[Rp,.]", "");
                jamaahId = name.get(spinner.getSelectedItemPosition()).getJamaahId();
                umrohId = name.get(spinner.getSelectedItemPosition()).getUmrohId();

                Call<ResponseInquiry> inquiry = apiClientInterface.agenPostInquiry(
                        preference.getUserid(),
                        preference.getToken(),
                        "mobile",
                        umrohId,
                        nominal,
                        preference.getAgent_id(),
                        "Agent",
                        preference.getIdTmoney(),
                        preference.getIdFusion(),
                        preference.getTokenppay()
                );

                inquiry.enqueue(new Callback<ResponseInquiry>() {
                    @Override
                    public void onResponse(Call<ResponseInquiry> call, Response<ResponseInquiry> response) {
                        if (response.code() == 200){
                            if(response.body().getErrorcode().equals("00000")){
                                progressDialog.dismiss();
                                if(response.body().getData().getResultCode().equals("0")){
                                    Intent intent = new Intent(PayManualJamaahActivity.this,PayPeruriPINActivity.class);
                                    intent.putExtra("Nominal",nominal);
                                    intent.putExtra("JamaahId",jamaahId);
                                    intent.putExtra("UmrohId",umrohId);
                                    intent.putExtra("JenisPembayaran",posisi);
                                    intent.putExtra("from","manual");
                                    intent.putExtra("transactionId",response.body().getData().getTransactionID());
                                    intent.putExtra("refNo",response.body().getData().getRefNo());
                                    startActivity(intent);
                                }
                            }else if(response.body().getErrorcode().equals("00165")){
                                progressDialog.dismiss();
                                if(response.body().getData().getResultCode().equals("GL-004")){
                                    com.example.user.peruriumroh.etc.AlertDialog.showAlert(PayManualJamaahActivity.this,"Umrah Safe", "Saldo T-MONEY anda tidak mencukupi untuk transaksi ini.");
                                }else {
                                    Logout.logout(PayManualJamaahActivity.this);
                                }
                            }
                        }else if(response.code() == 401){
                            Logout.logout(PayManualJamaahActivity.this);
                        } else{
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(),"CHECKOUT GAGAL",Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseInquiry> call, Throwable t) {
                        progressDialog.dismiss();
                        if(t instanceof IOException){
                            AlertConnectionDialog.showConnectionAlert(PayManualJamaahActivity.this);
                        }
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvSaldoJamaah.setText(CurrencyFormating.setToCurrency(preference.getBalance()));
    }
}

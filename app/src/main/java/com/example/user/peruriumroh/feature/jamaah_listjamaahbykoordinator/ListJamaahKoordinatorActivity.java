package com.example.user.peruriumroh.feature.jamaah_listjamaahbykoordinator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.jamaah_detailkoordinator.DetailKoordinatorAdapter;
import com.example.user.peruriumroh.feature.jamaah_listjamaah.ListJamaahAdapter;

public class ListJamaahKoordinatorActivity extends AppCompatActivity{
    RecyclerView rvListJamaahKoordinator;
    ListJamaahKoordinatorAdapter listJamaahKoordinatorAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_jamaah_koordinator);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvListJamaahKoordinator = (RecyclerView)findViewById(R.id.rv_list_jamaah_koordinator);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvListJamaahKoordinator.setLayoutManager(layoutManager);

        rvListJamaahKoordinator.setHasFixedSize(true);

        listJamaahKoordinatorAdapter = new ListJamaahKoordinatorAdapter(100);
        rvListJamaahKoordinator.setAdapter(listJamaahKoordinatorAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close_white,menu);
        return true;
    }
}

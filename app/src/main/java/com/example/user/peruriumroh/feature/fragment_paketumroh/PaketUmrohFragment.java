package com.example.user.peruriumroh.feature.fragment_paketumroh;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.BottomSheetConnectionDialog;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.feature.home.HomeActivity;
import com.example.user.peruriumroh.feature.login.LoginActivity;
import com.example.user.peruriumroh.model.PaketUmroh;
import com.example.user.peruriumroh.model.Produk;
import com.example.user.peruriumroh.model.userData;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaketUmrohFragment extends Fragment{
    PaketUmrohFragmentAdapter adapterPaketUmroh;
    RecyclerView rvPaketUmroh;
    public static List<Produk.Data> allProduk;

    Preferences preferences;

    private Toast mToast;

    EditText etCariProduk;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_paket_umroh, container, false);
        getActivity().setTitle("Paket Umroh");
        rvPaketUmroh = (RecyclerView) rootView.findViewById(R.id.rv_paket_umroh);

        preferences = new Preferences(this.getActivity());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvPaketUmroh.setLayoutManager(layoutManager);

        rvPaketUmroh.setHasFixedSize(true);
        getProduk();

        etCariProduk = (EditText)rootView.findViewById(R.id.et_cari_produk);
        etCariProduk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String input = s.toString().toLowerCase();

                List<Produk.Data> searchProduk = new ArrayList<Produk.Data>();

                if(input.length() > 0){
                    Log.d(TAG, "onTextChanged: Masuk > 0");
                    for(int i = 0;i<allProduk.size();i++){
                        if(allProduk.get(i).getProduct_name().toLowerCase().contains(input)){
                            searchProduk.add(allProduk.get(i));
                        }
                    }
                    adapterPaketUmroh.updateJamaahList(searchProduk);
                }else {
                    adapterPaketUmroh.updateJamaahList(allProduk);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return rootView;
    }

    private void getProduk(){
        ApiClient apiClient = new ApiClient();
        ApiClientInterface apiClientInterface = apiClient.clientInterface();

        String idUser = preferences.getUserid();
        String token = preferences.getToken();
        String platform = "mobile";

        Call<Produk> call = apiClientInterface.agenGetProduk(idUser,token,"",platform,"","","",preferences.getTravelId());
        call.enqueue(new Callback<Produk>() {
            @Override
            public void onResponse(Call<Produk> call, Response<Produk> response) {
                if(response.code() == 200){
                    allProduk = response.body().getData();
                    Log.d("TAG : produk awal", String.valueOf(allProduk.size()));
                    List<String> nullPackage = new ArrayList<>();
                    for (int i = 0; i < allProduk.size();i++){
                        if(allProduk.get(i).getPaket() == null){
                            nullPackage.add(String.valueOf(i));
                            Log.d("TAG: " + i +"#"+ allProduk.get(i).getProduct_id(), "KOSONG");
                        }
                    }
                    Log.d("TAG : Size", String.valueOf(nullPackage.size()));
                    if(nullPackage.size() > 0){
                        for(int i = nullPackage.size()-1; i >= 0; i--){
                            allProduk.remove(Integer.parseInt(nullPackage.get(i)));
                        }
                    }
                    Log.d("TAG : produk akhir", String.valueOf(allProduk.size()));
                    adapterPaketUmroh = new PaketUmrohFragmentAdapter(allProduk,getActivity());
                    rvPaketUmroh.setAdapter(adapterPaketUmroh);
                }else if(response.code() == 401){
                    Logout.logout(getActivity());
                }
            }

            @Override
            public void onFailure(Call<Produk> call, Throwable t) {
                BottomSheetConnectionDialog bottomSheet = new BottomSheetConnectionDialog();
                bottomSheet.show(getFragmentManager(),"bottomConnection");
            }
        });
    }
}

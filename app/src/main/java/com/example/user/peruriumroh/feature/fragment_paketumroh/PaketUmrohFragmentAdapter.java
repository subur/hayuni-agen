package com.example.user.peruriumroh.feature.fragment_paketumroh;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.CheckNull;
import com.example.user.peruriumroh.etc.CurrencyFormating;
import com.example.user.peruriumroh.etc.FormatingDate;
import com.example.user.peruriumroh.feature.detailpaketumroh.DetailPaketUmrohActivity;
import com.example.user.peruriumroh.model.PaketUmroh;
import com.example.user.peruriumroh.model.Produk;
import com.example.user.peruriumroh.model.userData;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;

public class PaketUmrohFragmentAdapter extends RecyclerView.Adapter<PaketUmrohFragmentAdapter.PaketFragmentViewHolder>{
    List<Produk.Data> allProduk = new ArrayList<>();
    private int mNumberItems;
    Activity mact;

    public PaketUmrohFragmentAdapter(List<Produk.Data> allProduk, Activity act)
    {
        this.allProduk = allProduk;
        mact = act;
    }

    LinearLayout ll;

    @NonNull
    @Override
    public PaketFragmentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutItem = R.layout.item_paketumroh;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachtoParentImmediately = false;

        View view = inflater.inflate(layoutItem,viewGroup,shouldAttachtoParentImmediately);
        PaketUmrohFragmentAdapter.PaketFragmentViewHolder viewHolder = new PaketUmrohFragmentAdapter.PaketFragmentViewHolder(view);

        ll = (LinearLayout)view.findViewById(R.id.ll);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PaketFragmentViewHolder paketFragmentViewHolder,final int i) {
        paketFragmentViewHolder.bind(i);

        Glide.with(mact).load("http://13.228.32.47/umroh/api/" + allProduk.get(i).getProduct_image()).apply(RequestOptions.bitmapTransform(new RoundedCorners(10))).into(paketFragmentViewHolder.ivPaket);
        paketFragmentViewHolder.tvTitlePaketUmroh.setText(allProduk.get(i).getProduct_name());

        //GET DATE AND SET TO TEXTVIEW
        String[] splitDate = FormatingDate.getDate(allProduk.get(i).getProduct_departure()).split(" ");
        paketFragmentViewHolder.tvTanggal.setText(splitDate[0]);
        paketFragmentViewHolder.tvBulanTahun.setText(splitDate[1] + " '" +splitDate[2].substring(2));
        paketFragmentViewHolder.tvKeterangan.setText(allProduk.get(i).getProduct_desc());

        //CURRENCY
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        try{
            paketFragmentViewHolder.tvHargaPaket.setText(CurrencyFormating.setToCurrency(allProduk.get(i).getPaket().get(allProduk.get(i).getPaket().size() - 1).getPackage_price()));
        }catch (Exception e){
            Log.e(TAG, "onBindViewHolderError: " + i);
        }

        String quotaBook = "0";
        if(allProduk.get(i).getQuota().get(0).getQuota_book() == null){
            quotaBook = "0";
        }else {
            quotaBook = allProduk.get(i).getQuota().get(0).getQuota_book();
        }
        paketFragmentViewHolder.tvKuotaPaket.setText("Kuota tersisa : " + String.valueOf(Integer.parseInt(allProduk.get(i).getQuota().get(0).getQuota_available()))  + " Slot dari : " + String.valueOf(Integer.parseInt(allProduk.get(i).getQuota().get(0).getQuota_available()) + Integer.parseInt(quotaBook)) + " Slot");
        ll.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ArrayList<Produk.Data> eachPaket = new ArrayList<Produk.Data>();
                eachPaket.add(allProduk.get(i));

                Intent intent = new Intent(mact,DetailPaketUmrohActivity.class);
                intent.putExtra("kunci",eachPaket);
                mact.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return allProduk.size();
    }

    public void updateJamaahList(List<Produk.Data> searchProduk) {
        this.allProduk = new ArrayList<>();
        this.allProduk = searchProduk;
        notifyDataSetChanged();
    }

    public class PaketFragmentViewHolder extends RecyclerView.ViewHolder{

        ImageView ivPaket;
        TextView tvTitlePaketUmroh, tvTanggal, tvBulanTahun, tvKeterangan, tvHargaPaket, tvKuotaPaket;

        public PaketFragmentViewHolder(View itemView) {
            super(itemView);
            ivPaket = (ImageView)itemView.findViewById(R.id.iv_paket);
            tvTitlePaketUmroh = (TextView)itemView.findViewById(R.id.tv_title_paket_umroh);
            tvTanggal = (TextView)itemView.findViewById(R.id.tv_tanggal);
            tvBulanTahun = (TextView)itemView.findViewById(R.id.tv_bulan_tahun);
            tvKeterangan = (TextView)itemView.findViewById(R.id.tv_keterangan_paket);
            tvHargaPaket = (TextView)itemView.findViewById(R.id.tv_harga_paket);
            tvKuotaPaket = (TextView)itemView.findViewById(R.id.tv_kuota_paket);
        }

        void bind(int listIndex){

        }
    }
}

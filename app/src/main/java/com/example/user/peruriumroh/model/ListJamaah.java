package com.example.user.peruriumroh.model;

import java.io.Serializable;
import java.util.List;

public class ListJamaah implements Serializable {
    private List<Data> data = null;

    private String errorcode = "";

    private String errormsg = "";

    public List<Data> getData() {
        return data;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public class Data implements Serializable {
        private String travel_id = "";

        private String agent_fax = "";

        private String agent_id = "";

        private String agent_email = "";

        private String agent_name = "";

        private String jamaah_ktp = "";

        private String jamaah_graduate = "";

        private List<Peruri_pay> peruri_pay = null;

        private String jamaah_koordinator = "";

        private List<Document> document = null;

        private String travel_contact = "";

        private String jamaah_blood_type = "";

        private String travel_nia = "";

        private String travel_name = "";

        private String travel_fax = "";

        private List<Contact> contact = null;

        private List<Umroh> umroh = null;

        private String agent_adress = "";

        private List<Paspor> paspor = null;

        private String travel_address = "";

        private String jamaah_name = "";

        private String travel_telp = "";

        private String jamaah_pob = "";

        private String agent_telp = "";

        private String jamaah_job = "";

        private String travel_image = "";

        private String jamaah_birth_date = "";

        private String jamaah_id = "";

        private String jamaah_status = "";

        private String travel_trademark = "";

        private String travel_license = "";

        private String agent_nik = "";

        private String jamaah_disease = "";

        private List<Sqrc> sqrc = null;

        private String travel_email = "";

        private String agent_image = "";

        private String jamaah_marital_status = "";

        private String city_id = "";

        public String getTravel_id() {
            return travel_id;
        }

        public String getAgent_fax() {
            return agent_fax;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public String getAgent_email() {
            return agent_email;
        }

        public String getAgent_name() {
            return agent_name;
        }

        public String getJamaah_ktp() {
            return jamaah_ktp;
        }

        public String getJamaah_graduate() {
            return jamaah_graduate;
        }

        public List<Peruri_pay> getPeruri_pay() {
            return peruri_pay;
        }

        public String getJamaah_koordinator() {
            return jamaah_koordinator;
        }

        public String getTravel_contact() {
            return travel_contact;
        }

        public String getJamaah_blood_type() {
            return jamaah_blood_type;
        }

        public String getTravel_nia() {
            return travel_nia;
        }

        public String getTravel_name() {
            return travel_name;
        }

        public String getTravel_fax() {
            return travel_fax;
        }

        public List<Contact> getContact() {
            return contact;
        }

        public List<Umroh> getUmroh() {
            return umroh;
        }

        public String getAgent_adress() {
            return agent_adress;
        }

        public List<Paspor> getPaspor() {
            return paspor;
        }

        public String getTravel_address() {
            return travel_address;
        }

        public String getJamaah_name() {
            return jamaah_name;
        }

        public String getTravel_telp() {
            return travel_telp;
        }

        public String getJamaah_pob() {
            return jamaah_pob;
        }

        public String getAgent_telp() {
            return agent_telp;
        }

        public String getJamaah_job() {
            return jamaah_job;
        }

        public String getTravel_image() {
            return travel_image;
        }

        public String getJamaah_birth_date() {
            return jamaah_birth_date;
        }

        public String getJamaah_id() {
            return jamaah_id;
        }

        public String getJamaah_status() {
            return jamaah_status;
        }

        public String getTravel_trademark() {
            return travel_trademark;
        }

        public String getTravel_license() {
            return travel_license;
        }

        public String getAgent_nik() {
            return agent_nik;
        }

        public String getJamaah_disease() {
            return jamaah_disease;
        }

        public List<Sqrc> getSqrc() {
            return sqrc;
        }

        public String getTravel_email() {
            return travel_email;
        }

        public String getAgent_image() {
            return agent_image;
        }

        public String getJamaah_marital_status() {
            return jamaah_marital_status;
        }

        public String getCity_id() {
            return city_id;
        }
    }

    public class Umroh implements Serializable{
        public String umroh_id = "";

        public String travel_id = "";

        public String agent_id = "";

        public String product_image = "";

        public String product_quota = "";

        public String product_status = "";

        public String umroh_deposit = "";

        public String package_id = "";

        public String product_code = "";

        public String product_name = "";

        public String package_price = "";

        public String umroh_debt = "";

        private String debt_book = "";

        private String debt_dp = "";

        public String product_desc = "";

        public String mentor_id = "";

        public String jamaah_id = "";

        public String product_departure = "";

        public String umroh_status = "";

        public String product_id = "";

        public String product_rute_desc = "";

        public String package_name = "";

        public String product_payment_deadline = "";

        public String getDebt_book() {
            return debt_book;
        }

        public String getDebt_dp() {
            return debt_dp;
        }

        public String getUmroh_id() {
            return umroh_id;
        }

        public String getTravel_id() {
            return travel_id;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public String getProduct_image() {
            return product_image;
        }

        public String getProduct_quota() {
            return product_quota;
        }

        public String getProduct_status() {
            return product_status;
        }

        public String getUmroh_deposit() {
            return umroh_deposit;
        }

        public String getPackage_id() {
            return package_id;
        }

        public String getProduct_code() {
            return product_code;
        }

        public String getProduct_name() {
            return product_name;
        }

        public String getPackage_price() {
            return package_price;
        }

        public String getUmroh_debt() {
            return umroh_debt;
        }

        public String getProduct_desc() {
            return product_desc;
        }

        public String getMentor_id() {
            return mentor_id;
        }

        public String getJamaah_id() {
            return jamaah_id;
        }

        public String getProduct_departure() {
            return product_departure;
        }

        public String getUmroh_status() {
            return umroh_status;
        }

        public String getProduct_id() {
            return product_id;
        }

        public String getProduct_rute_desc() {
            return product_rute_desc;
        }

        public String getPackage_name() {
            return package_name;
        }

        public String getProduct_payment_deadline() {
            return product_payment_deadline;
        }
    }

    public class Sqrc implements Serializable {
        private String sqrc_kode= "";

        private String jamaah_id= "";

        private String sqrc_id= "";

        public String getSqrc_kode() {
            return sqrc_kode;
        }

        public String getJamaah_id() {
            return jamaah_id;
        }

        public String getSqrc_id() {
            return sqrc_id;
        }
    }

    public class Peruri_pay implements Serializable {
        private String jamaah_id= "";

        private String peruri_pay_id= "";

        private String name= "";

        private String username= "";

        public String getJamaah_id() {
            return jamaah_id;
        }

        public String getPeruri_pay_id() {
            return peruri_pay_id;
        }

        public String getName() {
            return name;
        }

        public String getUsername() {
            return username;
        }
    }

    public class Paspor implements Serializable {
        private String paspor_no= "";

        private String paspor_dept_count= "";

        private String paspor_last_dept= "";

        private String paspor_valid_date= "";

        private String jamaah_id= "";

        private String paspor_date= "";

        private String paspor_id= "";

        private String city_id= "";

        public String getPaspor_no() {
            return paspor_no;
        }

        public String getPaspor_dept_count() {
            return paspor_dept_count;
        }

        public String getPaspor_last_dept() {
            return paspor_last_dept;
        }

        public String getPaspor_valid_date() {
            return paspor_valid_date;
        }

        public String getJamaah_id() {
            return jamaah_id;
        }

        public String getPaspor_date() {
            return paspor_date;
        }

        public String getPaspor_id() {
            return paspor_id;
        }

        public String getCity_id() {
            return city_id;
        }
    }

    public class Contact implements Serializable {
        private String contact_emergency_name= "";

        private String contact_emergency_relation= "";

        private String jamaah_id= "";

        private String province_id= "";

        private String contact_id= "";

        private String contact_emergency_telp= "";

        private String contact_address= "";

        private String contact_handphone= "";

        private String city_id= "";

        private String contact_email= "";

        private String contact_telp= "";

        public String getContact_emergency_name() {
            return contact_emergency_name;
        }

        public String getContact_emergency_relation() {
            return contact_emergency_relation;
        }

        public String getJamaah_id() {
            return jamaah_id;
        }

        public String getProvince_id() {
            return province_id;
        }

        public String getContact_id() {
            return contact_id;
        }

        public String getContact_emergency_telp() {
            return contact_emergency_telp;
        }

        public String getContact_address() {
            return contact_address;
        }

        public String getContact_handphone() {
            return contact_handphone;
        }

        public String getCity_id() {
            return city_id;
        }

        public String getContact_email() {
            return contact_email;
        }

        public String getContact_telp() {
            return contact_telp;
        }
    }

    public class Document implements Serializable{
        private String document_name= "";

        private String jamaah_id= "";

        private String document_path= "";

        private String document_id= "";

        public String getDocument_name() {
            return document_name;
        }

        public String getJamaah_id() {
            return jamaah_id;
        }

        public String getDocument_path() {
            return document_path;
        }

        public String getDocument_id() {
            return document_id;
        }
    }
}
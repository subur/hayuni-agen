package com.example.user.peruriumroh.feature.jamaah_listjamaah;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ToolbarWidgetWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.saldo_ceksaldo.SaldoCekSaldoAdapter;

public class ListJamaahActivity extends AppCompatActivity {

    private DrawerLayout dl;
    private ActionBarDrawerToggle abdt;
    RecyclerView rv_list_jamaah;
    ListJamaahAdapter listJamaahAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_jamaah);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        rv_list_jamaah = (RecyclerView)findViewById(R.id.rv_list_jamaah);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv_list_jamaah.setLayoutManager(layoutManager);

        rv_list_jamaah.setHasFixedSize(true);

        listJamaahAdapter = new ListJamaahAdapter(100);
        rv_list_jamaah.setAdapter(listJamaahAdapter);

        dl = (DrawerLayout)findViewById(R.id.drawer_list_jamaah);
        abdt = new ActionBarDrawerToggle(this,dl,R.string.Open,R.string.Close);
        abdt.setDrawerIndicatorEnabled(true);

        dl.addDrawerListener(abdt);
        abdt.syncState();

        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);
        nav_view.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        menuItem.setChecked(true);
                        dl.closeDrawers();
                        int id = menuItem.getItemId();
                        switch (id){
                            case R.id.item_paketumroh:
                                Toast.makeText(getApplicationContext(),"Paket Umroh",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.item_Pembayaran:
                                Toast.makeText(getApplicationContext(),"Pembayaran",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.item_daftarjamaah:
                                Toast.makeText(getApplicationContext(),"Daftar Jamaah",Toast.LENGTH_SHORT).show();
                                break;
                        }

                        return false;
                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }
}

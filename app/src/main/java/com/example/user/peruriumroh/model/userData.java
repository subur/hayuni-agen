package com.example.user.peruriumroh.model;

import java.util.List;

public class userData {
    private Data data;

    private String errorcode;

    private String errormsg;

    public Data getData() {
        return data;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public class Data {
        private String access_token;

        private Agent agent;

        private List<Peruri_pay> peruri_pay;

        private User user;

        public String getAccess_token() {
            return access_token;
        }

        public Agent getAgent() {
            return agent;
        }

        public List<Peruri_pay> getPeruri_pay() {
            return peruri_pay;
        }

        public User getUser() {
            return user;
        }
    }

    public class Peruri_pay {
        private String status_registrasi;

        private String status_integrasi;

        private String password;

        private String role;

        private String peruri_pay_id;

        private String fullname;

        private String id;

        private String phoneno;

        private String username;

        public String getStatus_registrasi() {
            return status_registrasi;
        }

        public String getStatus_integrasi() {
            return status_integrasi;
        }

        public String getPassword() {
            return password;
        }

        public String getRole() {
            return role;
        }

        public String getPeruri_pay_id() {
            return peruri_pay_id;
        }

        public String getFullname() {
            return fullname;
        }

        public String getId() {
            return id;
        }

        public String getPhoneno() {
            return phoneno;
        }

        public String getUsername() {
            return username;
        }
    }

    public class User {
        private String related_id;

        private String password;

        private String role;

        private String user_id;

        private String role_id;

        private String user_image;

        private String related_key;

        private String name;

        private String ts_created;

        private String msisdn;

        private String email;

        public String getRelated_id() {
            return related_id;
        }

        public String getPassword() {
            return password;
        }

        public String getRole() {
            return role;
        }

        public String getUser_id() {
            return user_id;
        }

        public String getRole_id() {
            return role_id;
        }

        public String getUser_image() {
            return user_image;
        }

        public String getRelated_key() {
            return related_key;
        }

        public String getName() {
            return name;
        }

        public String getTs_created() {
            return ts_created;
        }

        public String getMsisdn() {
            return msisdn;
        }

        public String getEmail() {
            return email;
        }
    }

    public class Agent {
        private String travel_id;

        private String agent_fax;

        private String agent_id;

        private String agent_email;

        private String agent_name;

        private String agent_nik;

        private String ts_registered;

        private String agent_adress;

        private String sqrc;

        private String agent_telp;

        private String agent_image;

        public String getTravel_id() {
            return travel_id;
        }

        public String getAgent_fax() {
            return agent_fax;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public String getAgent_email() {
            return agent_email;
        }

        public String getAgent_name() {
            return agent_name;
        }

        public String getAgent_nik() {
            return agent_nik;
        }

        public String getTs_registered() {
            return ts_registered;
        }

        public String getAgent_adress() {
            return agent_adress;
        }

        public String getSqrc() {
            return sqrc;
        }

        public String getAgent_telp() {
            return agent_telp;
        }

        public String getAgent_image() {
            return agent_image;
        }
    }
}

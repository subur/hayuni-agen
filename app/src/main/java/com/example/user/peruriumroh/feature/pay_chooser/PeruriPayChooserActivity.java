package com.example.user.peruriumroh.feature.pay_chooser;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.feature.pay_manualjamaah.PayManualJamaahActivity;
import com.example.user.peruriumroh.feature.pay_scansqrcjamaah.PayScanSQRCJamaahActivity;

public class PeruriPayChooserActivity extends AppCompatActivity {

    Button btManual, btScan;

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peruri_pay_chooser);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        btManual = (Button)findViewById(R.id.bt_pay_manual);
        btManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PeruriPayChooserActivity.this,PayManualJamaahActivity.class);
                startActivity(intent);
            }
        });

        btScan = (Button)findViewById(R.id.bt_pay_scan);
        btScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PeruriPayChooserActivity.this,PayScanSQRCJamaahActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

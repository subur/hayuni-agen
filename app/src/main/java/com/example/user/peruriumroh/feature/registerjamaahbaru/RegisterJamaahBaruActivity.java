package com.example.user.peruriumroh.feature.registerjamaahbaru;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.etc.ValidEmail;
import com.example.user.peruriumroh.feature.fragment_paketumroh.PaketUmrohFragment;
import com.example.user.peruriumroh.feature.fragment_paketumroh.PaketUmrohFragmentAdapter;
import com.example.user.peruriumroh.model.KoordinatorId;
import com.example.user.peruriumroh.model.PeruriPayRegister;
import com.example.user.peruriumroh.model.Produk;
import com.example.user.peruriumroh.model.RegisterResponse;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterJamaahBaruActivity extends AppCompatActivity {

    Preferences preferences;
    TextView tvProdukPaket;
    EditText regNamaLengkap,regEmail,regNomorKTP,regNomorHandphone,regAlamat,regEmailKoordinator, regNamaAyahKandung;
    CheckBox cbKoordinator,cbSubordinat;
    Button btRegister;
    boolean formData1 = false;
    boolean formData2 = false;
    String idKoordinator = "";
    private ProgressDialog progressBar;

    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();

    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_jamaah_baru);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        //PROGRESS BAR
        progressBar = new ProgressDialog(RegisterJamaahBaruActivity.this);
        progressBar.setCancelable(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        //GET PRODUK
        List<Produk.Data> searchList = PaketUmrohFragment.allProduk;

        //GET PREFERENCE
        preferences = new Preferences(RegisterJamaahBaruActivity.this);

        //VARIABEL VIEW
        tvProdukPaket = (TextView)findViewById(R.id.tv_produk_paket);

        regNamaLengkap = (EditText)findViewById(R.id.reg1_nama_lengkap);

        regNamaAyahKandung = (EditText)findViewById(R.id.reg1_nama_ayah);

        regEmail = (EditText)findViewById(R.id.reg1_email);

        regNomorKTP = (EditText)findViewById(R.id.reg1_ktp);

        regNomorHandphone = (EditText)findViewById(R.id.reg1_notelp);

        regAlamat = (EditText)findViewById(R.id.reg1_alamat);

        regEmailKoordinator = (EditText) findViewById(R.id.reg1_email_koordinator);
        regEmailKoordinator.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean pattern = ValidEmail.isValidEmail(regEmailKoordinator.getText());
                if(pattern){
                    regEmailKoordinator.setError(null);
                    Call<KoordinatorId> call = apiClientInterface.agenGetKoordinator(preferences.getUserid(),preferences.getToken(),"","mobile","","","",regEmailKoordinator.getText().toString());
                    call.enqueue(new Callback<KoordinatorId>() {
                        @Override
                        public void onResponse(Call<KoordinatorId> call, Response<KoordinatorId> response) {
                            if(response.code() == 200){
                                if (response.body().getErrorcode().equals("00000")){
                                    if(response.body().getData() == null){
                                        regEmailKoordinator.setError("Email Koordinator Tidak Ditemukan");
                                    }else {
                                        formData2 = true;
                                        idKoordinator = response.body().getData().get(0).getJamaah_id();
                                    }
                                }else {
                                    regEmailKoordinator.setError("Email Koordinator Tidak Ditemukan");
                                }
                            }else if(response.code() == 401){
                                Logout.logout(RegisterJamaahBaruActivity.this);
                            }
                        }

                        @Override
                        public void onFailure(Call<KoordinatorId> call, Throwable t) {

                        }
                    });
                }else {
                    regEmailKoordinator.setError("Format Email Tidak Valid");
                }
            }
        });

        cbKoordinator = (CheckBox)findViewById(R.id.reg1_checkbox_koordinator);
        cbKoordinator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    if (cbSubordinat.isChecked() == true){
                        regEmailKoordinator.setVisibility(View.GONE);
                        cbSubordinat.setChecked(false);
                    }
                    cbSubordinat.setEnabled(false);

                } else {
                    cbSubordinat.setEnabled(true);
                }
            }
        });

        cbSubordinat = (CheckBox)findViewById(R.id.reg1_checkbox_jamaah_subordinat);
        cbSubordinat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    if (cbKoordinator.isChecked()==true){
                        cbKoordinator.setChecked(false);
                    }
                    cbKoordinator.setEnabled(false);
                    regEmailKoordinator.setVisibility(View.VISIBLE);
                } else {
                    regEmailKoordinator.setVisibility(View.GONE);
                    cbKoordinator.setEnabled(true);
                }
            }
        });

        btRegister = (Button)findViewById(R.id.bt_register);
        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Check Empty and Format
                if(regNamaLengkap.getText().toString().equals("")){
                    regNamaLengkap.setError("Nama Lengkap Masih Kosong");
                }else if(TextUtils.isEmpty(regNamaAyahKandung.getText()) || regNamaAyahKandung.getText().toString().equals("")){
                    regNamaAyahKandung.setError("Nama Ayah Kandung Masih Kosong");
                } else if(regEmail.getText().toString().equals("")){
                    boolean pattern = ValidEmail.isValidEmail(regEmail.getText());
                    if(!pattern){
                        regEmail.setError("Masukan Email / Email Tidak Valid");
                    }
                }else if(regNomorKTP.getText().toString().equals("") || regNomorKTP.getText().toString().length() != 16){
                    regNomorKTP.setError("Nomor KTP Masih Kosong atau tidak valid");
                }else if(regNomorHandphone.getText().toString().equals("")){
                    regNomorHandphone.setError("Nomor Handphone Masih Kosong");
                }else if(regAlamat.getText().toString().equals("")){
                    regAlamat.setError("Alamat Masih Kosong");
                }else {
                    formData1 = true;
                }

                if(cbKoordinator.isChecked() && formData1 == true){

                    Call<RegisterResponse> call = apiClientInterface.registerJamaah(
                            preferences.getUserid(),
                            preferences.getToken(),
                            "mobile",
                            preferences.getAgent_id(),
                            regNamaLengkap.getText().toString(),
                            regNamaAyahKandung.getText().toString(),
                            regNomorKTP.getText().toString(),
                            "1",
                            preferences.getPaketId(),
                            regAlamat.getText().toString(),
                            regEmail.getText().toString(),
                            regNomorHandphone.getText().toString()
                    );
                    progressBar.setMessage("Mendaftarkan Jamaah");
                    progressBar.show();
                    call.enqueue(new Callback<RegisterResponse>() {
                        @Override
                        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                            if (response.code() == 200){
                                if(response.body().getErrorcode().equals("00000")){
                                    Log.d("ID JAMAAH", "onResponse: " + response.body().getData().getJamaah_id());
                                    progressBar.dismiss();
                                    registerPeruriPay(response.body().getData().getJamaah_id());
                                }
                            }else if(response.code() == 401){
                                progressBar.dismiss();
                                Logout.logout(RegisterJamaahBaruActivity.this);
                            }
                        }

                        @Override
                        public void onFailure(Call<RegisterResponse> call, Throwable t) {
                            progressBar.dismiss();
                            AlertConnectionDialog.showConnectionAlert(RegisterJamaahBaruActivity.this);
                        }
                    });

                }else if(cbSubordinat.isChecked() && formData1 == true){
                    if(formData2 == true && !idKoordinator.equals("") && !idKoordinator.equals(null)){

                        Call<RegisterResponse> call = apiClientInterface.registerJamaahKoordinator(
                                preferences.getUserid(),
                                preferences.getToken(),
                                "mobile",
                                preferences.getAgent_id(),
                                regNamaLengkap.getText().toString(),
                                regNamaAyahKandung.getText().toString(),
                                regNomorKTP.getText().toString(),
                                "0",
                                preferences.getPaketId().toString(),
                                regAlamat.getText().toString(),
                                regEmail.getText().toString(),
                                regNomorHandphone.getText().toString(),
                                idKoordinator
                                );
                        progressBar.setMessage("Mendaftarkan Akun");
                        progressBar.show();
                        call.enqueue(new Callback<RegisterResponse>() {
                            @Override
                            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                                if (response.code() == 200){
                                    if(response.body().getErrorcode().equals("00000")){
                                        Log.d("ID JAMAAH", "onResponse: " + response.body().getData().getJamaah_id());
                                        progressBar.dismiss();
                                        registerPeruriPay(response.body().getData().getJamaah_id());
                                    }
                                }else if(response.code() == 401){
                                    progressBar.dismiss();
                                    Logout.logout(RegisterJamaahBaruActivity.this);
                                }
                            }

                            @Override
                            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                                progressBar.dismiss();
                                AlertConnectionDialog.showConnectionAlert(RegisterJamaahBaruActivity.this);
                            }
                        });
                    }
                }else if(!cbKoordinator.isChecked() && !cbSubordinat.isChecked() && formData1 == true){
                    Call<RegisterResponse> call = apiClientInterface.registerJamaah(
                            preferences.getUserid(),
                            preferences.getToken(),
                            "mobile",
                            preferences.getAgent_id(),
                            regNamaLengkap.getText().toString(),
                            regNamaAyahKandung.getText().toString(),
                            regNomorKTP.getText().toString(),
                            "0",
                            preferences.getPaketId().toString(),
                            regAlamat.getText().toString(),
                            regEmail.getText().toString(),
                            regNomorHandphone.getText().toString()
                    );
                    progressBar.setMessage("Mendaftarkan Akun");
                    progressBar.show();
                    call.enqueue(new Callback<RegisterResponse>() {
                        @Override
                        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                            if (response.code() == 200){
                                if(response.body().getErrorcode().equals("00000")){
                                    Log.d("ID JAMAAH", "onResponse: " + response.body().getData().getJamaah_id());
                                    progressBar.dismiss();
                                    registerPeruriPay(response.body().getData().getJamaah_id());
                                }
                            }else if(response.code() == 401){
                                progressBar.dismiss();
                                Logout.logout(RegisterJamaahBaruActivity.this);
                            }
                        }

                        @Override
                        public void onFailure(Call<RegisterResponse> call, Throwable t) {
                            progressBar.dismiss();
                            AlertConnectionDialog.showConnectionAlert(RegisterJamaahBaruActivity.this);
                        }
                    });
                }
            }
        });

        for (int i = 0; i < searchList.size(); i++){
            for(int j = 0; j < searchList.get(i).getPaket().size(); j++){
                if (searchList.get(i).getPaket().get(j).getPackage_id().equals(preferences.getPaketId())) {
                    tvProdukPaket.setText(searchList.get(i).getProduct_name() + " - " + searchList.get(i).getPaket().get(j).getPackage_name());
                }
            }
        }
    }

    public void registerPeruriPay(final String idJamaah) {
        Call<PeruriPayRegister> call = apiClientInterface.peruriSignUp(
                preferences.getUserid(),
                preferences.getToken(),
                "mobile",
                "1",
                regEmail.getText().toString(),
                "Umroh12345678",
                regNamaLengkap.getText().toString(),
                regNomorHandphone.getText().toString()
                );
        progressBar.setMessage("Mendaftarkan Akun Peruri Pay");
        progressBar.show();
        call.enqueue(new Callback<PeruriPayRegister>(){
            @Override
            public void onResponse(Call<PeruriPayRegister> call, Response<PeruriPayRegister> response) {
                if(response.code()==200){
                    // SUKSES TMONEY REGISTER
                    if(response.body().getData().getResultCode().equals("185")){
                        updatePeruriPayJamaah(idJamaah);
                    }
                    // TMONEY TERDAFTAR
                    else if(response.body().getData().getResultCode().equals("F")){
//                        updatePeruriPayJamaahTerdaftar(idJamaah);
                        updatePeruriPayJamaahGagal(idJamaah);
                    }
                    //GAGAL DAFTAR PERURI PAY
                    else{
                        updatePeruriPayJamaahGagal(idJamaah);
                    }
                }else if(response.code()==401){
                    progressBar.dismiss();
                    Logout.logout(RegisterJamaahBaruActivity.this);
                }
            }

            @Override
            public void onFailure(Call<PeruriPayRegister> call, Throwable t) {
                progressBar.dismiss();
                //AlertConnectionDialog.showConnectionAlert(RegisterJamaahBaruActivity.this);
                updatePeruriPayJamaahGagal(idJamaah);
            }
        });
    }

    private void updatePeruriPayJamaahTerdaftar(String idJamaah) {
        Log.d("TAG", "onResponse: T Money Sudah Registrasi Peruri Pay Jamaah");
        Call<ResponseBody> call = apiClientInterface.jamaahUpdatePeruriPayGagal(
                preferences.getUserid(),
                preferences.getToken(),
                "mobile",
                idJamaah,
                "1",
                "0"
        );
        call.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    progressBar.dismiss();
                    finish();
                }else if(response.code()==401){
                    progressBar.dismiss();
                    Logout.logout(RegisterJamaahBaruActivity.this);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.dismiss();
                finish();
            }
        });
    }

    private void updatePeruriPayJamaahGagal(String idJamaah) {
        Log.d("TAG", "onResponse: Gagal Update Peruri Pay Jamaah");
        Call<ResponseBody> call = apiClientInterface.jamaahUpdatePeruriPayGagal(
                preferences.getUserid(),
                preferences.getToken(),
                "mobile",
                idJamaah,
                "0",
                "0"
        );
        call.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code()==200){
                    progressBar.dismiss();
                    finish();
                }else if(response.code() == 401){
                    progressBar.dismiss();
                    Logout.logout(RegisterJamaahBaruActivity.this);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.dismiss();
                finish();
            }
        });
    }

    private void updatePeruriPayJamaah(String idJamaah) {
        Log.d("TAG", "onResponse: Berhasil Update Peruri Pay Jamaah");
        Call<ResponseBody> call = apiClientInterface.jamaahUpdatePeruriPay(
                preferences.getUserid(),
                preferences.getToken(),
                "mobile",
                idJamaah,
                regEmail.getText().toString(),
                regNamaLengkap.getText().toString(),
                "Umroh12345678",
                regNomorHandphone.getText().toString(),
                "1",
                "1"
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code()==200){
                    progressBar.dismiss();
                    finish();
                }else if(response.code()==401){
                    progressBar.dismiss();
                    Logout.logout(RegisterJamaahBaruActivity.this);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.dismiss();
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_close_white,menu);
        return true;
    }
}

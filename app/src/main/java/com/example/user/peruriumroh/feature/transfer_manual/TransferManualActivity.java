package com.example.user.peruriumroh.feature.transfer_manual;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.peruriumroh.R;
import com.example.user.peruriumroh.etc.AlertConnectionDialog;
import com.example.user.peruriumroh.etc.AlertDialog;
import com.example.user.peruriumroh.etc.Logout;
import com.example.user.peruriumroh.etc.ValidEmail;
import com.example.user.peruriumroh.feature.login.LoginActivity;
import com.example.user.peruriumroh.feature.transfer_manual_amount.TransferManualAmountActivity;
import com.example.user.peruriumroh.model.EachAgenLogin;
import com.example.user.peruriumroh.model.ListJamaah;
import com.example.user.peruriumroh.network.ApiClient;
import com.example.user.peruriumroh.network.ApiClientInterface;
import com.example.user.peruriumroh.preference.Preferences;

import java.io.IOException;

import co.ceryle.radiorealbutton.RadioRealButton;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransferManualActivity extends AppCompatActivity {

    EditText etEmail;
    Button btSubmit;

    ApiClient apiClient = new ApiClient();
    ApiClientInterface apiClientInterface = apiClient.clientInterface();

    Preferences preferences;

    ProgressDialog progressDialog;

    public static Activity ac;

    int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_manual);

        ac = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Transfer Peruri Tunai");


        progressDialog = new ProgressDialog(TransferManualActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        preferences = new Preferences(TransferManualActivity.this);

        etEmail = (EditText)findViewById(R.id.et_transfer_email);

        RadioRealButtonGroup group = (RadioRealButtonGroup)findViewById(R.id.bt_group);
        group.setOnPositionChangedListener(new RadioRealButtonGroup.OnPositionChangedListener(){
            @Override
            public void onPositionChanged(RadioRealButton button, int currentPosition, int lastPosition){
                position = currentPosition;
            }
        });

        btSubmit = (Button)findViewById(R.id.bt_submit);
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position == 0){

                    //Transfer Agen
                    if(ValidEmail.isValidEmail(etEmail.getText()) && !etEmail.getText().equals("")){
                        progressDialog.setMessage("Mencari Pengguna...");
                        progressDialog.show();

                        Call<EachAgenLogin> getAgenData = apiClientInterface.getAgenByEmail(
                                preferences.getUserid(),
                                preferences.getToken(),
                                "mobile",
                                etEmail.getText().toString()
                        );

                        getAgenData.enqueue(new Callback<EachAgenLogin>() {
                            @Override
                            public void onResponse(Call<EachAgenLogin> call, Response<EachAgenLogin> response) {
                                if(response.code() == 200){
                                    //PARSING DATA AGEN
                                    if(response.body().getErrorcode().equals("00000")){
                                        if(response.body().getData() != null){
                                            if(response.body().getErrormsg().equals("OK")){
                                                progressDialog.dismiss();
                                                Intent intent = new Intent(TransferManualActivity.this,TransferManualAmountActivity.class);
                                                intent.putExtra("role","Agent");
                                                intent.putExtra("id",response.body().getData().get(0).getAgent_id());
                                                intent.putExtra("nama",response.body().getData().get(0).getAgent_name());
                                                intent.putExtra("email",response.body().getData().get(0).getAgent_email());
                                                startActivity(intent);
                                            }
                                        }
                                    }else if(response.body().getErrorcode().equals("00001")){
                                        progressDialog.dismiss();
                                        AlertDialog.showAlert(TransferManualActivity.this,"Transfer Peruri Tunai","Email Pengguna tidak ditemukan");
                                    }
                                    //OPEN AMOUNT PAGE
                                }else if(response.code() == 401){
                                    progressDialog.dismiss();
                                    Logout.logout(TransferManualActivity.this);
                                } else {
                                    progressDialog.dismiss();
                                    AlertDialog.showAlert(TransferManualActivity.this,"Transfer Peruri Tunai","Email Pengguna tidak ditemukan");
                                }
                            }

                            @Override
                            public void onFailure(Call<EachAgenLogin> call, Throwable t) {
                                if(t instanceof IOException){
                                    progressDialog.dismiss();
                                    AlertConnectionDialog.showConnectionAlert(TransferManualActivity.this);
                                }else {
                                    progressDialog.dismiss();
                                    AlertDialog.showAlert(TransferManualActivity.this,"Peruri Umroh", "Terjadi kesalahan sistem");
                                }
                            }
                        });
                    }


                }else if (position == 1){

                    //Transfer Jamaah
                    if(ValidEmail.isValidEmail(etEmail.getText()) && !etEmail.getText().equals("")){
                        progressDialog.setMessage("Mencari Pengguna...");
                        progressDialog.show();
                        Call<ListJamaah> getJamaah = apiClientInterface.jamaahGetJamaahByEmail(
                                preferences.getUserid(),
                                preferences.getToken(),
                                "mobile",
                                etEmail.getText().toString()
                        );
                        getJamaah.enqueue(new Callback<ListJamaah>(){
                            @Override
                            public void onResponse(Call<ListJamaah> call, Response<ListJamaah> response) {
                                if(response.code() == 200){
                                    if(response.body().getData() != null){
                                        if(response.body().getErrormsg().equals("OK")){
                                            progressDialog.dismiss();
                                            Intent intent = new Intent(TransferManualActivity.this,TransferManualAmountActivity.class);
                                            intent.putExtra("role","Jamaah");
                                            intent.putExtra("id",response.body().getData().get(0).getJamaah_id());
                                            intent.putExtra("nama",response.body().getData().get(0).getJamaah_name());
                                            intent.putExtra("email",response.body().getData().get(0).getContact().get(0).getContact_email());
                                            startActivity(intent);
                                        }
                                    }else {
                                        progressDialog.dismiss();
                                        AlertDialog.showAlert(TransferManualActivity.this,"Transfer Peruri Tunai","Email Pengguna tidak ditemukan");
                                    }
                                }else if(response.code() == 401){
                                    progressDialog.dismiss();
                                    Logout.logout(TransferManualActivity.this);
                                } else {
                                    progressDialog.dismiss();
                                    AlertDialog.showAlert(TransferManualActivity.this,"Transfer Peruri Tunai","Email Pengguna tidak ditemukan");
                                }
                            }

                            @Override
                            public void onFailure(Call<ListJamaah> call, Throwable t) {
                                progressDialog.dismiss();
                                if(t instanceof IOException){
                                    AlertConnectionDialog.showConnectionAlert(TransferManualActivity.this);
                                }else {
                                    AlertDialog.showAlert(TransferManualActivity.this,"Peruri Umroh", "Terjadi kesalahan sistem");
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_close,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemMenu = item.getItemId();
        switch (itemMenu){
            case R.id.action_close:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
